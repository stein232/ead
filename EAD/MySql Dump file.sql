CREATE DATABASE  IF NOT EXISTS `eadassignment` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `eadassignment`;
-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: ap-cdbr-azure-southeast-a.cloudapp.net    Database: eadassignment
-- ------------------------------------------------------
-- Server version	5.5.45-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actor`
--

DROP TABLE IF EXISTS `actor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actor` (
  `ActorId` int(11) NOT NULL AUTO_INCREMENT,
  `ActorName` varchar(255) NOT NULL,
  `_IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ActorId`)
) ENGINE=InnoDB AUTO_INCREMENT=562 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actor`
--

LOCK TABLES `actor` WRITE;
/*!40000 ALTER TABLE `actor` DISABLE KEYS */;
INSERT INTO `actor` VALUES (1,'Matt Damon',0),(2,'Jessica Chastain',0),(3,'Kristen Wiig',0),(4,'Jeff Daniels',0),(5,'Ellen Burstyn',0),(6,'Matthew McConaughey',0),(7,'Mackenzie Foy',0),(8,'John Lithgow',0),(9,'Jennifer Lawrence',0),(10,'Josh Hutcherson',0),(11,'Liam Hemsworth',0),(12,'Woody Harrelson',0),(13,'Daniel Craig',0),(14,'Christoph Waltz',0),(15,'Léa Seydoux',0),(16,'Ralph Fiennes',0),(17,'Vivian Sung',0),(18,'Darren Wang',0),(19,'Joe Chen',0),(20,'Jerry Yan',0),(21,'Mitsuki Tanimura',0),(22,'Haruka Kinami',0),(23,'Aimi Satsukawa',0),(24,'Anna Ishibashi',0),(111,'Chris Evans',0),(121,'Hayley Atwell',0),(131,'Sebastian Stan',0),(151,'Tommy Lee Jones',0),(161,'John Cusack',0),(162,'Amanda Peet',0),(172,'Chiwetel Ejiofor',0),(181,'Thandie Newton',0),(191,'Rebecca Black',0),(201,'Robert Downey Jr.',0),(211,'Gwyneth Paltrow',0),(221,'Don Cheadle',0),(231,'Guy Pearce',0),(241,'Amy Poehler',0),(251,'Phyllis Smith',0),(261,'Richard Kind',0),(271,'Bill Hader',0),(281,'Henry Cavill',0),(291,'Armie Hammer',0),(301,'Alicia Vikander',0),(311,'Elizabeth Debicki',0),(321,'Tom Hanks',0),(331,'Halle Berry',0),(341,'Jim Broadbent',0),(351,'Hugo Weaving',0),(361,'Mark Ruffalo',0),(371,'Chris Hemsworth',0),(381,'Blake Lively',0),(391,'Michiel Huisman',0),(401,'Harrison Ford',0),(411,'Adrian Quintons',0),(421,'Colin Firth',0),(431,'Mark Strong',0),(441,'Jonno Davies',0),(451,'Paul Rudd',0),(461,'Michael Douglas',0),(471,'Evangeline Lilly',0),(481,'Corey Stoll',0),(491,'Leonardo DiCaprio',0),(501,'Jonah Hill',0),(511,'Margot Robbie',0),(521,'a',1),(531,'John Boyega',0),(541,'Oscar Isaac',0),(551,'Daisy Ridley',0),(561,'Mark Hamill',0);
/*!40000 ALTER TABLE `actor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genre`
--

DROP TABLE IF EXISTS `genre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genre` (
  `GenreId` int(11) NOT NULL AUTO_INCREMENT,
  `GenreName` varchar(255) NOT NULL,
  `_IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`GenreId`)
) ENGINE=InnoDB AUTO_INCREMENT=512 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genre`
--

LOCK TABLES `genre` WRITE;
/*!40000 ALTER TABLE `genre` DISABLE KEYS */;
INSERT INTO `genre` VALUES (1,'Adventure',1),(2,'Sci-Fi',0),(3,'Thriller',0),(4,'Action',1),(5,'Comedy',0),(6,'Romance',0),(7,'Fantasy',0),(8,'Drama',0),(9,'Short',0),(10,'Horror',0),(11,'Family',0),(12,'Animation',0),(501,'Biography',0),(511,'Crime',0);
/*!40000 ALTER TABLE `genre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hall`
--

DROP TABLE IF EXISTS `hall`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hall` (
  `HallId` int(11) NOT NULL AUTO_INCREMENT,
  `Theater_TheaterId` int(11) NOT NULL,
  `HallNo` int(11) NOT NULL,
  PRIMARY KEY (`HallId`),
  KEY `fk_Hall_Theater1_idx` (`Theater_TheaterId`)
) ENGINE=InnoDB AUTO_INCREMENT=191 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hall`
--

LOCK TABLES `hall` WRITE;
/*!40000 ALTER TABLE `hall` DISABLE KEYS */;
INSERT INTO `hall` VALUES (131,1,1),(141,1,2),(151,91,1),(161,3,1),(171,2,1),(181,101,1);
/*!40000 ALTER TABLE `hall` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member` (
  `MemberId` int(11) NOT NULL AUTO_INCREMENT,
  `Email` varchar(45) NOT NULL,
  `Password` varchar(45) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `ContactNumber` varchar(45) NOT NULL,
  `MailingAddress` varchar(45) NOT NULL,
  `CreditCardNumber` varchar(45) NOT NULL,
  PRIMARY KEY (`MemberId`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member`
--

LOCK TABLES `member` WRITE;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;
INSERT INTO `member` VALUES (1,'root@root.root','5f4dcc3b5aa765d61d8327deb882cf99','Poe Dameron','4763337728','1, Lonely Island, 333744 Star Wars Universe','4096365875243192'),(51,'jjsjs@jdjj.com','2af54305f183778d87de0c70c591fae4','jdjjd','jjj','jjj','1111111111111111'),(61,'111@jj','90f55cf198742a1937b55b700449065e','eee','772722','3y3yy33','1111111111111111'),(71,'abc@def.com','c828dbded0edb98f36a73cac37f495a2','abc def','12345678','abcdef','4500123023212133'),(81,'abc@def.com','d1da264bc8ac232d72befa737083a8e2','abcdef','12345678','abcdef street','4150123412341234'),(91,'luke@starwars.com','aeb4d7d8d8b05880497e658972afc5ce','Anakin-Luke Skywalker','12345679','2, Death Star, 111111 Starkiller Base','5362737362626837');
/*!40000 ALTER TABLE `member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie`
--

DROP TABLE IF EXISTS `movie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movie` (
  `MovieId` int(11) NOT NULL AUTO_INCREMENT,
  `MovieName` varchar(255) NOT NULL,
  `ReleaseDate` date NOT NULL,
  `Sypnosis` varchar(1000) NOT NULL,
  `Duration` int(11) NOT NULL,
  `ImageURL` varchar(1000) DEFAULT NULL,
  `_IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `EndDate` date DEFAULT NULL,
  PRIMARY KEY (`MovieId`)
) ENGINE=InnoDB AUTO_INCREMENT=1102 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie`
--

LOCK TABLES `movie` WRITE;
/*!40000 ALTER TABLE `movie` DISABLE KEYS */;
INSERT INTO `movie` VALUES (1,'The Martian','2015-10-03','During a manned mission to Mars, Astronaut Mark Watney is presumed dead after a fierce storm and left behind by his crew. But Watney has survived and finds himself stranded and alone on the hostile planet. With only meager supplies, he must draw upon his ingenuity, wit and spirit to subsist and find a way to signal to Earth that he is alive. Millions of miles away, NASA and a team of international scientists work tirelessly to bring the \"Martian\" home, while his crewmates concurrently plot a daring, if not impossible, rescue mission. As these stories of incredible bravery unfold, the world comes together to root for Watney\'s safe return.',144,'https://res.cloudinary.com/djmxweuza/image/upload/v1448506243/v6ijdmlznfri3fxr2clc.jpg',0,NULL),(21,'Spectre','2015-11-06','A cryptic message from the past sends James Bond on a rogue mission to Mexico City and eventually Rome, where he meets Lucia, the beautiful and forbidden widow of an infamous criminal. Bond infiltrates a secret meeting and uncovers the existence of the sinister organisation known as SPECTRE. Meanwhile back in London, Max Denbigh, the new head of the Centre of National Security, questions Bond\'s actions and challenges the relevance of MI6 led by M. Bond covertly enlists Moneypenny and Q to help him seek out Madeleine Swann, the daughter of his old nemesis Mr White, who may hold the clue to untangling the web of SPECTRE. As the daughter of the assassin, she understands Bond in a way most others cannot. As Bond ventures towards the heart of SPECTRE, he learns a chilling connection between himself and the enemy he seeks.',148,'https://res.cloudinary.com/djmxweuza/image/upload/v1448506101/nvnvyphj6bbixfwoe4gj.jpg',0,NULL),(31,'Interstellar','2014-11-07','In the near future, Earth has been devastated by drought and famine, causing a scarcity in food and extreme changes in climate. When humanity is facing extinction, a mysterious rip in the space-time continuum is discovered, giving mankind the opportunity to widen its lifespan. A group of explorers must travel beyond our solar system in search of a planet that can sustain life. The crew of the Endurance are required to think bigger and go further than any human in history as they embark on an interstellar voyage into the unknown. Coop, the pilot of the Endurance, must decide between seeing his children again and the future of the human race.',169,'https://res.cloudinary.com/djmxweuza/image/upload/v1448506202/esmv4auxjjqwoxmiiabp.jpg',0,NULL),(41,'Inside Out','2015-06-19','Growing up can be a bumpy road, and it\'s no exception for Riley, who is uprooted from her Midwest life when her father starts a new job in San Francisco. Like all of us, Riley is guided by her emotions - Joy, Fear, Anger, Disgust and Sadness. The emotions live in Headquarters, the control center inside Riley\'s mind, where they help advise her through everyday life. As Riley and her emotions struggle to adjust to a new life in San Francisco, turmoil ensues in Headquarters. Although Joy, Riley\'s main and most important emotion, tries to keep things positive, the emotions conflict on how best to navigate a new city, house and school.',94,'https://res.cloudinary.com/djmxweuza/image/upload/v1448506254/wryrwkh8bcr663ggmut9.jpg',0,NULL),(51,'The Man From U.N.C.L.E.','2014-08-14','In the 1960s with the Cold War in play, CIA agent Napoleon Solo successfully helps Gaby Teller defect to West Germany despite the intimidating opposition of KGB agent Illya Kuryakin. Later, all three unexpectedly find themselves working together in a joint mission to stop a private criminal organization from using Gaby\'s father\'s scientific expertise to construct their own nuclear bomb. Through clenched teeth and stylish poise, all three must find a way to cooperate for the sake of world peace, even as they each pursue their own agendas.',116,'https://res.cloudinary.com/djmxweuza/image/upload/v1448506089/dvmvlukb7x77fudpouvo.jpg',0,NULL),(61,'Cloud Atlas','2012-10-26','Everything is connected: an 1849 diary of an ocean voyage across the Pacific; letters from a composer to his lover; a thriller about a conspiracy at a nuclear power plant; a farce about a publisher in a nursing home; a rebellious clone in futuristic Korea; and the tale of a tribe living in post-apocalyptic Seoul, far in the future.',172,'https://res.cloudinary.com/djmxweuza/image/upload/v1448506268/maexy1iyoluasz5hzu3y.jpg',0,'2012-07-01'),(71,'Ant Man','2015-07-17','Armed with the astonishing ability to shrink in scale but increase in strength, con-man Scott Lang must embrace his inner-hero and help his mentor, Dr. Hank Pym, protect the secret behind his spectacular Ant-Man suit from a new generation of towering threats. Against seemingly insurmountable obstacles, Pym and Lang must plan and pull off a heist that will save the world.',117,'https://res.cloudinary.com/djmxweuza/image/upload/v1448506124/edkxjhuc99qx7lvxn57o.jpg',0,NULL),(81,'Marvel\'s The Avengers','2012-05-04','Nick Fury is director of S.H.I.E.L.D, an international peace keeping agency. The agency is a who\'s who of Marvel Super Heroes, with Iron Man, The Incredible Hulk, Thor, Captain America, Hawkeye and Black Widow. When global security is threatened by Loki and his cohorts, Nick Fury and his team will need all their powers to save the world from disaster.',143,'https://res.cloudinary.com/djmxweuza/image/upload/v1448506219/rpyd4cthxzuvm5xba45r.jpg',0,NULL),(91,'Avengers: Age of Ultron','2015-05-01','When Tony Stark tries to jumpstart a dormant peacekeeping program, things go awry and Earth\'s Mightiest Heroes, including Iron Man, Captain America, Thor, the Incredible Hulk, Black Widow and Hawkeye, are put to the ultimate test as the fate of the planet hangs in the balance. As the villainous Ultron emerges, it is up to the Avengers to stop him from enacting his terrible plans, and soon uneasy alliances and unexpected action pave the way for a global adventure.',141,'https://res.cloudinary.com/djmxweuza/image/upload/v1448506141/r4z3qovlow9qyigvygv1.jpg',0,NULL),(101,'Age of Adaline','2015-04-24','After miraculously remaining 29 years old for almost eight decades, Adaline Bowman has lived a solitary existence, never allowing herself to get close to anyone who might reveal her secret. But a chance encounter with charismatic philanthropist Ellis Jones reignites her passion for life and romance. When a weekend with his parents threatens to uncover the truth, Adaline makes a decision that will change her life forever.',112,'https://res.cloudinary.com/djmxweuza/image/upload/v1448506188/giqvzz8jzmpmxk6kzbj7.jpg',0,NULL),(111,'Kingsman: The Secret Service','2015-02-13','Based upon the acclaimed comic book and directed by Matthew Vaughn, Kingsman: The Secret Service tells the story of a super-secret spy organization that recruits an unrefined but promising street kid into the agency\'s ultra-competitive training program just as a global threat emerges from a twisted tech genius.',150,'https://res.cloudinary.com/djmxweuza/image/upload/v1448506110/wyvmjteau8qf7izxqleu.jpg',0,NULL),(311,'Captain America: The First Avenger','2011-07-22','It is 1942, America has entered World War II, and sickly but determined Steve Rogers is frustrated at being rejected yet again for military service. Everything changes when Dr. Erskine recruits him for the secret Project Rebirth. Proving his extraordinary courage, wits and conscience, Rogers undergoes the experiment and his weak body is suddenly enhanced into the maximum human potential. When Dr. Erskine is then immediately assassinated by an agent of Nazi Germany\'s secret HYDRA research department (headed by Johann Schmidt, a.k.a. the Red Skull), Rogers is left as a unique man who is initially misused as a propaganda mascot; however, when his comrades need him, Rogers goes on a successful adventure that truly makes him Captain America, and his war against Schmidt begins.',124,'https://res.cloudinary.com/djmxweuza/image/upload/v1448546466/qsmw0rzxal7mzksjpmz5.jpg',0,'2015-12-31'),(321,'2012','2009-11-13','Dr. Adrian Helmsley, part of a worldwide geophysical team investigating the effect on the earth of radiation from unprecedented solar storms, learns that the earth\'s core is heating up. He warns U.S. President Thomas Wilson that the crust of the earth is becoming unstable and that without proper preparations for saving a fraction of the world\'s population, the entire race is doomed. Meanwhile, writer Jackson Curtis stumbles on the same information. While the world\'s leaders race to build \"arks\" to escape the impending cataclysm, Curtis struggles to find a way to save his family. Meanwhile, volcanic eruptions and earthquakes of unprecedented strength wreak havoc around the world.',158,'https://res.cloudinary.com/djmxweuza/image/upload/v1448597021/zojh6mmzsvfhszjuolug.jpg',0,'2015-12-31'),(891,'The Wolf of Wall Street','2013-12-25','Jordan Belfort is a Long Island penny stockbroker who served 22 months in prison for defrauding investors in a massive 1990s securities scam that involved widespread corruption on Wall Street and in the corporate banking world, including shoe designer Steve Madden.',180,'https://res.cloudinary.com/djmxweuza/image/upload/v1448882368/euqo9lnhqmornzpritfs.jpg',0,NULL),(1101,'Star Wars Episode VII: The Force Awakens','2015-12-24','Three decades after the defeat of the Galactic Empire, a new threat arises. The First Order attempts to rule the galaxy and only a ragtag group of heroes can stop them, along with the help of the Resistance.',135,'https://res.cloudinary.com/djmxweuza/image/upload/v1454045661/gqhsca59kxh2xy3q3bvk.jpg',0,NULL);
/*!40000 ALTER TABLE `movie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie_has_actor`
--

DROP TABLE IF EXISTS `movie_has_actor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movie_has_actor` (
  `Movie_MovieId` int(11) NOT NULL,
  `Actor_ActorId` int(11) NOT NULL,
  PRIMARY KEY (`Movie_MovieId`,`Actor_ActorId`),
  KEY `fk_Movie_has_Actor_Actor1_idx` (`Actor_ActorId`),
  KEY `fk_Movie_has_Actor_Movie1_idx` (`Movie_MovieId`),
  CONSTRAINT `fk_Movie_has_Actor_Actor1` FOREIGN KEY (`Actor_ActorId`) REFERENCES `actor` (`ActorId`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_Movie_has_Actor_Movie1` FOREIGN KEY (`Movie_MovieId`) REFERENCES `movie` (`MovieId`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie_has_actor`
--

LOCK TABLES `movie_has_actor` WRITE;
/*!40000 ALTER TABLE `movie_has_actor` DISABLE KEYS */;
INSERT INTO `movie_has_actor` VALUES (1,1),(1,3),(31,5),(101,5),(31,6),(891,6),(1,7),(31,7),(31,8),(21,13),(21,14),(21,15),(21,16),(81,111),(91,111),(311,121),(311,131),(311,151),(321,161),(321,162),(1,172),(321,172),(321,181),(81,201),(91,201),(41,241),(41,251),(41,261),(41,271),(51,281),(51,291),(51,301),(51,311),(61,321),(61,331),(61,341),(61,351),(81,361),(91,361),(81,371),(91,371),(101,381),(101,391),(101,401),(1101,401),(111,411),(111,421),(111,431),(111,441),(71,451),(71,461),(71,471),(71,481),(891,491),(891,501),(891,511),(1101,531),(1101,541),(1101,551),(1101,561);
/*!40000 ALTER TABLE `movie_has_actor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie_has_genre`
--

DROP TABLE IF EXISTS `movie_has_genre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movie_has_genre` (
  `Movie_MovieId` int(11) NOT NULL,
  `Genre_GenreId` int(11) NOT NULL,
  PRIMARY KEY (`Movie_MovieId`,`Genre_GenreId`),
  KEY `fk_Movie_has_Genre_Genre1_idx` (`Genre_GenreId`),
  KEY `fk_Movie_has_Genre_Movie1_idx` (`Movie_MovieId`),
  CONSTRAINT `fk_Movie_has_Genre_Genre1` FOREIGN KEY (`Genre_GenreId`) REFERENCES `genre` (`GenreId`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_Movie_has_Genre_Movie1` FOREIGN KEY (`Movie_MovieId`) REFERENCES `movie` (`MovieId`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie_has_genre`
--

LOCK TABLES `movie_has_genre` WRITE;
/*!40000 ALTER TABLE `movie_has_genre` DISABLE KEYS */;
INSERT INTO `movie_has_genre` VALUES (31,1),(51,1),(71,1),(81,1),(91,1),(111,1),(311,1),(321,1),(1,2),(31,2),(61,2),(71,2),(81,2),(91,2),(311,2),(321,2),(1101,2),(21,3),(51,4),(71,4),(81,4),(91,4),(111,4),(311,4),(321,4),(1,5),(41,5),(51,5),(111,5),(891,5),(101,6),(101,7),(1101,7),(1,8),(31,8),(61,8),(101,8),(41,12),(891,501),(891,511);
/*!40000 ALTER TABLE `movie_has_genre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movieticket`
--

DROP TABLE IF EXISTS `movieticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movieticket` (
  `MovieTicketId` int(11) NOT NULL AUTO_INCREMENT,
  `Seat_Row` varchar(2) NOT NULL,
  `Seat_SeatNumber` int(11) NOT NULL,
  `ShowTime_ShowTimeId` int(11) NOT NULL,
  `ShowTime_Hall_HallId` int(11) NOT NULL,
  `ShowTime_Hall_Theater_TheaterId` int(11) NOT NULL,
  `ShowTime_Movie_MovieId` int(11) NOT NULL,
  `MemberId` int(11) NOT NULL,
  `PurchasedDateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Price` int(11) NOT NULL,
  `MovieTypeId` int(11) NOT NULL,
  PRIMARY KEY (`MovieTicketId`),
  KEY `fk_Seat_has_ShowTime_ShowTime1_idx` (`ShowTime_Hall_Theater_TheaterId`),
  KEY `fk_Seat_has_ShowTime_Seat1_idx` (`Seat_Row`,`Seat_SeatNumber`),
  KEY `fk_Seat_has_ShowTime_ShowTime1_idx1` (`ShowTime_ShowTimeId`,`ShowTime_Hall_HallId`,`ShowTime_Hall_Theater_TheaterId`,`ShowTime_Movie_MovieId`),
  KEY `fk_Seat_has_ShowTime_Member1_idx` (`MemberId`),
  KEY `fk_MovieTicket_MovieType1_idx` (`MovieTypeId`),
  CONSTRAINT `fk_MovieTicket_Member1` FOREIGN KEY (`MemberId`) REFERENCES `member` (`MemberId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_MovieTicket_MovieType1` FOREIGN KEY (`MovieTypeId`) REFERENCES `movietype` (`MovieTypeId`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_Seat_has_ShowTime_Seat1` FOREIGN KEY (`Seat_Row`, `Seat_SeatNumber`) REFERENCES `seat` (`Row`, `SeatNumber`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Seat_has_ShowTime_ShowTime1` FOREIGN KEY (`ShowTime_ShowTimeId`, `ShowTime_Hall_HallId`, `ShowTime_Hall_Theater_TheaterId`, `ShowTime_Movie_MovieId`) REFERENCES `showtime` (`ShowTimeId`, `Hall_HallId`, `Hall_Theater_TheaterId`, `Movie_MovieId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=481 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movieticket`
--

LOCK TABLES `movieticket` WRITE;
/*!40000 ALTER TABLE `movieticket` DISABLE KEYS */;
INSERT INTO `movieticket` VALUES (351,'H',10,1671,161,3,1101,1,'2016-01-29 14:27:54',13,11),(361,'H',11,1671,161,3,1101,1,'2016-01-29 14:27:55',13,11),(371,'H',12,1671,161,3,1101,1,'2016-01-29 14:27:55',13,11),(381,'H',10,1641,151,91,1101,1,'2016-01-29 17:01:58',20,21),(391,'H',11,1641,151,91,1101,1,'2016-01-29 17:01:58',20,21),(401,'F',9,1671,161,3,1101,81,'2016-01-29 17:47:20',13,11),(411,'F',10,1671,161,3,1101,81,'2016-01-29 17:47:20',13,11),(421,'F',11,1671,161,3,1101,81,'2016-01-29 17:47:21',13,11),(431,'H',4,1671,161,3,1101,1,'2016-01-29 19:06:35',13,11),(441,'H',5,1671,161,3,1101,1,'2016-01-29 19:06:38',13,11),(451,'F',10,1641,151,91,1101,91,'2016-01-30 07:52:18',20,21),(461,'F',11,1641,151,91,1101,91,'2016-01-30 07:52:18',20,21),(471,'F',12,1641,151,91,1101,91,'2016-01-30 07:52:18',20,21);
/*!40000 ALTER TABLE `movieticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movietype`
--

DROP TABLE IF EXISTS `movietype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movietype` (
  `MovieTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `MovieTypeName` varchar(255) NOT NULL,
  `WeekdayPrice` int(11) NOT NULL,
  `WeekendPrice` int(11) NOT NULL,
  PRIMARY KEY (`MovieTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movietype`
--

LOCK TABLES `movietype` WRITE;
/*!40000 ALTER TABLE `movietype` DISABLE KEYS */;
INSERT INTO `movietype` VALUES (1,'Digital',9,12),(11,'Dolby Atmos',10,13),(21,'IMAX',20,23),(31,'IMAX 3D',23,25);
/*!40000 ALTER TABLE `movietype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movietype_has_movie`
--

DROP TABLE IF EXISTS `movietype_has_movie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movietype_has_movie` (
  `MovieType_MovieTypeId` int(11) NOT NULL,
  `Movie_MovieId` int(11) NOT NULL,
  PRIMARY KEY (`MovieType_MovieTypeId`,`Movie_MovieId`),
  KEY `fk_MovieType_has_Movie_Movie1_idx` (`Movie_MovieId`),
  CONSTRAINT `fk_MovieType_has_Movie_Movie1` FOREIGN KEY (`Movie_MovieId`) REFERENCES `movie` (`MovieId`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_MovieType_has_Movie_MovieType1` FOREIGN KEY (`MovieType_MovieTypeId`) REFERENCES `movietype` (`MovieTypeId`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movietype_has_movie`
--

LOCK TABLES `movietype_has_movie` WRITE;
/*!40000 ALTER TABLE `movietype_has_movie` DISABLE KEYS */;
INSERT INTO `movietype_has_movie` VALUES (1,1),(1,21),(1,31),(1,41),(1,51),(1,61),(1,71),(1,81),(1,91),(1,101),(1,111),(1,311),(1,321),(1,891);
/*!40000 ALTER TABLE `movietype_has_movie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review` (
  `ReviewId` int(11) NOT NULL AUTO_INCREMENT,
  `Movie_MovieId` int(11) NOT NULL,
  `ReviewMessage` varchar(1000) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `CreateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Rating` int(11) NOT NULL,
  PRIMARY KEY (`ReviewId`,`Movie_MovieId`),
  KEY `fk_Review_Movie1_idx` (`Movie_MovieId`),
  CONSTRAINT `fk_Review_Movie1` FOREIGN KEY (`Movie_MovieId`) REFERENCES `movie` (`MovieId`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=241 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
INSERT INTO `review` VALUES (1,1,'A good movie','some guy','2015-11-27 03:01:24',5),(11,1,'A good movie.','Some guy','2015-11-27 03:03:58',5),(21,1,'A great movie.','Some guy again','2015-11-27 03:29:59',4),(31,1,'Sucks!','Fag','2015-11-30 09:40:34',1),(41,891,'Very accurate depiction of a wolf.','Wolf','2015-11-30 11:29:27',5),(51,891,'Leo deserves an Oscar!!','The Academy','2015-11-30 11:29:55',5),(61,1,'dfdggf','df','2015-12-01 01:08:46',1),(62,41,'Sucks!','fag','2015-12-01 04:16:22',1),(161,71,'1337','gud','2016-01-19 13:29:26',1),(171,1101,'This jacket looks good on you!','Poe Dameron','2016-01-29 05:48:36',5),(181,1101,'Not enough darkness','Kylo Ren','2016-01-29 05:48:59',2),(191,1101,'Good','test','2016-01-29 18:56:58',5),(201,1101,'Good','test','2016-01-29 19:00:05',5),(211,1101,'good','test','2016-01-30 06:14:28',5),(221,1101,'gay','test','2016-01-30 06:46:53',5),(231,1101,'Not enough pizza','Ian Sooooooo','2016-01-30 07:55:03',3);
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seat`
--

DROP TABLE IF EXISTS `seat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seat` (
  `Row` varchar(2) NOT NULL,
  `SeatNumber` int(11) NOT NULL,
  `Hall_HallId` int(11) NOT NULL,
  PRIMARY KEY (`Row`,`SeatNumber`,`Hall_HallId`),
  KEY `fk_Seat_Hall1_idx` (`Hall_HallId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seat`
--

LOCK TABLES `seat` WRITE;
/*!40000 ALTER TABLE `seat` DISABLE KEYS */;
INSERT INTO `seat` VALUES ('A',1,131),('A',2,131),('A',3,131),('A',4,131),('A',5,131),('A',6,131),('A',7,131),('A',8,131),('A',9,131),('A',10,131),('A',11,131),('A',12,131),('A',13,131),('A',14,131),('A',15,131),('A',16,131),('A',17,131),('A',18,131),('A',19,131),('A',20,131),('B',1,131),('B',2,131),('B',3,131),('B',4,131),('B',5,131),('B',6,131),('B',7,131),('B',8,131),('B',9,131),('B',10,131),('B',11,131),('B',12,131),('B',13,131),('B',14,131),('B',15,131),('B',16,131),('B',17,131),('B',18,131),('B',19,131),('B',20,131),('C',1,131),('C',2,131),('C',3,131),('C',4,131),('C',5,131),('C',6,131),('C',7,131),('C',8,131),('C',9,131),('C',10,131),('C',11,131),('C',12,131),('C',13,131),('C',14,131),('C',15,131),('C',16,131),('C',17,131),('C',18,131),('C',19,131),('C',20,131),('D',1,131),('D',2,131),('D',3,131),('D',4,131),('D',5,131),('D',6,131),('D',7,131),('D',8,131),('D',9,131),('D',10,131),('D',11,131),('D',12,131),('D',13,131),('D',14,131),('D',15,131),('D',16,131),('D',17,131),('D',18,131),('D',19,131),('D',20,131),('E',1,131),('E',2,131),('E',3,131),('E',4,131),('E',5,131),('E',6,131),('E',7,131),('E',8,131),('E',9,131),('E',10,131),('E',11,131),('E',12,131),('E',13,131),('E',14,131),('E',15,131),('E',16,131),('E',17,131),('E',18,131),('E',19,131),('E',20,131),('F',1,131),('F',2,131),('F',3,131),('F',4,131),('F',5,131),('F',6,131),('F',7,131),('F',8,131),('F',9,131),('F',10,131),('F',11,131),('F',12,131),('F',13,131),('F',14,131),('F',15,131),('F',16,131),('F',17,131),('F',18,131),('F',19,131),('F',20,131),('G',1,131),('G',2,131),('G',3,131),('G',4,131),('G',5,131),('G',6,131),('G',7,131),('G',8,131),('G',9,131),('G',10,131),('G',11,131),('G',12,131),('G',13,131),('G',14,131),('G',15,131),('G',16,131),('G',17,131),('G',18,131),('G',19,131),('G',20,131),('H',1,131),('H',2,131),('H',3,131),('H',4,131),('H',5,131),('H',6,131),('H',7,131),('H',8,131),('H',9,131),('H',10,131),('H',11,131),('H',12,131),('H',13,131),('H',14,131),('H',15,131),('H',16,131),('H',17,131),('H',18,131),('H',19,131),('H',20,131),('I',1,131),('I',2,131),('I',3,131),('I',4,131),('I',5,131),('I',6,131),('I',7,131),('I',8,131),('I',9,131),('I',10,131),('I',11,131),('I',12,131),('I',13,131),('I',14,131),('I',15,131),('I',16,131),('I',17,131),('I',18,131),('I',19,131),('I',20,131),('J',1,131),('J',2,131),('J',3,131),('J',4,131),('J',5,131),('J',6,131),('J',7,131),('J',8,131),('J',9,131),('J',10,131),('J',11,131),('J',12,131),('J',13,131),('J',14,131),('J',15,131),('J',16,131),('J',17,131),('J',18,131),('J',19,131),('J',20,131),('A',1,141),('A',2,141),('A',3,141),('A',4,141),('A',5,141),('A',6,141),('A',7,141),('A',8,141),('A',9,141),('A',10,141),('A',11,141),('A',12,141),('A',13,141),('A',14,141),('A',15,141),('A',16,141),('A',17,141),('A',18,141),('A',19,141),('A',20,141),('B',1,141),('B',2,141),('B',3,141),('B',4,141),('B',5,141),('B',6,141),('B',7,141),('B',8,141),('B',9,141),('B',10,141),('B',11,141),('B',12,141),('B',13,141),('B',14,141),('B',15,141),('B',16,141),('B',17,141),('B',18,141),('B',19,141),('B',20,141),('C',1,141),('C',2,141),('C',3,141),('C',4,141),('C',5,141),('C',6,141),('C',7,141),('C',8,141),('C',9,141),('C',10,141),('C',11,141),('C',12,141),('C',13,141),('C',14,141),('C',15,141),('C',16,141),('C',17,141),('C',18,141),('C',19,141),('C',20,141),('D',1,141),('D',2,141),('D',3,141),('D',4,141),('D',5,141),('D',6,141),('D',7,141),('D',8,141),('D',9,141),('D',10,141),('D',11,141),('D',12,141),('D',13,141),('D',14,141),('D',15,141),('D',16,141),('D',17,141),('D',18,141),('D',19,141),('D',20,141),('E',1,141),('E',2,141),('E',3,141),('E',4,141),('E',5,141),('E',6,141),('E',7,141),('E',8,141),('E',9,141),('E',10,141),('E',11,141),('E',12,141),('E',13,141),('E',14,141),('E',15,141),('E',16,141),('E',17,141),('E',18,141),('E',19,141),('E',20,141),('F',1,141),('F',2,141),('F',3,141),('F',4,141),('F',5,141),('F',6,141),('F',7,141),('F',8,141),('F',9,141),('F',10,141),('F',11,141),('F',12,141),('F',13,141),('F',14,141),('F',15,141),('F',16,141),('F',17,141),('F',18,141),('F',19,141),('F',20,141),('G',1,141),('G',2,141),('G',3,141),('G',4,141),('G',5,141),('G',6,141),('G',7,141),('G',8,141),('G',9,141),('G',10,141),('G',11,141),('G',12,141),('G',13,141),('G',14,141),('G',15,141),('G',16,141),('G',17,141),('G',18,141),('G',19,141),('G',20,141),('H',1,141),('H',2,141),('H',3,141),('H',4,141),('H',5,141),('H',6,141),('H',7,141),('H',8,141),('H',9,141),('H',10,141),('H',11,141),('H',12,141),('H',13,141),('H',14,141),('H',15,141),('H',16,141),('H',17,141),('H',18,141),('H',19,141),('H',20,141),('I',1,141),('I',2,141),('I',3,141),('I',4,141),('I',5,141),('I',6,141),('I',7,141),('I',8,141),('I',9,141),('I',10,141),('I',11,141),('I',12,141),('I',13,141),('I',14,141),('I',15,141),('I',16,141),('I',17,141),('I',18,141),('I',19,141),('I',20,141),('J',1,141),('J',2,141),('J',3,141),('J',4,141),('J',5,141),('J',6,141),('J',7,141),('J',8,141),('J',9,141),('J',10,141),('J',11,141),('J',12,141),('J',13,141),('J',14,141),('J',15,141),('J',16,141),('J',17,141),('J',18,141),('J',19,141),('J',20,141),('A',1,151),('A',2,151),('A',3,151),('A',4,151),('A',5,151),('A',6,151),('A',7,151),('A',8,151),('A',9,151),('A',10,151),('A',11,151),('A',12,151),('A',13,151),('A',14,151),('A',15,151),('A',16,151),('A',17,151),('A',18,151),('A',19,151),('A',20,151),('B',1,151),('B',2,151),('B',3,151),('B',4,151),('B',5,151),('B',6,151),('B',7,151),('B',8,151),('B',9,151),('B',10,151),('B',11,151),('B',12,151),('B',13,151),('B',14,151),('B',15,151),('B',16,151),('B',17,151),('B',18,151),('B',19,151),('B',20,151),('C',1,151),('C',2,151),('C',3,151),('C',4,151),('C',5,151),('C',6,151),('C',7,151),('C',8,151),('C',9,151),('C',10,151),('C',11,151),('C',12,151),('C',13,151),('C',14,151),('C',15,151),('C',16,151),('C',17,151),('C',18,151),('C',19,151),('C',20,151),('D',1,151),('D',2,151),('D',3,151),('D',4,151),('D',5,151),('D',6,151),('D',7,151),('D',8,151),('D',9,151),('D',10,151),('D',11,151),('D',12,151),('D',13,151),('D',14,151),('D',15,151),('D',16,151),('D',17,151),('D',18,151),('D',19,151),('D',20,151),('E',1,151),('E',2,151),('E',3,151),('E',4,151),('E',5,151),('E',6,151),('E',7,151),('E',8,151),('E',9,151),('E',10,151),('E',11,151),('E',12,151),('E',13,151),('E',14,151),('E',15,151),('E',16,151),('E',17,151),('E',18,151),('E',19,151),('E',20,151),('F',1,151),('F',2,151),('F',3,151),('F',4,151),('F',5,151),('F',6,151),('F',7,151),('F',8,151),('F',9,151),('F',10,151),('F',11,151),('F',12,151),('F',13,151),('F',14,151),('F',15,151),('F',16,151),('F',17,151),('F',18,151),('F',19,151),('F',20,151),('G',1,151),('G',2,151),('G',3,151),('G',4,151),('G',5,151),('G',6,151),('G',7,151),('G',8,151),('G',9,151),('G',10,151),('G',11,151),('G',12,151),('G',13,151),('G',14,151),('G',15,151),('G',16,151),('G',17,151),('G',18,151),('G',19,151),('G',20,151),('H',1,151),('H',2,151),('H',3,151),('H',4,151),('H',5,151),('H',6,151),('H',7,151),('H',8,151),('H',9,151),('H',10,151),('H',11,151),('H',12,151),('H',13,151),('H',14,151),('H',15,151),('H',16,151),('H',17,151),('H',18,151),('H',19,151),('H',20,151),('I',1,151),('I',2,151),('I',3,151),('I',4,151),('I',5,151),('I',6,151),('I',7,151),('I',8,151),('I',9,151),('I',10,151),('I',11,151),('I',12,151),('I',13,151),('I',14,151),('I',15,151),('I',16,151),('I',17,151),('I',18,151),('I',19,151),('I',20,151),('J',1,151),('J',2,151),('J',3,151),('J',4,151),('J',5,151),('J',6,151),('J',7,151),('J',8,151),('J',9,151),('J',10,151),('J',11,151),('J',12,151),('J',13,151),('J',14,151),('J',15,151),('J',16,151),('J',17,151),('J',18,151),('J',19,151),('J',20,151),('A',1,161),('A',2,161),('A',3,161),('A',4,161),('A',5,161),('A',6,161),('A',7,161),('A',8,161),('A',9,161),('A',10,161),('A',11,161),('A',12,161),('A',13,161),('A',14,161),('A',15,161),('A',16,161),('A',17,161),('A',18,161),('A',19,161),('A',20,161),('B',1,161),('B',2,161),('B',3,161),('B',4,161),('B',5,161),('B',6,161),('B',7,161),('B',8,161),('B',9,161),('B',10,161),('B',11,161),('B',12,161),('B',13,161),('B',14,161),('B',15,161),('B',16,161),('B',17,161),('B',18,161),('B',19,161),('B',20,161),('C',1,161),('C',2,161),('C',3,161),('C',4,161),('C',5,161),('C',6,161),('C',7,161),('C',8,161),('C',9,161),('C',10,161),('C',11,161),('C',12,161),('C',13,161),('C',14,161),('C',15,161),('C',16,161),('C',17,161),('C',18,161),('C',19,161),('C',20,161),('D',1,161),('D',2,161),('D',3,161),('D',4,161),('D',5,161),('D',6,161),('D',7,161),('D',8,161),('D',9,161),('D',10,161),('D',11,161),('D',12,161),('D',13,161),('D',14,161),('D',15,161),('D',16,161),('D',17,161),('D',18,161),('D',19,161),('D',20,161),('E',1,161),('E',2,161),('E',3,161),('E',4,161),('E',5,161),('E',6,161),('E',7,161),('E',8,161),('E',9,161),('E',10,161),('E',11,161),('E',12,161),('E',13,161),('E',14,161),('E',15,161),('E',16,161),('E',17,161),('E',18,161),('E',19,161),('E',20,161),('F',1,161),('F',2,161),('F',3,161),('F',4,161),('F',5,161),('F',6,161),('F',7,161),('F',8,161),('F',9,161),('F',10,161),('F',11,161),('F',12,161),('F',13,161),('F',14,161),('F',15,161),('F',16,161),('F',17,161),('F',18,161),('F',19,161),('F',20,161),('G',1,161),('G',2,161),('G',3,161),('G',4,161),('G',5,161),('G',6,161),('G',7,161),('G',8,161),('G',9,161),('G',10,161),('G',11,161),('G',12,161),('G',13,161),('G',14,161),('G',15,161),('G',16,161),('G',17,161),('G',18,161),('G',19,161),('G',20,161),('H',1,161),('H',2,161),('H',3,161),('H',4,161),('H',5,161),('H',6,161),('H',7,161),('H',8,161),('H',9,161),('H',10,161),('H',11,161),('H',12,161),('H',13,161),('H',14,161),('H',15,161),('H',16,161),('H',17,161),('H',18,161),('H',19,161),('H',20,161),('I',1,161),('I',2,161),('I',3,161),('I',4,161),('I',5,161),('I',6,161),('I',7,161),('I',8,161),('I',9,161),('I',10,161),('I',11,161),('I',12,161),('I',13,161),('I',14,161),('I',15,161),('I',16,161),('I',17,161),('I',18,161),('I',19,161),('I',20,161),('J',1,161),('J',2,161),('J',3,161),('J',4,161),('J',5,161),('J',6,161),('J',7,161),('J',8,161),('J',9,161),('J',10,161),('J',11,161),('J',12,161),('J',13,161),('J',14,161),('J',15,161),('J',16,161),('J',17,161),('J',18,161),('J',19,161),('J',20,161),('A',1,171),('A',2,171),('A',3,171),('A',4,171),('A',5,171),('A',6,171),('A',7,171),('A',8,171),('A',9,171),('A',10,171),('A',11,171),('A',12,171),('A',13,171),('A',14,171),('A',15,171),('A',16,171),('A',17,171),('A',18,171),('A',19,171),('A',20,171),('B',1,171),('B',2,171),('B',3,171),('B',4,171),('B',5,171),('B',6,171),('B',7,171),('B',8,171),('B',9,171),('B',10,171),('B',11,171),('B',12,171),('B',13,171),('B',14,171),('B',15,171),('B',16,171),('B',17,171),('B',18,171),('B',19,171),('B',20,171),('C',1,171),('C',2,171),('C',3,171),('C',4,171),('C',5,171),('C',6,171),('C',7,171),('C',8,171),('C',9,171),('C',10,171),('C',11,171),('C',12,171),('C',13,171),('C',14,171),('C',15,171),('C',16,171),('C',17,171),('C',18,171),('C',19,171),('C',20,171),('D',1,171),('D',2,171),('D',3,171),('D',4,171),('D',5,171),('D',6,171),('D',7,171),('D',8,171),('D',9,171),('D',10,171),('D',11,171),('D',12,171),('D',13,171),('D',14,171),('D',15,171),('D',16,171),('D',17,171),('D',18,171),('D',19,171),('D',20,171),('E',1,171),('E',2,171),('E',3,171),('E',4,171),('E',5,171),('E',6,171),('E',7,171),('E',8,171),('E',9,171),('E',10,171),('E',11,171),('E',12,171),('E',13,171),('E',14,171),('E',15,171),('E',16,171),('E',17,171),('E',18,171),('E',19,171),('E',20,171),('F',1,171),('F',2,171),('F',3,171),('F',4,171),('F',5,171),('F',6,171),('F',7,171),('F',8,171),('F',9,171),('F',10,171),('F',11,171),('F',12,171),('F',13,171),('F',14,171),('F',15,171),('F',16,171),('F',17,171),('F',18,171),('F',19,171),('F',20,171),('G',1,171),('G',2,171),('G',3,171),('G',4,171),('G',5,171),('G',6,171),('G',7,171),('G',8,171),('G',9,171),('G',10,171),('G',11,171),('G',12,171),('G',13,171),('G',14,171),('G',15,171),('G',16,171),('G',17,171),('G',18,171),('G',19,171),('G',20,171),('H',1,171),('H',2,171),('H',3,171),('H',4,171),('H',5,171),('H',6,171),('H',7,171),('H',8,171),('H',9,171),('H',10,171),('H',11,171),('H',12,171),('H',13,171),('H',14,171),('H',15,171),('H',16,171),('H',17,171),('H',18,171),('H',19,171),('H',20,171),('I',1,171),('I',2,171),('I',3,171),('I',4,171),('I',5,171),('I',6,171),('I',7,171),('I',8,171),('I',9,171),('I',10,171),('I',11,171),('I',12,171),('I',13,171),('I',14,171),('I',15,171),('I',16,171),('I',17,171),('I',18,171),('I',19,171),('I',20,171),('J',1,171),('J',2,171),('J',3,171),('J',4,171),('J',5,171),('J',6,171),('J',7,171),('J',8,171),('J',9,171),('J',10,171),('J',11,171),('J',12,171),('J',13,171),('J',14,171),('J',15,171),('J',16,171),('J',17,171),('J',18,171),('J',19,171),('J',20,171),('A',1,181),('A',2,181),('A',3,181),('A',4,181),('A',5,181),('A',6,181),('A',7,181),('A',8,181),('A',9,181),('A',10,181),('A',11,181),('A',12,181),('A',13,181),('A',14,181),('A',15,181),('A',16,181),('A',17,181),('A',18,181),('A',19,181),('A',20,181),('B',1,181),('B',2,181),('B',3,181),('B',4,181),('B',5,181),('B',6,181),('B',7,181),('B',8,181),('B',9,181),('B',10,181),('B',11,181),('B',12,181),('B',13,181),('B',14,181),('B',15,181),('B',16,181),('B',17,181),('B',18,181),('B',19,181),('B',20,181),('C',1,181),('C',2,181),('C',3,181),('C',4,181),('C',5,181),('C',6,181),('C',7,181),('C',8,181),('C',9,181),('C',10,181),('C',11,181),('C',12,181),('C',13,181),('C',14,181),('C',15,181),('C',16,181),('C',17,181),('C',18,181),('C',19,181),('C',20,181),('D',1,181),('D',2,181),('D',3,181),('D',4,181),('D',5,181),('D',6,181),('D',7,181),('D',8,181),('D',9,181),('D',10,181),('D',11,181),('D',12,181),('D',13,181),('D',14,181),('D',15,181),('D',16,181),('D',17,181),('D',18,181),('D',19,181),('D',20,181),('E',1,181),('E',2,181),('E',3,181),('E',4,181),('E',5,181),('E',6,181),('E',7,181),('E',8,181),('E',9,181),('E',10,181),('E',11,181),('E',12,181),('E',13,181),('E',14,181),('E',15,181),('E',16,181),('E',17,181),('E',18,181),('E',19,181),('E',20,181),('F',1,181),('F',2,181),('F',3,181),('F',4,181),('F',5,181),('F',6,181),('F',7,181),('F',8,181),('F',9,181),('F',10,181),('F',11,181),('F',12,181),('F',13,181),('F',14,181),('F',15,181),('F',16,181),('F',17,181),('F',18,181),('F',19,181),('F',20,181),('G',1,181),('G',2,181),('G',3,181),('G',4,181),('G',5,181),('G',6,181),('G',7,181),('G',8,181),('G',9,181),('G',10,181),('G',11,181),('G',12,181),('G',13,181),('G',14,181),('G',15,181),('G',16,181),('G',17,181),('G',18,181),('G',19,181),('G',20,181),('H',1,181),('H',2,181),('H',3,181),('H',4,181),('H',5,181),('H',6,181),('H',7,181),('H',8,181),('H',9,181),('H',10,181),('H',11,181),('H',12,181),('H',13,181),('H',14,181),('H',15,181),('H',16,181),('H',17,181),('H',18,181),('H',19,181),('H',20,181),('I',1,181),('I',2,181),('I',3,181),('I',4,181),('I',5,181),('I',6,181),('I',7,181),('I',8,181),('I',9,181),('I',10,181),('I',11,181),('I',12,181),('I',13,181),('I',14,181),('I',15,181),('I',16,181),('I',17,181),('I',18,181),('I',19,181),('I',20,181),('J',1,181),('J',2,181),('J',3,181),('J',4,181),('J',5,181),('J',6,181),('J',7,181),('J',8,181),('J',9,181),('J',10,181),('J',11,181),('J',12,181),('J',13,181),('J',14,181),('J',15,181),('J',16,181),('J',17,181),('J',18,181),('J',19,181),('J',20,181);
/*!40000 ALTER TABLE `seat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `showtime`
--

DROP TABLE IF EXISTS `showtime`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `showtime` (
  `Hall_HallId` int(11) NOT NULL,
  `Hall_Theater_TheaterId` int(11) NOT NULL,
  `Movie_MovieId` int(11) NOT NULL,
  `ShowTimeId` int(11) NOT NULL AUTO_INCREMENT,
  `ShowTimeDate` date NOT NULL,
  `ShowTimeTime` time NOT NULL,
  `MovieTypeId` int(11) NOT NULL,
  PRIMARY KEY (`ShowTimeId`,`Hall_HallId`,`Hall_Theater_TheaterId`,`Movie_MovieId`),
  KEY `fk_Hall_has_Movie_Movie1_idx` (`Movie_MovieId`),
  KEY `fk_ShowTime_MovieType1_idx` (`MovieTypeId`),
  KEY `fk_Hall_has_Movie_Hall1_idx` (`Hall_HallId`),
  CONSTRAINT `fk_ShowTime_MovieType1` FOREIGN KEY (`MovieTypeId`) REFERENCES `movietype` (`MovieTypeId`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1692 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `showtime`
--

LOCK TABLES `showtime` WRITE;
/*!40000 ALTER TABLE `showtime` DISABLE KEYS */;
INSERT INTO `showtime` VALUES (151,91,1101,1621,'2016-02-11','19:20:00',1),(151,91,1101,1631,'2016-02-12','23:45:00',11),(151,91,1101,1641,'2016-02-12','14:55:00',21),(151,91,1101,1651,'2016-02-12','20:15:00',31),(161,3,1101,1661,'2016-02-10','12:45:00',1),(161,3,1101,1671,'2016-02-20','18:30:00',11),(141,1,1101,1681,'2016-02-12','13:30:00',1),(151,91,1,1691,'2016-02-02','08:50:00',11);
/*!40000 ALTER TABLE `showtime` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff` (
  `StaffId` int(11) NOT NULL AUTO_INCREMENT,
  `Email` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `CreateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`StaffId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff`
--

LOCK TABLES `staff` WRITE;
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;
INSERT INTO `staff` VALUES (1,'stein121@gmail.com','121','2015-11-20 16:36:30'),(2,'root@root.root','password','2015-11-20 16:36:30');
/*!40000 ALTER TABLE `staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_manages_movie`
--

DROP TABLE IF EXISTS `staff_manages_movie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff_manages_movie` (
  `Staff_StaffId` int(11) NOT NULL,
  `Movie_MovieId` int(11) NOT NULL,
  `UpdatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Staff_StaffId`,`Movie_MovieId`),
  KEY `fk_Staff_has_Movie_Movie1_idx` (`Movie_MovieId`),
  KEY `fk_Staff_has_Movie_Staff1_idx` (`Staff_StaffId`),
  CONSTRAINT `fk_Staff_has_Movie_Movie1` FOREIGN KEY (`Movie_MovieId`) REFERENCES `movie` (`MovieId`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_Staff_has_Movie_Staff1` FOREIGN KEY (`Staff_StaffId`) REFERENCES `staff` (`StaffId`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_manages_movie`
--

LOCK TABLES `staff_manages_movie` WRITE;
/*!40000 ALTER TABLE `staff_manages_movie` DISABLE KEYS */;
/*!40000 ALTER TABLE `staff_manages_movie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_manages_showtime`
--

DROP TABLE IF EXISTS `staff_manages_showtime`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff_manages_showtime` (
  `Staff_StaffId` int(11) NOT NULL,
  `ShowTime_ShowTimeId` int(11) NOT NULL,
  `UpdatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Staff_StaffId`,`ShowTime_ShowTimeId`),
  KEY `fk_Staff_has_ShowTime_ShowTime1_idx` (`ShowTime_ShowTimeId`),
  KEY `fk_Staff_has_ShowTime_Staff1_idx` (`Staff_StaffId`),
  CONSTRAINT `fk_Staff_has_ShowTime_ShowTime1` FOREIGN KEY (`ShowTime_ShowTimeId`) REFERENCES `showtime` (`ShowTimeId`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_Staff_has_ShowTime_Staff1` FOREIGN KEY (`Staff_StaffId`) REFERENCES `staff` (`StaffId`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_manages_showtime`
--

LOCK TABLES `staff_manages_showtime` WRITE;
/*!40000 ALTER TABLE `staff_manages_showtime` DISABLE KEYS */;
/*!40000 ALTER TABLE `staff_manages_showtime` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `theater`
--

DROP TABLE IF EXISTS `theater`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `theater` (
  `TheaterId` int(11) NOT NULL AUTO_INCREMENT,
  `TheaterName` varchar(255) NOT NULL,
  PRIMARY KEY (`TheaterId`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `theater`
--

LOCK TABLES `theater` WRITE;
/*!40000 ALTER TABLE `theater` DISABLE KEYS */;
INSERT INTO `theater` VALUES (1,'SP VivoCity'),(2,'SP Shangri-La'),(3,'SP Capitol'),(91,'ArcLight'),(101,'Arclight The Cathay');
/*!40000 ALTER TABLE `theater` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'eadassignment'
--
/*!50003 DROP PROCEDURE IF EXISTS `add_hall` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b05052488fa3ed`@`%` PROCEDURE `add_hall`(IN theaterId INT, IN hallNo INT)
BEGIN

INSERT INTO hall (Theater_TheaterId, HallNo)
VALUES
(theaterId, HallNo);
SET @hallId = LAST_INSERT_ID();

INSERT INTO seat (`Row`, `SeatNumber`,`Hall_HallId`) VALUES 
('A', 1, @hallId), ('A', 2, @hallId), ('A', 3, @hallId), ('A', 4, @hallId), ('A', 5, @hallId), ('A', 6, @hallId), ('A', 7, @hallId), ('A', 8, @hallId), ('A', 9, @hallId), ('A', 10, @hallId), ('A', 11, @hallId), ('A', 12, @hallId), ('A', 13, @hallId), ('A', 14, @hallId), ('A', 15, @hallId), ('A', 16, @hallId), ('A', 17, @hallId), ('A', 18, @hallId), ('A', 19, @hallId), ('A', 20, @hallId), 
('B', 1, @hallId), ('B', 2, @hallId), ('B', 3, @hallId), ('B', 4, @hallId), ('B', 5, @hallId), ('B', 6, @hallId), ('B', 7, @hallId), ('B', 8, @hallId), ('B', 9, @hallId), ('B', 10, @hallId), ('B', 11, @hallId), ('B', 12, @hallId), ('B', 13, @hallId), ('B', 14, @hallId), ('B', 15, @hallId), ('B', 16, @hallId), ('B', 17, @hallId), ('B', 18, @hallId), ('B', 19, @hallId), ('B', 20, @hallId), 
('C', 1, @hallId), ('C', 2, @hallId), ('C', 3, @hallId), ('C', 4, @hallId), ('C', 5, @hallId), ('C', 6, @hallId), ('C', 7, @hallId), ('C', 8, @hallId), ('C', 9, @hallId), ('C', 10, @hallId), ('C', 11, @hallId), ('C', 12, @hallId), ('C', 13, @hallId), ('C', 14, @hallId), ('C', 15, @hallId), ('C', 16, @hallId), ('C', 17, @hallId), ('C', 18, @hallId), ('C', 19, @hallId), ('C', 20, @hallId), 
('D', 1, @hallId), ('D', 2, @hallId), ('D', 3, @hallId), ('D', 4, @hallId), ('D', 5, @hallId), ('D', 6, @hallId), ('D', 7, @hallId), ('D', 8, @hallId), ('D', 9, @hallId), ('D', 10, @hallId), ('D', 11, @hallId), ('D', 12, @hallId), ('D', 13, @hallId), ('D', 14, @hallId), ('D', 15, @hallId), ('D', 16, @hallId), ('D', 17, @hallId), ('D', 18, @hallId), ('D', 19, @hallId), ('D', 20, @hallId), 
('E', 1, @hallId), ('E', 2, @hallId), ('E', 3, @hallId), ('E', 4, @hallId), ('E', 5, @hallId), ('E', 6, @hallId), ('E', 7, @hallId), ('E', 8, @hallId), ('E', 9, @hallId), ('E', 10, @hallId), ('E', 11, @hallId), ('E', 12, @hallId), ('E', 13, @hallId), ('E', 14, @hallId), ('E', 15, @hallId), ('E', 16, @hallId), ('E', 17, @hallId), ('E', 18, @hallId), ('E', 19, @hallId), ('E', 20, @hallId), 
('F', 1, @hallId), ('F', 2, @hallId), ('F', 3, @hallId), ('F', 4, @hallId), ('F', 5, @hallId), ('F', 6, @hallId), ('F', 7, @hallId), ('F', 8, @hallId), ('F', 9, @hallId), ('F', 10, @hallId), ('F', 11, @hallId), ('F', 12, @hallId), ('F', 13, @hallId), ('F', 14, @hallId), ('F', 15, @hallId), ('F', 16, @hallId), ('F', 17, @hallId), ('F', 18, @hallId), ('F', 19, @hallId), ('F', 20, @hallId), 
('G', 1, @hallId), ('G', 2, @hallId), ('G', 3, @hallId), ('G', 4, @hallId), ('G', 5, @hallId), ('G', 6, @hallId), ('G', 7, @hallId), ('G', 8, @hallId), ('G', 9, @hallId), ('G', 10, @hallId), ('G', 11, @hallId), ('G', 12, @hallId), ('G', 13, @hallId), ('G', 14, @hallId), ('G', 15, @hallId), ('G', 16, @hallId), ('G', 17, @hallId), ('G', 18, @hallId), ('G', 19, @hallId), ('G', 20, @hallId), 
('H', 1, @hallId), ('H', 2, @hallId), ('H', 3, @hallId), ('H', 4, @hallId), ('H', 5, @hallId), ('H', 6, @hallId), ('H', 7, @hallId), ('H', 8, @hallId), ('H', 9, @hallId), ('H', 10, @hallId), ('H', 11, @hallId), ('H', 12, @hallId), ('H', 13, @hallId), ('H', 14, @hallId), ('H', 15, @hallId), ('H', 16, @hallId), ('H', 17, @hallId), ('H', 18, @hallId), ('H', 19, @hallId), ('H', 20, @hallId), 
('I', 1, @hallId), ('I', 2, @hallId), ('I', 3, @hallId), ('I', 4, @hallId), ('I', 5, @hallId), ('I', 6, @hallId), ('I', 7, @hallId), ('I', 8, @hallId), ('I', 9, @hallId), ('I', 10, @hallId), ('I', 11, @hallId), ('I', 12, @hallId), ('I', 13, @hallId), ('I', 14, @hallId), ('I', 15, @hallId), ('I', 16, @hallId), ('I', 17, @hallId), ('I', 18, @hallId), ('I', 19, @hallId), ('I', 20, @hallId), 
('J', 1, @hallId), ('J', 2, @hallId), ('J', 3, @hallId), ('J', 4, @hallId), ('J', 5, @hallId), ('J', 6, @hallId), ('J', 7, @hallId), ('J', 8, @hallId), ('J', 9, @hallId), ('J', 10, @hallId), ('J', 11, @hallId), ('J', 12, @hallId), ('J', 13, @hallId), ('J', 14, @hallId), ('J', 15, @hallId), ('J', 16, @hallId), ('J', 17, @hallId), ('J', 18, @hallId), ('J', 19, @hallId), ('J', 20, @hallId);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_movie_details` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b05052488fa3ed`@`%` PROCEDURE `get_movie_details`(IN mName varchar(500))
BEGIN
SELECT movieId  
FROM movie 
WHERE movieName = mName INTO @movId;

SELECT MovieId, MovieName, ReleaseDate, EndDate, Sypnosis, Duration, ImageURL 
FROM movie
WHERE movieId = @movId
ORDER BY ReleaseDate;

SELECT a.actorid, a.actorname 
FROM movie 
INNER JOIN movie_has_actor ma 
ON ma.movie_movieid = movieid 
INNER JOIN actor a 
ON ma.actor_actorid = actorid 
WHERE movieid = @movId AND a._IsDeleted = 0;

SELECT g.genreid, g.genrename 
FROM movie 
INNER JOIN movie_has_genre mg 
ON mg.movie_movieid = movieid 
INNER JOIN genre g 
ON mg.genre_genreid = g.genreid 
WHERE movieid = @movId AND g._IsDeleted = 0;

SELECT *  
FROM movie
INNER JOIN showtime s
ON s.movie_movieid = movieid
WHERE movieid = @movId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_movie_details_by_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b05052488fa3ed`@`%` PROCEDURE `get_movie_details_by_id`(IN id INT)
BEGIN
SELECT movieId  
FROM movie 
WHERE movieId = id INTO @movId;

SELECT MovieId, MovieName, ReleaseDate, EndDate, Sypnosis, Duration, ImageURL 
FROM movie
WHERE movieId = @movId;

SELECT a.actorid, a.actorname 
FROM movie 
INNER JOIN movie_has_actor ma 
ON ma.movie_movieid = movieid 
INNER JOIN actor a 
ON ma.actor_actorid = actorid 
WHERE movieid = @movId AND a._IsDeleted = 0;

SELECT g.genreid, g.genrename 
FROM movie 
INNER JOIN movie_has_genre mg 
ON mg.movie_movieid = movieid 
INNER JOIN genre g 
ON mg.genre_genreid = g.genreid 
WHERE movieid = @movId AND g._IsDeleted = 0;

SELECT *  
FROM movie
INNER JOIN showtime s
ON s.movie_movieid = movieid
WHERE movieid = @movId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-30 16:46:14
