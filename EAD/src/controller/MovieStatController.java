package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.Movie;
import mysqlDatabase.MovieManager;
import mysqlDatabase.MovieTicketManager;
import viewModel.MonthYearViewModel;
import viewModel.MovieViewModel;

/**
 * Servlet implementation class MovieStatController
 */
@WebServlet("/Admin/MovieStat")
public class MovieStatController extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("In movie stat controller (get request)");
		
//		String[] movieStatUrl = request.getRequestURI().split("/");
//		
//		if (!movieStatUrl[movieStatUrl.length - 1].equals("MovieStat")) {
//			return;
//		}
		
		System.out.println(request.getRequestURI());
		
		MovieTicketManager mtm = new MovieTicketManager();
		ArrayList<MonthYearViewModel> availDates = null;
		try {
			availDates = mtm.getMovieTicStatDateTime();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		request.setAttribute("dates", availDates);
		RequestDispatcher rd = request.getRequestDispatcher("AdminStats.jsp?status=true");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("In movie stat controller (post request)");
		String reqParam;
		ArrayList<MovieViewModel> movies = null;
		try {
			reqParam = request.getParameter("mDate"); 
			Gson gson = new GsonBuilder().create();
			MonthYearViewModel myvm = gson.fromJson(reqParam, MonthYearViewModel.class);
			MovieTicketManager mtm = new MovieTicketManager();
			movies = mtm.getTopTenMovies(Integer.parseInt(myvm.getSqlParamMonth())
					, Integer.parseInt(myvm.getSqlParamYear()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Gson gson = new Gson();
		String jsonText = gson.toJson(movies);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().write(jsonText);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
