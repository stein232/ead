package controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.braintreegateway.BraintreeGateway;
import com.braintreegateway.Result;
import com.braintreegateway.Transaction;
import com.braintreegateway.TransactionRequest;
import com.google.gson.Gson;

import model.MovieTicket;
import model.MovieType;
import mysqlDatabase.MovieManager;
import mysqlDatabase.MovieTicketManager;
import mysqlDatabase.MovieTypeManager;
import mysqlDatabase.ShowTimeManager;
import viewModel.CartViewModel;

@WebServlet("/Cart")
public class CartController extends HttpServlet {
	
	//getting the list of movie tickets in the session cart
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		
		if (session.getAttribute("member") == null) {
			resp.sendRedirect("/?loggedIn=false");
			return;
		}
		
		if (session.getAttribute("cvms") == null) {
			session.setAttribute("cvms", new ArrayList<CartViewModel>());
		}
		
		if (((ArrayList<CartViewModel>)session.getAttribute("cvms")).size() == 0) {
			RequestDispatcher rd = req.getRequestDispatcher("/EmptyCart.jsp");
			rd.forward(req, resp);
			return;
		}
		
		RequestDispatcher rd = req.getRequestDispatcher("/Cart.jsp");
		rd.forward(req, resp);
	}
	
	//buying the movie tickets
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//checkLogin(req);
		String a = "";
		
		if (req.getParameter("buy") != null) {
		    buy(req, resp);
		} else if (req.getParameter("delete") != null) {
		    delete(req, resp);
		} else if (req.getParameter("checkout") != null) {
			checkout(req, resp);
		} else if (req.getParameter("paypal") != null) {
			paypal(req,resp);
		}
	}

	private void paypal(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		BraintreeGateway gateway = new BraintreeGateway("access_token$sandbox$9rrjx472zz84pfqx$873207db33fb44f86fb5947c923cbcdc");
		String nonce = req.getParameter("payment_method_nonce");
		
		if (nonce == null) {
			resp.sendRedirect("/Cart");
		}
		
		String[] selectionsString = req.getParameterValues("cvmSelections");
		int[] selections = new int[selectionsString.length];
		
		for (int i = 0; i < selectionsString.length; i++) {
			selections[i] = Integer.parseInt(selectionsString[i]);
		}
		HttpSession session = req.getSession();
		
		ArrayList<CartViewModel> cvms = (ArrayList<CartViewModel>)session.getAttribute("cvms");
		
		BigDecimal amount = new BigDecimal(0);
		for (int i = 0; i < selections.length; i++) {
			ArrayList<MovieTicket> movieTickets =  cvms.get(selections[i]).getMovieTickets();
			for (int j = 0; j < movieTickets.size(); j++) {
				amount =  amount.add(new BigDecimal(movieTickets.get(j).getPrice()));
			}
		}
		
		TransactionRequest transactionRequest = new TransactionRequest()
				.amount(amount)
				.paymentMethodNonce(nonce)
				.orderId("" + cvms.get(selections[0]).getMovieTickets().get(0).hashCode())
				.descriptor()
					.name("Arc*Movieticket")
					.done();
		
		Result<Transaction> saleResult = gateway.transaction().sale(transactionRequest);
		if (saleResult.isSuccess()) {
			Transaction transaction = saleResult.getTarget();
			System.out.println("Success ID: " + transaction.getId());
		} else {
			System.out.println("Message: " + saleResult.getMessage());
			return;
		}
		
		buy(req, resp);
	}

	private void checkout(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		String[] selectionsString = req.getParameterValues("cvmSelections");
		
		if (selectionsString == null) {
			resp.sendRedirect("/Cart");
			return;
		}
		
		int[] selections = new int[selectionsString.length];
		
		for (int i = 0; i < selectionsString.length; i++) {
			selections[i] = Integer.parseInt(selectionsString[i]);
		}
		HttpSession session = req.getSession();
		
		ArrayList<CartViewModel> cvms = (ArrayList<CartViewModel>)session.getAttribute("cvms");
		if (cvms == null) {
			resp.sendRedirect("/Cart");
			return;
		}
		
		ArrayList<CartViewModel> selectedCvms = new ArrayList<CartViewModel>();
		for (int i = 0; i < selections.length; i++) {
			selectedCvms.add(cvms.get(i));
		}
		
		//get client token
		BraintreeGateway gateway = new BraintreeGateway("access_token$sandbox$9rrjx472zz84pfqx$873207db33fb44f86fb5947c923cbcdc");
		
		req.setAttribute("selections", selections);
		req.setAttribute("selectedCvms", selectedCvms);
		req.setAttribute("braintreeClientToken", gateway.clientToken().generate());
		req.getRequestDispatcher("/ConfirmCheckout.jsp").forward(req, resp);
	}

	private void buy(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String[] selectionsString = req.getParameterValues("cvmSelections");
		
		if (selectionsString == null) {
			resp.sendRedirect("/Cart");
			return;
		}
		
		int[] selections = new int[selectionsString.length];
		
		for (int i = 0; i < selectionsString.length; i++) {
			selections[i] = Integer.parseInt(selectionsString[i]);
		}
		HttpSession session = req.getSession();
		
		ArrayList<CartViewModel> cvms = (ArrayList<CartViewModel>)session.getAttribute("cvms");
		if (cvms == null) {
			resp.sendRedirect("/Cart");
			return;
		}
		
		
		ArrayList<MovieTicket> bookekMovieTickets;
		
		try {
			bookekMovieTickets = new MovieTicketManager().getAllMovieTicketsOfShowTime(cvms.get(0).getMovieTickets().get(0).getShowTimeId());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
			e.printStackTrace();
			return;
		}
		
		int invalidCvms = 0;
		
		//check if tickets are booked
		for (int i = selections.length - 1; i >= 0; i--) {
			ArrayList<MovieTicket> movieTickets = cvms.get(i).getMovieTickets();
			for (int j = 0; j < movieTickets.size(); j++) {
				if (bookekMovieTickets.contains(movieTickets.get(j))) {
					invalidCvms++;
					cvms.remove(i);
					break;
				}
			}
		}
		
		if (invalidCvms > 0) {
			resp.sendRedirect("/Cart?status=Selected seats are now booked");
			return;
		}
		
		//buy tickets
		MovieTicketManager mtm = new MovieTicketManager();
		for (int i = 0; i < selections.length; i++) {
			ArrayList<MovieTicket> movieTickets = cvms.get(i).getMovieTickets();
			for (int j = 0; j < movieTickets.size(); j++) {
				try {
					mtm.addMovieTicket(movieTickets.get(j));
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
					return;
				}
			}
			
		}
		
		//delete the selected cvms from the session
		//delete backwards to avoid change in index of elements
		for (int i = selections.length -1 ; i >= 0; i--) {
			cvms.remove(selections[i]);
		}
		
		session.setAttribute("cvms", cvms);
		resp.sendRedirect("/?booking=success");
	}
	
	private void delete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String[] selectionsString = req.getParameterValues("cvmSelections");
		
		if (selectionsString == null) {
			resp.sendRedirect("/Cart");
			return;
		}
		
		int[] selections = new int[selectionsString.length];
		
		for (int i = 0; i < selectionsString.length; i++) {
			selections[i] = Integer.parseInt(selectionsString[i]);
		}
		
		HttpSession session = req.getSession();
		
		ArrayList<CartViewModel> cvms = (ArrayList<CartViewModel>)session.getAttribute("cvms");
		if (cvms == null) {
			resp.sendRedirect("/Cart");
			return;
		}
		
		//delete the selected cvms from the session
		//delete backwards to avoid change in index of elements
		for (int i = selections.length -1 ; i >= 0; i--) {
			cvms.remove(selections[i]);
		}
		
		session.setAttribute("cvms", cvms);
		resp.sendRedirect("/Cart");
	}
	
	//check if logged in
//	private void checkLogin(HttpServletRequest req) throws ServletException, IOException {
//		HttpSession session = req.getSession();
//		Member member;
//		boolean loggedIn = false;
//		try {
//			member = (Member)session.getAttribute("member");
//			loggedIn = new MemberManager().checkMember(email, password);
//		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
}
