package controller;

import java.io.IOException;
import java.net.URLDecoder;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Movie;
import mysqlDatabase.MovieManager;

@WebServlet("/Movies/*")
public class MovieController extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String[] movieNameHtml = req.getRequestURI().split("/");
		
		if (!movieNameHtml[movieNameHtml.length - 2].equals("Movies")) {
			return;
		}
		String movieName = URLDecoder.decode(movieNameHtml[movieNameHtml.length - 1], "UTF-8");
		System.out.println("Get request coming from: " + req.getRequestURI());
		Movie movie = null;
		try {
			movie = new MovieManager().getMovie(movieName);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
			e.printStackTrace();
		}
		
		if (movie.getMovieName() == null) {
			resp.sendRedirect("../");
			return;
		}
		
		System.out.println("from database: " + movie.getMovieName());
		
		req.setAttribute("movie", movie);
		RequestDispatcher rd = req.getRequestDispatcher("../Movie.jsp");
		rd.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String[] movieNameHtml = req.getRequestURI().split("/");
		
		if (!movieNameHtml[movieNameHtml.length - 2].equals("Movies")) {
			return;
		}
		String movieName = URLDecoder.decode(movieNameHtml[movieNameHtml.length - 1], "UTF-8");
		System.out.println("Post request coming from: " + req.getRequestURI());
		req.setAttribute("movieName", movieNameHtml[movieNameHtml.length - 1]);
		RequestDispatcher rd = req.getRequestDispatcher("../ProcessReview");
		rd.forward(req, resp);
	}
	
}
