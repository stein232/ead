package controller;

import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import model.Member;
import model.Theater;
import mysqlDatabase.TheaterManager;

/**
 * Servlet implementation class HallController
 */
@WebServlet("/Hall/*")
public class HallController extends HttpServlet {
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		int lastHallNo = (int)session.getAttribute("lastHallNo");
		String[] url = request.getRequestURI().split("/");
		System.out.println("Get request coming from: " + request.getRequestURI());
		if (!url[url.length - 3].equals("Hall")) {
			return;
		}
		
		String options = URLDecoder.decode(url[url.length - 2], "UTF-8");
		String idText = URLDecoder.decode(url[url.length - 1], "UTF-8");
		
		switch (options) {
		case "AddHall":
			try {
				int theatreId = Integer.parseInt(idText);
				TheaterManager tm = new TheaterManager();
				tm.addHalls(theatreId, lastHallNo);
				session.removeAttribute("lastHallNo");
				session.setAttribute("lastHallNo", lastHallNo + 1);
			} catch (Exception ex) {
				ex.printStackTrace();
			} 
			break;
		case "AddTheater":
			try {
				TheaterManager tm = new TheaterManager();
				Theater t = new Theater();
				t.setTheaterName(idText);
				tm.addTheatre(t);
			} catch (Exception ex) {
				ex.printStackTrace();
			} 
			break;
		default:
			break;
		}
		
		Gson gson = new Gson();
		String jsonText = gson.toJson(null);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().write(jsonText);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
