package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import encryption.Security;
import model.Member;
import model.Movie;
import model.MovieTicket;
import model.ShowTime;
import mysqlDatabase.MemberManager;
import mysqlDatabase.MovieManager;
import mysqlDatabase.MovieTicketManager;
import mysqlDatabase.ShowTimeManager;
import viewModel.CartViewModel;

/**
 * Servlet implementation class MemberController
 */
@WebServlet("/Member/*")
public class MemberController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Profile request coming from: " + request.getRequestURI());
		String[] memberUrl = request.getRequestURI().split("/");
		if (!memberUrl[memberUrl.length - 2].equals("Member")) {
			return;
		}
		String action = memberUrl[memberUrl.length - 1].toLowerCase();
		
		switch (action) {
			case "profile":
				profile(request, response);
				break;
			default:
				break;
		}
	}

	private void profile(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Member m = (Member)session.getAttribute("member");
		
		ArrayList<MovieTicket> purchasedMovieTickets = new ArrayList<MovieTicket>();

		try {
			purchasedMovieTickets = new MovieTicketManager().getAllMovieTicketsOfMember(m.getMemberId());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
			return;
		}
		
		if (purchasedMovieTickets.size() == 0) {
			request.getRequestDispatcher("../MyArclight.jsp").forward(request, response);
			return;
		}
		
		ArrayList<CartViewModel> purchasedCvms = new ArrayList<CartViewModel>(); 
		CartViewModel cvm = new CartViewModel();
	
		ShowTime st; 
		Movie mv;
		try {
			st = new ShowTimeManager().getShowTime(purchasedMovieTickets.get(0).getShowTimeId());
			mv = new MovieManager().getMovie(purchasedMovieTickets.get(0).getMovieId());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
			return;
		}
		
		cvm.setShowTimeDate(st.getShowTimeDate());
		cvm.setShowTimeTime(st.getShowTimeTime());
		cvm.setTheaterName(st.getTheaterName());
		cvm.setMovieImgUrl(mv.getImageURL());
		cvm.setMovieName(mv.getMovieName());
		cvm.setMovieTypeName(st.getMovieTypeName());
		cvm.setHallNo(st.getHallNo());
		
		int showTimeId = st.getShowTimeId();
		
		ArrayList<MovieTicket> movieTickets = new ArrayList<MovieTicket>();
		
		for (int i = 0; i < purchasedMovieTickets.size(); i++) {
			MovieTicket movieTicket = purchasedMovieTickets.get(i);
			if (movieTicket.getShowTimeId() != showTimeId) {
				showTimeId = movieTicket.getShowTimeId();
				cvm.setMovieTickets(movieTickets);
				purchasedCvms.add(cvm);
				cvm = new CartViewModel();
				movieTickets = new ArrayList<MovieTicket>();
				ShowTime showTime; 
				Movie movie;
				try {
					showTime = new ShowTimeManager().getShowTime(purchasedMovieTickets.get(i).getShowTimeId());
					movie = new MovieManager().getMovie(purchasedMovieTickets.get(i).getMovieId());
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
					return;
				}
				
				cvm.setShowTimeDate(showTime.getShowTimeDate());
				cvm.setShowTimeTime(showTime.getShowTimeTime());
				cvm.setTheaterName(showTime.getTheaterName());
				cvm.setMovieImgUrl(movie.getImageURL());
				cvm.setMovieName(movie.getMovieName());
				cvm.setMovieTypeName(showTime.getMovieTypeName());
				cvm.setHallNo(showTime.getHallNo());
			}
			movieTickets.add(movieTicket);
		}
		cvm.setMovieTickets(movieTickets);
		purchasedCvms.add(cvm);
		
		request.setAttribute("purchasedCvms", purchasedCvms);
		request.getRequestDispatcher("../MyArclight.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String[] memberUrl = request.getRequestURI().split("/");
		for (String s : memberUrl)
			System.out.println(s);
		String action = memberUrl[memberUrl.length - 1].toLowerCase();
		
		switch (action) {
			case "login":
				login(request, response);
				break;
			case "logout":
				logout(request, response);
				break;
			case "create":
				create(request, response);
				break;
			case "update":
				update(request, response);
			default:
				break;
		}
	}
	
	private void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Security pw = new Security();
		HttpSession session = request.getSession();
		Member m = (Member)session.getAttribute("member");
		
		m.setEmail(request.getParameter("userEmailText"));
		m.setName(request.getParameter("userNameText"));
		m.setContactNumber(request.getParameter("userPhone"));
		m.setMailingAddress(request.getParameter("userAddress"));
		m.setCreditCardNumber(request.getParameter("ccField1") + request.getParameter("ccField2") 
						+ request.getParameter("ccField3") + request.getParameter("ccField4"));
		String passwordText = request.getParameter("userPasswordText");
		if (!request.getParameter("userPasswordText").equals(""))
			m.setPassword(pw.getHash((request.getParameter("userPasswordText"))));
		else 
			m.setPassword(null);
		
		MemberManager mm = new MemberManager();
		try {
			mm.UpdateMember(m);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		} 

//		Log out
		session.removeAttribute("member");
		loginAfterUpdate(request, response, m);
	}

	private void create(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Security pw = new Security();
		String email = request.getParameter("userEmailText");
		String name = request.getParameter("userNameText");
		String password = pw.getHash((request.getParameter("userPasswordText")));
		String phoneNumber = request.getParameter("userPhone");
		String address = request.getParameter("userAddress");
		String ccNo = request.getParameter("ccField1") + request.getParameter("ccField2") 
						+ request.getParameter("ccField3") + request.getParameter("ccField4");
		int memberId = 0;
		MemberManager mm = new MemberManager();
		Member m = new Member();
		m.setEmail(email);
		m.setName(name); 
		m.setPassword(password);
		m.setContactNumber(phoneNumber);
		m.setMailingAddress(address);
		m.setCreditCardNumber(ccNo);
		try {
			memberId = mm.CreateMember(m);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
		loginAfterCreation(request, response, memberId);
	}
	
	private void logout(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		session.removeAttribute("member");
		
		Gson gson = new Gson();
		String jsonText = gson.toJson(null);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().write(jsonText);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void login(HttpServletRequest request, HttpServletResponse response) {
		String reqParam;
		Member nm = null;
		try {
			reqParam = request.getParameter("member"); 
			if (reqParam != null){
				Gson gson = new GsonBuilder().create();
				Member m = gson.fromJson(reqParam, Member.class);
				MemberManager mm = new MemberManager();
				nm = mm.Login(m);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		HttpSession session = request.getSession();
		if (nm != null){
			session.setAttribute("member", nm);
		} 
		Gson gson = new Gson();
		String jsonText = gson.toJson(nm);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().write(jsonText);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void loginAfterCreation(HttpServletRequest request, HttpServletResponse response, int memberId) 
			throws ServletException, IOException {
		Member nm = null;
		try {
			MemberManager mm = new MemberManager();
			nm = mm.GetOneMember(memberId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		HttpSession session = request.getSession();
		if (nm != null){
			session.setAttribute("member", nm);
		} 
		response.sendRedirect("/?status=true");
	}
	
	private void loginAfterUpdate(HttpServletRequest request, HttpServletResponse response, Member m) 
			throws ServletException, IOException {
		String reqParam;
		Member loginMember = null;
		try {
				MemberManager mm = new MemberManager();
				loginMember = mm.GetOneMember(m.getMemberId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		HttpSession session = request.getSession();
		if (loginMember != null){
			session.setAttribute("member", loginMember);
		} 
		
//		Redirect to home page with update message
		response.sendRedirect("/?update=true");
	}
}
