package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import model.Genre;
import model.MovieType;
import mysqlDatabase.GenreManager;
import mysqlDatabase.MovieTicketManager;
import mysqlDatabase.MovieTypeManager;

/**
 * Servlet implementation class MovieTypeController
 */
@WebServlet("/MovieTypeController")
public class MovieTypeController extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList<MovieType> movieTypes = null;
		MovieType mt = null;
		try {
			MovieTypeManager mtm = new MovieTypeManager();
			movieTypes = mtm.getAllMovieTypes();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String jsonText = new Gson().toJson(movieTypes);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(jsonText);
	}
}
