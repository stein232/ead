package controller;

import java.io.IOException;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

import model.Member;
import model.Movie;
import model.MovieTicket;
import model.MovieType;
import model.Seat;
import model.ShowTime;
import mysqlDatabase.MovieManager;
import mysqlDatabase.MovieTicketManager;
import mysqlDatabase.MovieTypeManager;
import mysqlDatabase.SeatManager;
import mysqlDatabase.ShowTimeManager;
import viewModel.CartViewModel;

@WebServlet("/Booking/*")
public class BookingController extends HttpServlet {
	
	//Display seat selection page
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String[] bookingUrl = req.getRequestURI().split("/");
		System.out.println("Get request coming from: " + req.getRequestURI());
		if (!bookingUrl[bookingUrl.length - 2].equals("Booking")) {
			return;
		}
		int showTimeId = Integer.parseInt(URLDecoder.decode(bookingUrl[bookingUrl.length - 1], "UTF-8"));
		
		
		ShowTime showTime;
		String movieName;
		MovieType movieType;
		ArrayList<ArrayList<Seat>> seats;
		ArrayList<MovieTicket> bookekMovieTickets;
		
		try {
			showTime = new ShowTimeManager().getShowTime(showTimeId);
			movieName = new MovieManager().getMovie(showTime.getMovieId()).getMovieName();
			movieType = new MovieTypeManager().getMovieType(showTime.getMovieTypeId());
			seats = new SeatManager().getSeatsForHall(showTimeId);
			bookekMovieTickets = new MovieTicketManager().getAllMovieTicketsOfShowTime(showTimeId);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
			e.printStackTrace();
			return;
		}
		
		DateTime dt = new DateTime(showTime.getShowTimeDate());
		System.out.println(dt);
		
		int price;
		if (dt.getDayOfWeek() != DateTimeConstants.SATURDAY && dt.getDayOfWeek() != DateTimeConstants.SUNDAY) {
			price = movieType.getWeekdayPrice();
		} else {
			price = movieType.getWeekendPrice();
		}
		
		req.setAttribute("showTime", showTime);
		req.setAttribute("movieName", movieName);
		req.setAttribute("movieType", movieType);
		req.setAttribute("price", price);
		req.setAttribute("seats", seats);
		req.setAttribute("bookedMovieTickets", bookekMovieTickets);
		RequestDispatcher rd = req.getRequestDispatcher("../Booking.jsp");
		rd.forward(req, resp);
	}
	
	//Add to cart
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		Member member = (Member)session.getAttribute("member");
		
		int showTimeId = Integer.parseInt(req.getParameter("showTimeId"));
		String seatsString = req.getParameter("seats");
		String[] seats = req.getParameter("seats").split("\\|");
		
		//get all the data needed to create movieTickets to check
		ShowTime showTime;
		Movie movie;
		MovieType movieType;
		ArrayList<MovieTicket> bookekMovieTickets;
		
		try {
			showTime = new ShowTimeManager().getShowTime(showTimeId);
			movie = new MovieManager().getMovie(showTime.getMovieId());
			movieType = new MovieTypeManager().getMovieType(showTime.getMovieTypeId());
			bookekMovieTickets = new MovieTicketManager().getAllMovieTicketsOfShowTime(showTimeId);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
			e.printStackTrace();
			return;
		}
		
		DateTime dt = new DateTime(showTime.getShowTimeDate());
		System.out.println(dt);
		
		int price;
		if (dt.getDayOfWeek() != DateTimeConstants.SATURDAY && dt.getDayOfWeek() != DateTimeConstants.SUNDAY) {
			price = movieType.getWeekdayPrice();
		} else {
			price = movieType.getWeekendPrice();
		}
		
		//create the movie tickets
		ArrayList<MovieTicket> movieTicketsToAddToCart = new ArrayList<MovieTicket>();
		for (int i = 0; i < seats.length; i++) {
			String[] SeatRowAndSeatNumber = seats[i].split("\\-");
			
			String seatRow = SeatRowAndSeatNumber[0];
			int seatNumber = Integer.parseInt(SeatRowAndSeatNumber[1]);
			
			MovieTicket movieTicket = new MovieTicket();
			movieTicket.setTheaterId(showTime.getTheaterId());
			movieTicket.setHallId(showTime.getHallId());
			movieTicket.setShowTimeId(showTime.getShowTimeId());
			movieTicket.setSeatRow(seatRow);
			movieTicket.setSeatNumber(seatNumber);
			movieTicket.setMovieId(showTime.getMovieId());
			movieTicket.setMemberId(member.getMemberId());
			movieTicket.setPrice(price);
			movieTicket.setMovieTypeId(showTime.getMovieTypeId());
			movieTicketsToAddToCart.add(movieTicket);
		}
		
		//find tickets that are booked
		ArrayList<MovieTicket> invalidMovieTickets = new ArrayList<MovieTicket>();
		for (int i = 0; i < movieTicketsToAddToCart.size(); i++) {
			if (bookekMovieTickets.contains(movieTicketsToAddToCart)) {
				invalidMovieTickets.add(movieTicketsToAddToCart.get(i));
			}
		}
		
		if (invalidMovieTickets.size() > 0) {
			String message = "Sorry, seats ";
			for (int i = 0; i < invalidMovieTickets.size(); i++) {
				message += invalidMovieTickets.get(i).getSeatRow() + invalidMovieTickets.get(i).getSeatNumber() + ", ";
			}
			message += "have been booked";
			resp.sendRedirect("/Booking/" + showTimeId + "?msg=" + message);
			return;
		}

		CartViewModel cvm = new CartViewModel();
		cvm.setMovieTickets(movieTicketsToAddToCart);
		cvm.setMovieName(movie.getMovieName());
		cvm.setMovieImgUrl(movie.getImageURL());
		cvm.setShowTimeDate(showTime.getShowTimeDate());
		cvm.setShowTimeTime(showTime.getShowTimeTime());
		cvm.setTheaterName(showTime.getTheaterName());
		cvm.setHallNo(showTime.getHallNo());
		cvm.setMovieTypeName(movieType.getMovieTypeName());
		
		
		ArrayList<CartViewModel> cvms = (ArrayList<CartViewModel>)session.getAttribute("cvms");
		if (cvms == null) {
			cvms = new ArrayList<CartViewModel>();
		}
		
		cvms.add(0, cvm);
		
		session.setAttribute("cvms", cvms);
		resp.sendRedirect("/?msg=Successfully added the seats to your cart");
	}
}
