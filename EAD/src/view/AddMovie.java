package view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import model.Genre;
import model.Movie;
import mysqlDatabase.MovieManager;

/**
 * Servlet implementation class AddMovie
 */
@WebServlet("/AddMovie")
public class AddMovie extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String reqParam;
		int movieId = 0;
		try {
			reqParam = request.getParameter("movie"); 
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
			Movie m = gson.fromJson(reqParam, Movie.class);
			//TypeToken<ArrayList<Genre>> token = new TypeToken<ArrayList<Genre>>() {};
			//ArrayList<Genre> genres = gson.fromJson(reqParam, token.getType());
			MovieManager mm = new MovieManager();
			movieId = mm.addMovie(m);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Gson gson = new Gson();
		String jsonText = gson.toJson(movieId);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().write(jsonText);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
