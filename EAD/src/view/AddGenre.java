package view;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.google.gson.Gson;

import mysqlDatabase.GenreManager;

/**
 * Servlet implementation class AddGenre
 */
@WebServlet("/AddGenre")
public class AddGenre extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONObject jo;
		String reqParam;
		String key = "", obj = "";
		try {
			reqParam = request.getParameter("genreName"); 
			jo = (JSONObject) new JSONParser().parse(reqParam);
			Iterator it = jo.keySet().iterator();
			while (it.hasNext()){
				key = (String)it.next();
				System.out.println(key);
				obj = (String)jo.get(key);
				System.out.println(obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		GenreManager gm = new GenreManager();
		try {
			gm.addGenre(obj);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Gson gson = new Gson();
		String jsonText = gson.toJson("");
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().write(jsonText);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
