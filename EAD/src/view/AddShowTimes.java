package view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.ShowTime;
import mysqlDatabase.ShowTimeManager;

/**
 * Servlet implementation class AddShowTimes
 */
@WebServlet("/AddShowTimes")
public class AddShowTimes extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String reqParam;
		ShowTimeManager stm = new ShowTimeManager();
		try {
			reqParam = request.getParameter("showTimes"); 
			String testStr="[{\"MovieId\": \"471\",\"TheaterId\": \"1\",\"TheaterName\": \"SP VivoCity\", \"ShowTimeDate\" : \"2015-11-24T00:00:00\", \"ShowTimeTime\" : \"2000-01-01T12:34\"}]";
			reqParam = reqParam.replace("\"", "");
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
			ShowTime[] oldSt =gson.fromJson(reqParam, ShowTime[].class);
			ArrayList<ShowTime> st = new ArrayList<ShowTime>(Arrays.asList(oldSt));
			ArrayList<ShowTime> existingSt = null;
			if (st.size() != 0 && st != null){
				existingSt = stm.getShowTimes(st.get(0).getMovieId());
				for (int i = 0; i < st.size(); i++){
					if (!existingSt.contains(st.get(i))) {
						stm.addShowTime(st.get(i));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Gson gson = new Gson();
		String jsonText = gson.toJson("");
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().write(jsonText);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
