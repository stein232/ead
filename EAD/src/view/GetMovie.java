package view;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.google.gson.Gson;

import model.Movie;
import mysqlDatabase.MovieManager;

/**
 * Servlet implementation class GetMovie
 */
@WebServlet("/GetMovie")
public class GetMovie extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONObject jo;
		String reqParam;
		String key = "", obj = "";
		try {
			reqParam = request.getParameter("name"); 
			jo = (JSONObject) new JSONParser().parse(reqParam);
			Iterator it = jo.keySet().iterator();
			while (it.hasNext()){
				key = (String)it.next();
				obj = (String)jo.get(key);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		MovieManager mm = new MovieManager();
		Movie m = null;
		try {
			m = mm.getMovie(obj);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
			e.printStackTrace();
		}
		
		Gson gson = new Gson();
		String jsonText = gson.toJson(m);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().write(jsonText);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
