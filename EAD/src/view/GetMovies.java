package view;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import model.Movie;
import mysqlDatabase.MovieManager;
import com.google.gson.*;

@WebServlet("/GetMovies")
public class GetMovies extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		JSONObject jo;
		String reqParam;
		String key = "", obj = "";
		try {
			reqParam = req.getParameter("type"); 
			jo = (JSONObject) new JSONParser().parse(reqParam);
			Iterator it = jo.keySet().iterator();
			while (it.hasNext()){
				key = (String)it.next();
				System.out.println(key);
				obj = (String)jo.get(key);
				System.out.println(obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		switch (obj) {
			case "cur":
				getCurrentMovies(req, resp);
				break;
			case "all":
				getAllMovies(req, resp);
				break;
			case "moviesGenres":
				getCurrentMoviesGenres(req, resp);
				break;
		}
//		jsonObj.put("name", "Azeem Chocolate Roll");
//		StringWriter out = new StringWriter();
//		jsonObj.writeJSONString(out);
//		String jsonText = out.toString();
//		resp.setContentType("application/json");
//		resp.setCharacterEncoding("UTF-8");
//		resp.getWriter().write(jsonText);
	}
	
	protected void getCurrentMovies(HttpServletRequest req, HttpServletResponse resp){
		MovieManager mm = new MovieManager();
		ArrayList<Movie> movies = new ArrayList<Movie>();
		try {
			movies = mm.getCurrentMovies();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
			e.printStackTrace();
		}
		
		Gson gson = new Gson();
		String jsonText = gson.toJson(movies);
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		try {
			resp.getWriter().write(jsonText);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void getCurrentMoviesGenres(HttpServletRequest req, HttpServletResponse resp){
		MovieManager mm = new MovieManager();
		ArrayList<Movie> movies = new ArrayList<Movie>();
		try {
			movies = mm.getCurrentMoviesGenres();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
			e.printStackTrace();
		}
		
		Gson gson = new Gson();
		String jsonText = gson.toJson(movies);
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		try {
			resp.getWriter().write(jsonText);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	protected void getAllMovies(HttpServletRequest req, HttpServletResponse resp){
		MovieManager mm = new MovieManager();
		ArrayList<Movie> movies = new ArrayList<Movie>();
		try {
			movies = mm.getAllMovies();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
			e.printStackTrace();
		}
		
		Gson gson = new Gson();
		String jsonText = gson.toJson(movies);
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		try {
			resp.getWriter().write(jsonText);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
