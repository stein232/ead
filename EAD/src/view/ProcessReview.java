package view;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mysqlDatabase.ReviewManager;

@WebServlet("/ProcessReview")
public class ProcessReview extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println(req.getRequestURL());
		System.out.println(req.getAttribute("javax.servlet.forward.request_uri"));
		int movieId = Integer.parseInt(req.getParameter("movieId"));
		String name = req.getParameter("name");
		String reviewMessage = req.getParameter("reviewMessage");
		int rating = Integer.parseInt(req.getParameter("rating"));
		
		boolean success = false;
		try {
			success = new ReviewManager().addReview(movieId, reviewMessage, name, rating);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
			e.printStackTrace();
		}
		
		System.out.println("processed review from: " + req.getAttribute("movieName").toString());
		
		//When upload to bluemix preprend "/Movies/"
		resp.sendRedirect(req.getAttribute("movieName").toString());
	}

}