package view;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.google.gson.Gson;

import model.Hall;
import model.Theater;
import mysqlDatabase.TheaterManager;

/**
 * Servlet implementation class GetHalls
 */
@WebServlet("/GetHalls")
public class GetHalls extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONObject jo;
		String reqParam = "";
		String key = "", obj = "";
		try {
			reqParam = request.getParameter("theater"); 
			jo = (JSONObject) new JSONParser().parse(reqParam);
			Iterator it = jo.keySet().iterator();
			while (it.hasNext()){
				key = (String)it.next();
				System.out.println(key);
				obj = (String)jo.get(key);
				System.out.println(obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Hall h = null;
		int lastHallNo = 0;
		TheaterManager tm = new TheaterManager();
		ArrayList<Hall> halls = new ArrayList<Hall>();
		try {
			halls = tm.getHalls(Integer.parseInt(obj));
			if (halls != null && halls.size() != 0)
				lastHallNo = halls.get(halls.size() - 1).getHallNo();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
			e.printStackTrace();
		}
		
		HttpSession session = request.getSession();
		session.setAttribute("lastHallNo", lastHallNo);
		Gson gson = new Gson();
		String jsonText = gson.toJson(halls);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().write(jsonText);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
