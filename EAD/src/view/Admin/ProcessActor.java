package view.Admin;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mysqlDatabase.ActorManager;

@WebServlet("/Admin/ProcessActor")
public class ProcessActor extends HttpServlet {
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getParameter("action");
		switch (action) {
			case "add":
				if (createActor(req, resp)) {
					resp.sendRedirect("Actors.jsp");
				} else {
					resp.sendRedirect("AddActor.jsp");
				}
				break;
			case "update":
				if (updateActor(req, resp)) {
					resp.sendRedirect("Actors.jsp");
				} else {
					RequestDispatcher rd = req.getRequestDispatcher("UpdateActor.jsp");
					rd.forward(req, resp);
				}
				break;
			case "delete":
				deleteActor(req, resp);
				resp.sendRedirect("Actors.jsp");
				break;
		}
	}

	private boolean createActor(HttpServletRequest req, HttpServletResponse resp) {
		String actorName = req.getParameter("actorName");
		boolean success = false;
		
		try {
			success = new ActorManager().addActor(actorName);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
			e.printStackTrace();
		}
		
		HttpSession session = req.getSession();
		
		if (success) {
			session.setAttribute("message", "Successfully added " + actorName);
		} else {
			session.setAttribute("message", "failed to add " + actorName);
		}
		
		return success;
	}
	private boolean updateActor(HttpServletRequest req, HttpServletResponse resp) {
		String actorId = req.getParameter("actorId");
		String actorName = req.getParameter("actorName");
		boolean success = false;
		
		try {
			success = new ActorManager().updateActor(Integer.parseInt(actorId), actorName);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
			e.printStackTrace();
		}
		
		HttpSession session = req.getSession();
		
		if (success) {
			session.setAttribute("message", "Successfully updated " + actorName);
		} else {
			session.setAttribute("message", "failed to update " + actorName);
		}
		
		return success;
	}
	
	private boolean deleteActor(HttpServletRequest req, HttpServletResponse resp) {
		String actorId = req.getParameter("actorId");
		String actorName = req.getParameter("actorName");
		boolean success = false;
		
		try {
			success = new ActorManager().deleteActor(Integer.parseInt(actorId));
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
			e.printStackTrace();
		}
		
		HttpSession session = req.getSession();
		
		if (success) {
			session.setAttribute("message", "Successfully deleted " + actorName);
		} else {
			session.setAttribute("message", "failed to delete " + actorName);
		}
		
		return success;
	}
}
