package view.Admin;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mysqlDatabase.MovieManager;
@WebServlet("/Admin/ProcessMovie")
public class ProcessMovie extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getParameter("action");
		switch (action) {
			case "delete":
				deleteMovie(req, resp);
				break;
			default:
				break;
		}
	}
	
	private boolean deleteMovie(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		int movieId = Integer.parseInt(req.getParameter("movieId"));
		boolean success = false;
	
		try {
			success = new MovieManager().deleteMovie(movieId);
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		resp.sendRedirect("Dashboard.jsp");
		
		return success;
	}
}
