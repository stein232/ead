package view.Admin;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mysqlDatabase.StaffManager;

@WebServlet("/Admin/ProcessAuthentication")
public class ProcessAuthentication extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		session.removeAttribute("email");
		session.removeAttribute("password");
		resp.sendRedirect("Login.jsp");
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String email = req.getParameter("email");
		String password = req.getParameter("password");
		
		boolean login;
		
		try {
			StaffManager sm = new StaffManager();
			login = sm.checkStaff(email, password);
		} catch (Exception e) {
			req.setAttribute("error", "Something is wrong, please try again later.");
			RequestDispatcher rd = req.getRequestDispatcher("Login.jsp");
			rd.forward(req, resp);
			return;
		}
		
		if (login){
			HttpSession session = req.getSession();
			session.setAttribute("email", email);
			session.setAttribute("password", password);
			resp.sendRedirect("Dashboard.jsp");
		} else {
			req.setAttribute("error", "Email/Password is invalid");
			RequestDispatcher rd = req.getRequestDispatcher("Login.jsp");
			rd.forward(req, resp);
		}
	}

}
