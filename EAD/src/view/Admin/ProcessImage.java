package view.Admin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.tomcat.util.http.fileupload.IOUtils;

import com.google.gson.Gson;

import mysqlDatabase.ImageManager;


@WebServlet("/ProcessImage")
@MultipartConfig
public class ProcessImage extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	    Part filePart = req.getPart("image"); // Retrieves <input type="file" name="file">
	    InputStream fileContent = filePart.getInputStream();
	    File file = File.createTempFile("pom", ".jpg");
	    OutputStream os = new FileOutputStream(file);
	    IOUtils.copy(fileContent, os);
	    System.out.println(file);
	    String imageLink = "";
	    
	    try {
	    	imageLink = new ImageManager().uploadImage(file);
	    } catch (IOException e) {
	    	e.printStackTrace();
	    } finally { 
	    	file.delete();
	    }
	    
	    String jsonText = new Gson().toJson(imageLink);
		
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write(jsonText);
	}

}
