package view.Admin;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mysqlDatabase.GenreManager;

@WebServlet("/Admin/ProcessGenre")
public class ProcessGenre extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getParameter("action");
		switch (action) {
			case "add":
				if (createGenre(req, resp)) {
					resp.sendRedirect("Genres.jsp");
				} else {
					resp.sendRedirect("AddGenre.jsp");
				}
				break;
			case "update":
				if (updateGenre(req, resp)) {
					resp.sendRedirect("Genres.jsp");
				} else {
					RequestDispatcher rd = req.getRequestDispatcher("UpdateGenre.jsp");
					rd.forward(req, resp);
				}
				break;
			case "delete":
				deleteGenre(req, resp);
				resp.sendRedirect("Genres.jsp");
				break;
		}
	}
	
	private boolean createGenre(HttpServletRequest req, HttpServletResponse resp) {
		String genreName = req.getParameter("genreName");
		boolean success = false;
		
		try {
			success = new GenreManager().addGenre(genreName);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
			e.printStackTrace();
		}
		
		HttpSession session = req.getSession();
		
		if (success) {
			session.setAttribute("message", "Successfully added " + genreName);
		} else {
			session.setAttribute("message", "failed to add " + genreName);
		}
		
		return success;
	}
	
	private boolean updateGenre(HttpServletRequest req, HttpServletResponse resp) {
		String genreId = req.getParameter("genreId");
		String genreName = req.getParameter("genreName");
		boolean success = false;
		
		try {
			success = new GenreManager().updateGenre(Integer.parseInt(genreId), genreName);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
			e.printStackTrace();
		}
		
		HttpSession session = req.getSession();
		
		if (success) {
			session.setAttribute("message", "Successfully updated " + genreName);
		} else {
			session.setAttribute("message", "failed to update " + genreName);
		}
		
		return success;
	}
	
	private boolean deleteGenre(HttpServletRequest req, HttpServletResponse resp) {
		String genreId = req.getParameter("genreId");
		String genreName = req.getParameter("genreName");
		boolean success = false;
		
		try {
			success = new GenreManager().deleteGenre(Integer.parseInt(genreId));
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
			e.printStackTrace();
		}
		
		HttpSession session = req.getSession();
		
		if (success) {
			session.setAttribute("message", "Successfully deleted " + genreName);
		} else {
			session.setAttribute("message", "failed to delete " + genreName);
		}
		
		return success;
	}
}
