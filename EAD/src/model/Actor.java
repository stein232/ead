package model;

public class Actor {
	public int ActorId;
	public String ActorName;
	
	public Actor() { }

	public Actor(int actorId, String actorName) {
		ActorId = actorId;
		ActorName = actorName;
	}
	
	public int getActorId() {
		return ActorId;
	}
	public void setActorId(int actorId) {
		ActorId = actorId;
	}
	public String getActorName() {
		return ActorName;
	}
	public void setActorName(String actorName) {
		ActorName = actorName;
	}
}
