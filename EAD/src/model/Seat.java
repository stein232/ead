package model;

public class Seat {
	
	private int HallId;
	private String Row;
	private int SeatNumber;

	public int getHallId() {
		return HallId;
	}

	public void setHallId(int hallId) {
		HallId = hallId;
	}

	public String getRow() {
		return Row;
	}

	public void setRow(String row) {
		Row = row;
	}

	public int getSeatNumber() {
		return SeatNumber;
	}

	public void setSeatNumber(int seatNumber) {
		SeatNumber = seatNumber;
	}
	
	@Override
	public boolean equals(Object otherObject) {
		boolean isEqual = false;
		
		if (otherObject == null || !(otherObject instanceof Seat)) {
			return false;
		}
		
		Seat otherSeat = (Seat)otherObject;
		
		if (otherSeat.HallId == this.HallId && 
			otherSeat.Row.equals(this.Row) && 
			otherSeat.SeatNumber == this.SeatNumber) {
			isEqual = true;
		}
		
		return isEqual;
	}
	
	@Override
	public int hashCode() {
		
		int rowInt = 0;
		
		for (int i = 0; i < this.Row.length(); i++) {
			rowInt += this.Row.charAt(i);
		}
		
		return this.HallId * 500000 + rowInt * 250000 + this.SeatNumber;
	}
}
