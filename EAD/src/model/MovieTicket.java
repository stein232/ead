package model;

import java.sql.Timestamp;

public class MovieTicket {
	private int MovieTicketId;
	private String SeatRow;
	private int SeatNumber;
	private int HallId;
	private int TheaterId;
	private int ShowTimeId;
	private int MovieId;
	private int MemberId;
	private Timestamp PurchasedDate;
	private int Price;
	private int MovieTypeId;

	public int getMovieTicketId() {
		return MovieTicketId;
	}

	public void setMovieTicketId(int movieTicketId) {
		MovieTicketId = movieTicketId;
	}

	public String getSeatRow() {
		return SeatRow;
	}

	public void setSeatRow(String seatRow) {
		SeatRow = seatRow;
	}

	public int getSeatNumber() {
		return SeatNumber;
	}

	public void setSeatNumber(int seatNumber) {
		SeatNumber = seatNumber;
	}

	public int getHallId() {
		return HallId;
	}

	public void setHallId(int hallId) {
		HallId = hallId;
	}

	public int getTheaterId() {
		return TheaterId;
	}

	public void setTheaterId(int theaterId) {
		TheaterId = theaterId;
	}

	public int getShowTimeId() {
		return ShowTimeId;
	}

	public void setShowTimeId(int showTimeId) {
		ShowTimeId = showTimeId;
	}

	public int getMovieId() {
		return MovieId;
	}

	public void setMovieId(int movieId) {
		MovieId = movieId;
	}
	
	public int getMemberId() {
		return MemberId;
	}

	public void setMemberId(int memberId) {
		MemberId = memberId;
	}
	
	public Timestamp getPurchasedDate() {
		return PurchasedDate;
	}

	public void setPurchasedDate(Timestamp purchasedDate) {
		PurchasedDate = purchasedDate;
	}
	
	public int getPrice() {
		return Price;
	}

	public void setPrice(int price) {
		Price = price;
	}
	
	public int getMovieTypeId() {
		return MovieTypeId;
	}

	public void setMovieTypeId(int movieTypeId) {
		MovieTypeId = movieTypeId;
	}
	
	@Override
	public boolean equals(Object otherObject) {
		boolean isEqual = false;
		
		if (otherObject == null || !(otherObject instanceof MovieTicket)) {
			return false;
		}
		
		MovieTicket otherMovieTicket = (MovieTicket)otherObject;
		
		if (otherMovieTicket.ShowTimeId == this.ShowTimeId && 
			otherMovieTicket.SeatRow.equals(SeatRow) && 
			otherMovieTicket.SeatNumber == this.SeatNumber) {
			isEqual = true;
		}
		
		return isEqual;
	}
	
	@Override
	public int hashCode() {
		
		int rowInt = 0;
		
		for (int i = 0; i < this.SeatRow.length(); i++) {
			rowInt += this.SeatRow.charAt(i);
		}
		
		return Integer.parseInt("" + this.ShowTimeId + rowInt + this.SeatNumber);
	}
}
