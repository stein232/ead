package model;

import java.sql.Timestamp;

public class Review {
	private int ReviewId;
	private int MovieId;
	private String Name;
	private String ReviewMessage;
	private Timestamp CreateDate;
	private int Rating;
	
	public Review() { }
	
	public Review(int reviewId, String name, String message) {
		ReviewId = reviewId;
		Name = name;
		ReviewMessage = message;
	}
	
	public int getReviewId() {
		return ReviewId;
	}
	public void setReviewId(int reviewId) {
		ReviewId = reviewId;
	}
	public int getMovieId() {
		return MovieId;
	}
	public void setMovieId(int movieId) {
		MovieId = movieId;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getReviewMessage() {
		return ReviewMessage;
	}
	public void setReviewMessage(String message) {
		ReviewMessage = message;
	}
	public Timestamp getCreateDate() {
		return CreateDate;
	}
	public void setCreateDate(Timestamp createDate) {
		CreateDate = createDate;
	}
	public int getRating() {
		return Rating;
	}
	public void setRating(int rating) {
		Rating = rating;
	}
}
