package model;

import java.util.ArrayList;

/**
 * @author lauchehoe
 *
 */
public class Hall {
	
	public int HallId;
	public int HallNo;
	public ArrayList<Seat> Seats;
	
	public Hall() { }

	public Hall(int hallId, ArrayList<Seat> seats) {
		HallId = hallId;
		Seats = seats;
	}

	public int getHallId() {
		return HallId;
	}

	public void setHallId(int hallId) {
		HallId = hallId;
	}

	public ArrayList<Seat> getSeats() {
		return Seats;
	}

	public void setSeats(ArrayList<Seat> seats) {
		Seats = seats;
	}

	public int getHallNo() {
		return HallNo;
	}

	public void setHallNo(int hallNo) {
		HallNo = hallNo;
	}
	
}
