package model;

import java.util.ArrayList;
import java.sql.Date;

public class Movie {
	
	private int MovieId;
	private String MovieName;
	private String MovieRating;
	private String Sypnosis;
	private String ImageURL;
	private Date ReleaseDate;
	private Date EndDate;
	private int Duration;
	private ArrayList<Actor> Actors;
	private ArrayList<Genre> Genres;
	private ArrayList<Review> Reviews;
	private ArrayList<ShowTime> ShowTimes;

	public Movie() { 
		Actors = new ArrayList<Actor>();
		Genres = new ArrayList<Genre>();
		Reviews = new ArrayList<Review>();
	}
	
	public Movie(int movieId, String movieName, String movieRating, String sypnosis, Date releaseDate, 
			int duration, ArrayList<Actor> actors,
			ArrayList<Genre> genres, ArrayList<Review> reviews) {
		MovieId = movieId;
		MovieName = movieName;
		MovieRating = movieRating;
		Sypnosis = sypnosis;
		ReleaseDate = releaseDate;
		Duration = duration;
		Actors = actors;
		Genres = genres;
		Reviews = reviews;
	}
	
	public int getMovieId() {
		return MovieId;
	}
	public void setMovieId(int movieId) {
		MovieId = movieId;
	}
	public String getMovieName() {
		return MovieName;
	}
	public void setMovieName(String movieName) {
		MovieName = movieName;
	}
	public String getMovieRating() {
		return MovieRating;
	}
	public void setMovieRating(String movieRating) {
		MovieRating = movieRating;
	}
	public String getSypnosis() {
		return Sypnosis;
	}
	public void setSypnosis(String sypnosis) {
		Sypnosis = sypnosis;
	}
	public String getImageURL() {
		return ImageURL;
	}

	public void setImageURL(String imageURL) {
		ImageURL = imageURL;
	}
	public Date getReleaseDate() {
		return ReleaseDate;
	}
	public void setReleaseDate(Date releaseDate) {
		ReleaseDate = releaseDate;
	}
	public Date getEndDate() {
		return EndDate;
	}

	public void setEndDate(Date endDate) {
		EndDate = endDate;
	}
	public int getDuration() {
		return Duration;
	}
	public void setDuration(int duration) {
		Duration = duration;
	}
	public ArrayList<Actor> getActors() {
		return Actors;
	}
	public void setActors(ArrayList<Actor> actors) {
		Actors = actors;
	}
	public void addActor(Actor actor) {
		Actors.add(actor);
	}
	public ArrayList<Genre> getGenres() {
		return Genres;
	}
	public void setGenres(ArrayList<Genre> genres) {
		Genres = genres;
	}
	public void addGenre(Genre genre) {
		Genres.add(genre);
	}
	public ArrayList<Review> getReviews() {
		return Reviews;
	}
	public void setReviews(ArrayList<Review> reviews) {
		Reviews = reviews;
	}
	public void addReview(Review review) {
		Reviews.add(review);
	}
	public ArrayList<ShowTime> getShowTimes() {
		return ShowTimes;
	}

	public void setShowTimes(ArrayList<ShowTime> showTimes) {
		ShowTimes = showTimes;
	}
	public void addShowTime(ShowTime showTime) {
		ShowTimes.add(showTime);
	}
}
