package model;

public class MovieType {
	private int MovieTypeId;
	private String MovieTypeName;
	private int WeekdayPrice;
	private int WeekendPrice;
	
	public MovieType() { }
	
	public MovieType(int movieTypeId, String movieTypeName, int weekdayPrice, int weekendPrice) {
		MovieTypeId = movieTypeId;
		MovieTypeName = movieTypeName;
		WeekdayPrice = weekdayPrice;
		WeekendPrice = weekendPrice;
	}

	public int getMovieTypeId() {
		return MovieTypeId;
	}

	public void setMovieTypeId(int movieTypeId) {
		MovieTypeId = movieTypeId;
	}

	public String getMovieTypeName() {
		return MovieTypeName;
	}

	public void setMovieTypeName(String movieTypeName) {
		MovieTypeName = movieTypeName;
	}

	public int getWeekdayPrice() {
		return WeekdayPrice;
	}

	public void setWeekdayPrice(int weekdayPrice) {
		WeekdayPrice = weekdayPrice;
	}

	public int getWeekendPrice() {
		return WeekendPrice;
	}

	public void setWeekendPrice(int weekendPrice) {
		WeekendPrice = weekendPrice;
	}
}
