package model;

public class Member {
	
	private int MemberId;
	private String Email;
	private String Name;
	private String ContactNumber;
	private String MailingAddress;
	private String CreditCardNumber;
	private String Password;

	public Member() { }
	
	public Member(int memberId, String email, String name, String contactNumber, String mailingAddress,
			String creditCardNumber, String password) {
		MemberId = memberId;
		Email = email;
		Name = name;
		ContactNumber = contactNumber;
		MailingAddress = mailingAddress;
		CreditCardNumber = creditCardNumber;
		Password = password;
	}
	
	public int getMemberId() {
		return MemberId;
	}
	public void setMemberId(int memberId) {
		MemberId = memberId;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getContactNumber() {
		return ContactNumber;
	}
	public void setContactNumber(String contactNumber) {
		ContactNumber = contactNumber;
	}
	public String getMailingAddress() {
		return MailingAddress;
	}
	public void setMailingAddress(String mailingAddress) {
		MailingAddress = mailingAddress;
	}
	public String getCreditCardNumber() {
		return CreditCardNumber;
	}
	public void setCreditCardNumber(String creditCardNumber) {
		CreditCardNumber = creditCardNumber;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}	
}
