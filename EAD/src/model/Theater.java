package model;

import java.util.ArrayList;

public class Theater {
	
	private int TheaterId;
	private String TheaterName;
	public String getTheaterName() {
		return TheaterName;
	}
	public void setTheaterName(String theaterName) {
		TheaterName = theaterName;
	}
	private ArrayList<Integer> halls;
	
	public int getTheaterId() {
		return TheaterId;
	}
	public void setTheaterId(int theaterId) {
		TheaterId = theaterId;
	}
	public ArrayList<Integer> getHalls() {
		return halls;
	}
	public void setHalls(ArrayList<Integer> halls) {
		this.halls = halls;
	}
	public void addHall(Integer hall){
		halls.add(hall);
	}
}
