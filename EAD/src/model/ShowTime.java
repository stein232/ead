package model;

import java.sql.Date;

public class ShowTime {
	private int HallId;
	private int HallNo;
	private int TheaterId;
	private String TheaterName;
	private int MovieId;
	private int ShowTimeId;
	private Date ShowTimeDate;
	private java.util.Date ShowTimeTime;
	private int MovieTypeId;
	private String MovieTypeName;
	
	public int getHallId() {
		return HallId;
	}
	public void setHallId(int hallId) {
		HallId = hallId;
	}
	public int getTheaterId() {
		return TheaterId;
	}
	public void setTheaterId(int theaterId) {
		TheaterId = theaterId;
	}
	public String getTheaterName() {
		return TheaterName;
	}
	public void setTheaterName(String theaterName) {
		TheaterName = theaterName;
	}
	public int getMovieId() {
		return MovieId;
	}
	public void setMovieId(int movieId) {
		MovieId = movieId;
	}
	public int getShowTimeId() {
		return ShowTimeId;
	}
	public void setShowTimeId(int showTimeId) {
		ShowTimeId = showTimeId;
	}
	public Date getShowTimeDate() {
		return ShowTimeDate;
	}
	public void setShowTimeDate(Date showTimeDate) {
		ShowTimeDate = showTimeDate;
	}
	public java.util.Date getShowTimeTime() {
		return ShowTimeTime;
	}
	public void setShowTimeTime(java.util.Date showTimeTime) {
		ShowTimeTime = showTimeTime;
	}
	public int getMovieTypeId() {
		return MovieTypeId;
	}
	public void setMovieTypeId(int movieTypeId) {
		MovieTypeId = movieTypeId;
	}
	public String getMovieTypeName() {
		return MovieTypeName;
	}
	public void setMovieTypeName(String movieTypeName) {
		MovieTypeName = movieTypeName;
	}
	public int getHallNo() {
		return HallNo;
	}
	public void setHallNo(int hallNo) {
		HallNo = hallNo;
	}
	
	@Override
	public boolean equals(Object otherObject) {
		boolean isEqual = false;
		
		if (otherObject == null || !(otherObject instanceof ShowTime)) {
			return false;
		}
		
		ShowTime otherShowTime = (ShowTime)otherObject;
		
		if (otherShowTime.ShowTimeId == this.ShowTimeId) {
			isEqual = true;
		}
		
		return isEqual;
	}
	
	@Override
	public int hashCode() {
		return this.ShowTimeId;
	}
}
