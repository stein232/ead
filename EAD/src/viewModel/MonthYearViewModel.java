package viewModel;

public class MonthYearViewModel {
	String friendlyDate;
	String sqlParamMonth;
	String sqlParamYear;
	String sqlParamDate;
	
	public String getFriendlyDate() {
		return friendlyDate;
	}
	public void setFriendlyDate(String friendlyDate) {
		this.friendlyDate = friendlyDate;
	}
	public String getSqlParamMonth() {
		return sqlParamMonth;
	}
	public void setSqlParamMonth(String sqlParamMonth) {
		this.sqlParamMonth = sqlParamMonth;
	}
	public String getSqlParamYear() {
		return sqlParamYear;
	}
	public void setSqlParamYear(String sqlParamYear) {
		this.sqlParamYear = sqlParamYear;
	}
	public String getSqlParamDate() {
		return sqlParamDate;
	}
	public void setSqlParamDate(String sqlParamDate) {
		this.sqlParamDate = sqlParamDate;
	}
	
}
