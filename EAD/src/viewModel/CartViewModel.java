package viewModel;

import java.util.ArrayList;
import java.util.Date;

import model.MovieTicket;

public class CartViewModel {
	//Naming convention changed to small case first due to movieticket
	private ArrayList<MovieTicket> movieTickets;
	private String movieName;
	private String movieImgUrl;
	private Date showTimeDate;
	private Date showTimeTime;
	private String theaterName;
	private int hallNo;
	private String movieTypeName;

	public ArrayList<MovieTicket> getMovieTickets() {
		return movieTickets;
	}

	public void setMovieTickets(ArrayList<MovieTicket> movieTickets) {
		this.movieTickets = movieTickets;
	}

	public String getMovieName() {
		return movieName;
	}

	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	
	public String getMovieImgUrl() {
		return movieImgUrl;
	}

	public void setMovieImgUrl(String movieImgUrl) {
		this.movieImgUrl = movieImgUrl;
	}

	public Date getShowTimeDate() {
		return showTimeDate;
	}

	public void setShowTimeDate(Date showTimeDate) {
		this.showTimeDate = showTimeDate;
	}

	public Date getShowTimeTime() {
		return showTimeTime;
	}

	public void setShowTimeTime(Date showTimeTime) {
		this.showTimeTime = showTimeTime;
	}

	public String getTheaterName() {
		return theaterName;
	}

	public void setTheaterName(String theaterName) {
		this.theaterName = theaterName;
	}

	public int getHallNo() {
		return hallNo;
	}

	public void setHallNo(int hallNo) {
		this.hallNo = hallNo;
	}

	public String getMovieTypeName() {
		return movieTypeName;
	}

	public void setMovieTypeName(String movieTypeName) {
		this.movieTypeName = movieTypeName;
	}
}
