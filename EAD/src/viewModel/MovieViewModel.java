package viewModel;

import model.Movie;

public class MovieViewModel extends Movie {
	String numberOfTickets;
	String revenue;
	
	public String getNumberOfTickets() {
		return numberOfTickets;
	}
	public void setNumberOfTickets(String numberOfTickets) {
		this.numberOfTickets = numberOfTickets;
	}
	public String getRevenue() {
		return revenue;
	}
	public void setRevenue(String revenue) {
		this.revenue = revenue;
	}
}
