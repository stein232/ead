package mysqlDatabase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Actor;

public class ActorManager {
	/***** CREATE *****/
	
	public boolean addActor(String actorName) 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		int rowsAffected = 0;
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "INSERT INTO actor (`ActorName`) VALUES (?)";
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setString(1, actorName);
			rowsAffected = psmt.executeUpdate();
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		
		return rowsAffected == 1;
	}
	
	/***** RETRIEVE *****/
	
	public Actor getActor(int actorId) 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		Actor actor = new Actor();
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "SELECT ActorId, ActorName FROM actor WHERE ActorId = ?;";
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setInt(1, actorId);
			ResultSet results = psmt.executeQuery();
			try {
				while (results.next()) {
					actor.setActorId(results.getInt("ActorId"));
					actor.setActorName(results.getString("ActorName"));
				}
			} finally {
				results.close();
			}
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		return actor;
	}
	
	public ArrayList<Actor> getAllActors() 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		ArrayList<Actor> actors = new ArrayList<Actor>();
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "SELECT ActorId, ActorName FROM actor WHERE _IsDeleted = 0;";
			Statement smt = cn.createStatement();
			ResultSet results = smt.executeQuery(query);
			try {
				while (results.next()) {
					Actor actor = new Actor();
					actor.setActorId(results.getInt("ActorId"));
					actor.setActorName(results.getString("ActorName"));
					actors.add(actor);
				}
			} finally {
				results.close();
			}
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		return actors;
	}
	
	public ArrayList<Actor> getAllActors(int movieId) 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		ArrayList<Actor> actors = new ArrayList<Actor>();
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "SELECT ActorName, ActorId FROM movie_has_actor m " +
								 "INNER JOIN actor a " +
								 "ON m.actor_actorid = a.actorId " +
                                 "WHERE movie_movieId = ? and " +
                                 "a._IsDeleted = 0 ";
			PreparedStatement pst = cn.prepareStatement(query);
			pst.setInt(1, movieId);
			ResultSet results = pst.executeQuery();
			try {
				while (results.next()) {
					Actor actor = new Actor();
					actor.setActorId(results.getInt("ActorId"));
					actor.setActorName(results.getString("ActorName"));
					actors.add(actor);
				}
			} finally {
				results.close();
			}
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		return actors;
	}
	
	/***** UPDATE *****/
	
	public boolean updateActor(int actorIdToUpdate, String actorName) 
			throws InstantiationException, IllegalAccessException, 
			ClassNotFoundException, SQLException {
		int rowsAffected = 0;
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "UPDATE actor set ActorName = ? where ActorId = ?";
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setString(1, actorName);
			psmt.setInt(2, actorIdToUpdate);
			rowsAffected = psmt.executeUpdate();
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		
		return rowsAffected == 1;
	}
	
	/***** DELETE *****/
	
	public boolean deleteActor(int actorIdToUpdate) 
			throws InstantiationException, IllegalAccessException, 
			ClassNotFoundException, SQLException {
		int rowsAffected = 0;
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "UPDATE actor set _IsDeleted = 1 where ActorId = ?";
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setInt(1, actorIdToUpdate);
			rowsAffected = psmt.executeUpdate();
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		
		return rowsAffected == 1;
	}
}
