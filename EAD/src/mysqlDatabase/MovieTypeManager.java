package mysqlDatabase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.util.ArrayList;

import model.MovieType;

public class MovieTypeManager {	
	/***** RETRIEVE *****/
	public MovieType getMovieType(int movieTypeId) 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		MovieType movieType = new MovieType();
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "SELECT MovieTypeId, MovieTypeName, WeekdayPrice, WeekendPrice "
					+ "FROM movietype "
					+ "WHERE MovieTypeId = ?;";
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setInt(1, movieTypeId);
			ResultSet results = psmt.executeQuery();
			try {
				results.next();
				movieType.setMovieTypeId(results.getInt("MovieTypeId"));
				movieType.setMovieTypeName(results.getString("MovieTypeName"));
				movieType.setWeekdayPrice(results.getInt("WeekdayPrice"));
				movieType.setWeekendPrice(results.getInt("WeekendPrice"));
			} finally {
				results.close();
			}
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		return movieType;
	}
	
	public ArrayList<MovieType> getAllMovieTypes() 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		ArrayList<MovieType> movieTypes = new ArrayList<>();
		MovieType movieType;
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "SELECT MovieTypeId, MovieTypeName, WeekdayPrice, WeekendPrice "
					+ "FROM movietype ";
			PreparedStatement psmt = cn.prepareStatement(query);
			ResultSet results = psmt.executeQuery();
			try {
				while (results.next()){
					movieType = new MovieType();
					movieType.setMovieTypeId(results.getInt("MovieTypeId"));
					movieType.setMovieTypeName(results.getString("MovieTypeName"));
					movieType.setWeekdayPrice(results.getInt("WeekdayPrice"));
					movieType.setWeekendPrice(results.getInt("WeekendPrice"));
					movieTypes.add(movieType);
				}
				
			} finally {
				results.close();
			}
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		return movieTypes;
	}
}
