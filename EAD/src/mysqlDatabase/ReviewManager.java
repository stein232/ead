package mysqlDatabase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.util.ArrayList;

import model.Review;

public class ReviewManager {

	/***** CREATE *****/
	
	public boolean addReview(int movieId, String reviewMessage, String name, int rating) 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		int rowsAffected = 0;
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "INSERT INTO review "
					+ "(`Movie_MovieId`, `ReviewMessage`, `Name`, `Rating`) "
					+ "VALUES "
					+ "(?, ?, ?, ?)";
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setInt(1, movieId);
			psmt.setString(2, reviewMessage);
			psmt.setString(3, name);
			psmt.setInt(4, rating);
			rowsAffected = psmt.executeUpdate();
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		
		return rowsAffected == 1;
	}
	
	/***** RETRIEVE *****/
	
	public ArrayList<Review> getReviews(int movieId) 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		ArrayList<Review> reviews = new ArrayList<Review>();
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "SELECT ReviewId, Movie_MovieId, ReviewMessage, Name, CreateDate, Rating "
					+ "FROM review "
					+ "WHERE Movie_MovieId = ?;";
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setInt(1, movieId);
			ResultSet results = psmt.executeQuery();
			try {
				while (results.next()) {
					Review review = new Review();
					review.setReviewId(results.getInt("ReviewId"));
					review.setMovieId(results.getInt("Movie_MovieId"));
					review.setReviewMessage(results.getString("ReviewMessage"));
					review.setName(results.getString("Name"));
					review.setCreateDate(results.getTimestamp("CreateDate"));
					review.setRating(results.getInt("Rating"));
					reviews.add(review);
				}
			} finally {
				results.close();
			}
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		return reviews;
	}
	
	/***** UPDATE *****/
	
	
	/***** DELETE *****/
}
