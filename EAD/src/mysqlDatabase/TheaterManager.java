package mysqlDatabase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.util.ArrayList;

import model.Hall;
import model.Theater;

public class TheaterManager {
	/***** RETRIEVE *****/
	public ArrayList<Theater> getTheaters()
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		Theater t = null;
		ArrayList<Theater> theaters = new ArrayList<Theater>();
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "SELECT theaterid, theatername from theater;";
			PreparedStatement psmt = cn.prepareStatement(query);
			ResultSet results = psmt.executeQuery();
			try {
				while (results.next()) {
					t = new Theater();
					t.setTheaterId(results.getInt("theaterid"));
					t.setTheaterName(results.getString("theatername"));
					theaters.add(t);
				}
			} finally {
				results.close();
			}
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		return theaters;
	}
	
	public ArrayList<Hall> getHalls(int theaterId)
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		Hall h = null;
		ArrayList<Hall> halls = new ArrayList<Hall>();
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "SELECT HallId, HallNo FROM hall WHERE Theater_TheaterId = ? ORDER BY hallno asc;";
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setInt(1, theaterId);
			ResultSet results = psmt.executeQuery();
			try {
				while (results.next()) {
					h = new Hall();
					h.setHallId(results.getInt("HallId"));
					h.setHallNo(results.getInt("HallNo"));
					halls.add(h);
				}
			} finally {
				results.close();
			}
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		return halls;
	}
	
	public int addTheatre(Theater t)
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		int theatreId = 0;
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "Insert into theater (theatername) values (?);";
			PreparedStatement psmt = cn.prepareStatement(query, java.sql.Statement.RETURN_GENERATED_KEYS);
			psmt.setString(1, t.getTheaterName());
			psmt.executeUpdate();
			ResultSet rs = psmt.getGeneratedKeys();
			while (rs.next()){
				theatreId = rs.getInt(1);
			}
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		return theatreId;
	}
	
	public void addHalls(int theaterId, int lastHallNo)
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		int newHallNo;
		if (lastHallNo != 0){
			newHallNo = lastHallNo + 1;
		} else {
			newHallNo = 1;
		}
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "CALL add_hall(?, ?)";
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setInt(1, theaterId);
			psmt.setInt(2, newHallNo);
			psmt.executeQuery();
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
	}
}
