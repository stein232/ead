package mysqlDatabase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.sql.Statement;
import java.util.ArrayList;

import java.sql.CallableStatement;

import model.Actor;
import model.Genre;
import model.Movie;
import model.ShowTime;

public class MovieManager {
	
	/***** CREATE *****/
	public int addMovie(Movie m) 
			throws InstantiationException, IllegalAccessException, 
			ClassNotFoundException, SQLException {
		int rowId = 0;
		boolean success = false;
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "INSERT INTO MOVIE (MovieName, ReleaseDate, EndDate, Sypnosis, Duration, ImageURL) " +
							"VALUES (?, ?, ?, ?, ?, ?);";
			PreparedStatement pstmt = cn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, m.getMovieName());
			pstmt.setDate(2, m.getReleaseDate());
			pstmt.setDate(3, m.getEndDate());
			pstmt.setString(4, m.getSypnosis());
			pstmt.setInt(5, m.getDuration());
			pstmt.setString(6, m.getImageURL());
			pstmt.executeUpdate();
			ResultSet generatedKeys = pstmt.getGeneratedKeys();
			if (generatedKeys.next()){
				rowId = generatedKeys.getInt(1);
			}
			
			query = "INSERT INTO movie_has_genre (Movie_MovieId, Genre_GenreId) VALUES ";
			ArrayList<Genre> gn = m.getGenres();
			for (int i = 0; i < gn.size(); i++){
				if (i == gn.size()-1){
					query += "(" + rowId + "," + gn.get(i).getGenreId() + ");";
				} else {
					query += "(" + rowId + "," + gn.get(i).getGenreId() + "),";
				}
			}
			pstmt = cn.prepareStatement(query);
			pstmt.executeUpdate();
			
			query = "INSERT INTO movie_has_actor (Movie_MovieId, Actor_ActorId) VALUES ";
			ArrayList<Actor> ac = m.getActors();
			for (int i = 0; i < ac.size(); i++){
				if (i == ac.size()-1){
					query += "(" + rowId + "," + ac.get(i).ActorId + ");";
				} else {
					query += "(" + rowId + "," + ac.get(i).ActorId + "),";
				}
			}
			pstmt = cn.prepareStatement(query);
			pstmt.executeUpdate();
			
			
			success = true;
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		
		return rowId;
	}
	
	
	/***** RETRIEVE *****/
	
	public ArrayList<Movie> getAllMovies() 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {

		ArrayList<Movie> movies = new ArrayList<Movie>();
		ArrayList<ShowTime> showTimes = new ArrayList<ShowTime>();
		int movieId;
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "Select MovieId, MovieName, ReleaseDate, EndDate, Sypnosis, Duration, ImageURL from movie "
							+ "WHERE _IsDeleted = 0 "
							+ "ORDER BY ReleaseDate DESC;";
			PreparedStatement psmt = cn.prepareStatement(query);
			ResultSet rs = psmt.executeQuery();
			
			while (rs.next()){
				Movie m = new Movie();
				movieId = rs.getInt("MovieId");
				m.setMovieId(movieId);
				m.setMovieName(rs.getString("MovieName"));
				m.setReleaseDate(rs.getDate("ReleaseDate"));
				m.setEndDate(rs.getDate("EndDate"));
				m.setSypnosis(rs.getString("Sypnosis"));
				m.setDuration(rs.getInt("Duration"));
				m.setImageURL(rs.getString("ImageURL"));
				
//				query = "Select Hall_HallId, Hall_Theater_TheaterId, ShowTimeId, ShowTimeDate, ShowTimeTime from ShowTime where movie_movieid =" + 
//							movieId + ";";
//				PreparedStatement psmtTwo = cn.prepareStatement(query);
//				ResultSet rsTwo = psmtTwo.executeQuery();
//
//				while (rsTwo.next()){
//					ShowTime s = new ShowTime();
//					s.setHallId(rsTwo.getInt("Hall_HallId"));
//					s.setTheaterId(rsTwo.getInt("Hall_Theater_TheaterId"));
//					s.setShowtimeId(rsTwo.getInt("ShowTimeId"));
//					s.setShowtimeDate(rsTwo.getDate("ShowTimeDate"));
//					s.setShowtimeTime(rsTwo.getTime("ShowTimeTime"));
//					showTimes.add(s);
//				}
				
				//ActorManager am = new ActorManager();
				//m.setActors(am.getAllActors(movieId));
				
				//m.setShowTimes(showTimes);
				movies.add(m);
			}
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		return movies;
	}
	
	public ArrayList<Movie> getCurrentMovies() 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {

		ArrayList<Movie> movies = new ArrayList<Movie>();
		ArrayList<ShowTime> showTimes = new ArrayList<ShowTime>();
		int movieId;
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "Select MovieId, MovieName, ReleaseDate, EndDate, Sypnosis, Duration, ImageURL from movie "
							+ "WHERE _IsDeleted = 0 AND (EndDate >= NOW() OR EndDate IS NULL) "
							+ "ORDER BY ReleaseDate DESC;";
			PreparedStatement psmt = cn.prepareStatement(query);
			ResultSet rs = psmt.executeQuery();
			
			while (rs.next()){
				Movie m = new Movie();
				movieId = rs.getInt("MovieId");
				m.setMovieId(movieId);
				m.setMovieName(rs.getString("MovieName"));
				m.setReleaseDate(rs.getDate("ReleaseDate"));
				m.setEndDate(rs.getDate("EndDate"));
				m.setSypnosis(rs.getString("Sypnosis"));
				m.setDuration(rs.getInt("Duration"));
				m.setImageURL(rs.getString("ImageURL"));
				
//				query = "Select Hall_HallId, Hall_Theater_TheaterId, ShowTimeId, ShowTimeDate, ShowTimeTime from ShowTime where movie_movieid =" + 
//							movieId + ";";
//				PreparedStatement psmtTwo = cn.prepareStatement(query);
//				ResultSet rsTwo = psmtTwo.executeQuery();
//
//				while (rsTwo.next()){
//					ShowTime s = new ShowTime();
//					s.setHallId(rsTwo.getInt("Hall_HallId"));
//					s.setTheaterId(rsTwo.getInt("Hall_Theater_TheaterId"));
//					s.setShowtimeId(rsTwo.getInt("ShowTimeId"));
//					s.setShowtimeDate(rsTwo.getDate("ShowTimeDate"));
//					s.setShowtimeTime(rsTwo.getTime("ShowTimeTime"));
//					showTimes.add(s);
//				}
//				
//				ActorManager am = new ActorManager();
//				m.setActors(am.getAllActors(movieId));
//				
//				m.setShowTimes(showTimes);
				movies.add(m);
			}
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		return movies;
	}

	public ArrayList<Movie> getCurrentMoviesGenres() 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {

		ArrayList<Movie> movies = new ArrayList<Movie>();
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "SELECT MovieId, MovieName, ReleaseDate, EndDate, Sypnosis, Duration, ImageURL, GenreId, GenreName "
					+ "FROM movie INNER JOIN movie_has_genre "
					+ "ON movie.MovieId = movie_has_genre.Movie_MovieId "
					+ "INNER JOIN genre "
					+ "ON movie_has_genre.Genre_GenreId = genre.GenreId "
					+ "WHERE movie._IsDeleted = 0 AND genre._IsDeleted = 0 AND (EndDate >= NOW() OR EndDate IS NULL) "
					+ "ORDER BY ReleaseDate DESC;";
			PreparedStatement psmt = cn.prepareStatement(query);
			ResultSet rs = psmt.executeQuery();
			
			Movie m = null;
			int movieId = -1;
			while (rs.next()){
				if (rs.getInt("MovieId") != movieId) {
					if (movieId != -1) {
						movies.add(m);
					}
					movieId = rs.getInt("MovieId");
					m = new Movie();
					m.setMovieId(rs.getInt("MovieId"));
					m.setMovieName(rs.getString("MovieName"));
					m.setReleaseDate(rs.getDate("ReleaseDate"));
					m.setEndDate(rs.getDate("EndDate"));
					m.setSypnosis(rs.getString("Sypnosis"));
					m.setDuration(rs.getInt("Duration"));
					m.setImageURL(rs.getString("ImageURL"));
				}
				
				Genre genre = new Genre();
				genre.setGenreId(rs.getInt("GenreId"));
				genre.setGenreName(rs.getString("GenreName"));
				m.addGenre(genre);
			}
			movies.add(m);
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		return movies;
	}

	public Movie getMovie(int movieId) 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		Movie movie = new Movie();
		boolean nextResultSet = false;
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "call get_movie_details_by_id(?);";
			CallableStatement cst = cn.prepareCall(query);
			cst.setInt(1, movieId);
			nextResultSet = cst.execute();
			try {
				int count = 1;
				while (nextResultSet) {
					ResultSet rs = cst.getResultSet();
					
					switch (count) {
					case 1:
						while (rs.next()){
							movie.setMovieId(rs.getInt("MovieId"));
							movie.setMovieName(rs.getString("MovieName"));
							movie.setReleaseDate(rs.getDate("ReleaseDate"));
							movie.setEndDate(rs.getDate("EndDate"));
							movie.setSypnosis(rs.getString("Sypnosis"));
							movie.setDuration(rs.getInt("Duration"));
							movie.setImageURL(rs.getString("ImageURL"));
						}
						break;
					case 2:
						ArrayList<Actor> ac = new ArrayList<Actor>();
						while (rs.next()){
							Actor a = new Actor();
							a.setActorId(rs.getInt("actorid"));
							a.setActorName(rs.getString("actorname"));
							ac.add(a);
						}
						movie.setActors(ac);
						break;
					case 3:
						ArrayList<Genre> gn = new ArrayList<Genre>();
						while (rs.next()){
							Genre g = new Genre();
							g.setGenreId(rs.getInt("genreid"));
							g.setGenreName(rs.getString("genrename"));
							gn.add(g);
						}
						movie.setGenres(gn);
						break;
					case 4:
						ArrayList<ShowTime> st = new ArrayList<ShowTime>();
						while (rs.next()){
							ShowTime s = new ShowTime();
							s.setShowTimeId(rs.getInt("ShowTimeId"));
							s.setShowTimeDate(rs.getDate("ShowTimeDate"));
							s.setShowTimeTime(rs.getDate("ShowTimeTime"));
							s.setHallId(rs.getInt("Hall_HallId"));
							s.setTheaterId(rs.getInt("Hall_Theater_TheaterId"));
							st.add(s);
						}
						movie.setShowTimes(st);
						break;
					}

					rs.close();
					count++;
					nextResultSet = cst.getMoreResults();
				}
			} finally {
				//results.close();
			}
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		return movie;
	}
	
	public Movie getMovie(String movieName) 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		Movie movie = new Movie();
		boolean nextResultSet = false;
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "call get_movie_details(?);";
			CallableStatement cst = cn.prepareCall(query);
			cst.setString(1, movieName);
//			String query = "SELECT MovieId, MovieName, ReleaseDate, EndDate, Sypnosis, Duration, ImageURL "
//					+ "FROM movie m "
//					+ "INNER JOIN movie_has_actor ma "
//					+ "ON m.movieid = ma.movie_movieid "
//					+ "INNER JOIN movie_has_genre mg "
//					+ "ON m.movieid = mg.movie_movieid "
//					+ "WHERE MovieName = ?; ";
			//PreparedStatement psmt = cn.prepareStatement(query);
			//psmt.setString(1, movieName);
			nextResultSet = cst.execute();
			try {
				int count = 1;
				while (nextResultSet) {
					ResultSet rs = cst.getResultSet();
					
					switch (count) {
					case 1:
						while (rs.next()){
							movie.setMovieId(rs.getInt("MovieId"));
							movie.setMovieName(rs.getString("MovieName"));
							movie.setReleaseDate(rs.getDate("ReleaseDate"));
							movie.setEndDate(rs.getDate("EndDate"));
							movie.setSypnosis(rs.getString("Sypnosis"));
							movie.setDuration(rs.getInt("Duration"));
							movie.setImageURL(rs.getString("ImageURL"));
						}
						break;
					case 2:
						ArrayList<Actor> ac = new ArrayList<Actor>();
						while (rs.next()){
							Actor a = new Actor();
							a.setActorId(rs.getInt("actorid"));
							a.setActorName(rs.getString("actorname"));
							ac.add(a);
						}
						movie.setActors(ac);
						break;
					case 3:
						ArrayList<Genre> gn = new ArrayList<Genre>();
						while (rs.next()){
							Genre g = new Genre();
							g.setGenreId(rs.getInt("genreid"));
							g.setGenreName(rs.getString("genrename"));
							gn.add(g);
						}
						movie.setGenres(gn);
						break;
					case 4:
						ArrayList<ShowTime> st = new ArrayList<ShowTime>();
						while (rs.next()){
							ShowTime s = new ShowTime();
							s.setShowTimeId(rs.getInt("ShowTimeId"));
							s.setShowTimeDate(rs.getDate("ShowTimeDate"));
							s.setShowTimeTime(rs.getDate("ShowTimeTime"));
							s.setHallId(rs.getInt("Hall_HallId"));
							s.setTheaterId(rs.getInt("Hall_Theater_TheaterId"));
							st.add(s);
						}
						movie.setShowTimes(st);
						break;
					}

//					movie.setMovieId(results.getInt("MovieId"));
//					movie.setMovieName(results.getString("MovieName"));
//					movie.setReleaseDate(results.getDate("ReleaseDate"));
//					movie.setEndDate(results.getDate("EndDate"));
//					movie.setSypnosis(results.getString("Sypnosis"));
//					movie.setDuration(results.getInt("Duration"));
//					movie.setImageURL(results.getString("ImageURL"));
//					while (rs.next()){
//						
//					}
					rs.close();
					count++;
					nextResultSet = cst.getMoreResults();
				}
			} finally {
				//results.close();
			}
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		
		//query = ""
		return movie;
	}
	
//	public ArrayList<Movie> getAllMovies() 
//			throws SQLException, SQLTimeoutException, 
//			ClassNotFoundException, InstantiationException, IllegalAccessException {
//		
//		ArrayList<Movie> movies = new ArrayList<Movie>();
//		
//		Class.forName("com.mysql.jdbc.Driver").newInstance();
//		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
//		try {
//			String query = "SELECT MovieId, MovieName FROM movie WHERE _IsDeleted = 0;";
//			Statement smt = cn.createStatement();
//			ResultSet results = smt.executeQuery(query);
//			try {
//				while (results.next()) {
//					Movie movie = new Movie();
//					movie.setMovieId(results.getInt("MovieId"));
//					movie.setMovieName(results.getString("MovieName"));
//					movies.add(movie);
//				}
//			} finally {
//				results.close();
//			}
//		} catch (SQLException e) {
//			e.getMessage();
//		} finally {
//			cn.close();
//		}
//		return movies;
//	}
	
	/***** UPDATE *****/
	
	public int updateMovie(Movie m) 
			throws InstantiationException, IllegalAccessException, 
			ClassNotFoundException, SQLException {
		int rowId = m.getMovieId();
		boolean success = false;
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "UPDATE MOVIE SET MovieName = ?, ReleaseDate = ?, EndDate = ?, "
					+ "Sypnosis = ?, Duration = ?, ImageURL = ? WHERE movieId = ?;";
			PreparedStatement pstmt = cn.prepareStatement(query);
			pstmt.setString(1, m.getMovieName());
			pstmt.setDate(2, m.getReleaseDate());
			pstmt.setDate(3, m.getEndDate());
			pstmt.setString(4, m.getSypnosis());
			pstmt.setInt(5, m.getDuration());
			pstmt.setString(6, m.getImageURL());
			pstmt.setInt(7, m.getMovieId());
			pstmt.executeUpdate();
			
			query = "DELETE FROM movie_has_genre WHERE movie_movieid = ?; "
					+ "INSERT INTO movie_has_genre (Movie_MovieId, Genre_GenreId) VALUES ";
			ArrayList<Genre> gn = m.getGenres();
			System.out.println(m.getMovieId());
			for (int i = 0; i < gn.size(); i++){
				if (i == gn.size()-1){
					System.out.println("(" + rowId + "," + gn.get(i).getGenreId() + ");");
					query += "(" + rowId + "," + gn.get(i).getGenreId() + ");";
				} else {
					query += "(" + rowId + "," + gn.get(i).getGenreId() + "),";
				}
			}
			pstmt = cn.prepareStatement(query);
			pstmt.setInt(1, m.getMovieId());
			pstmt.execute();
			
			query = "DELETE FROM movie_has_actor WHERE movie_movieid= ?; "
					+ "INSERT INTO movie_has_actor (Movie_MovieId, Actor_ActorId) VALUES ";
			ArrayList<Actor> ac = m.getActors();
			for (int i = 0; i < ac.size(); i++){
				if (i == ac.size()-1){
					System.out.println("(" + rowId + "," + ac.get(i).ActorId + ");");
					query += "(" + rowId + "," + ac.get(i).ActorId + ");";
				} else {
					query += "(" + rowId + "," + ac.get(i).ActorId + "),";
				}
			}
			pstmt = cn.prepareStatement(query);
			pstmt.setInt(1, m.getMovieId());
			pstmt.execute();
			
			success = true;
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		return rowId;
	}
	
	public boolean updateMovie(int movieIdToUpdate, String movieName) 
			throws InstantiationException, IllegalAccessException, 
			ClassNotFoundException, SQLException {
		int rowsAffected = 0;
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "UPDATE movie set MovieName = ? where MovieId = ?";
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setString(1, movieName);
			psmt.setInt(2, movieIdToUpdate);
			rowsAffected = psmt.executeUpdate();
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		
		return rowsAffected == 1;
	}
	
	/***** DELETE *****/
	
	public boolean deleteMovie(int movieId) 
			throws InstantiationException, IllegalAccessException, 
			ClassNotFoundException, SQLException {
		int rowsAffected = 0;
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "UPDATE movie set _IsDeleted = 1 where MovieId = ?";
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setInt(1, movieId);
			rowsAffected = psmt.executeUpdate();
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		
		return rowsAffected == 1;
	}
}
