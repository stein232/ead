
package mysqlDatabase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Genre;

public class GenreManager {
	
	/***** CREATE *****/
	
	public boolean addGenre(String genreName) 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		int rowsAffected = 0;
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "INSERT INTO genre (`GenreName`) VALUES (?)";
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setString(1, genreName);
			rowsAffected = psmt.executeUpdate();
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		
		return rowsAffected == 1;
	}
	
	/***** RETRIEVE *****/
	
	public Genre getGenre(int genreId) 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		Genre genre = new Genre();
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "SELECT GenreId, GenreName FROM genre WHERE GenreId = ?;";
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setInt(1, genreId);
			ResultSet results = psmt.executeQuery();
			try {
				while (results.next()) {
					genre.setGenreId(results.getInt("GenreId"));
					genre.setGenreName(results.getString("GenreName"));
				}
			} finally {
				results.close();
			}
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		return genre;
	}
	
	public ArrayList<Genre> getAllGenres() 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		ArrayList<Genre> genres = new ArrayList<Genre>();
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "SELECT GenreId, GenreName FROM genre WHERE _IsDeleted = 0;";
			Statement smt = cn.createStatement();
			ResultSet results = smt.executeQuery(query);
			try {
				while (results.next()) {
					Genre genre = new Genre();
					genre.setGenreId(results.getInt("GenreId"));
					genre.setGenreName(results.getString("GenreName"));
					genres.add(genre);
				}
			} finally {
				results.close();
			}
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		return genres;
	}
	
	/***** UPDATE *****/
	
	public boolean updateGenre(int genreIdToUpdate, String genreName) 
			throws InstantiationException, IllegalAccessException, 
			ClassNotFoundException, SQLException {
		int rowsAffected = 0;
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "UPDATE genre set GenreName = ? where GenreId = ?";
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setString(1, genreName);
			psmt.setInt(2, genreIdToUpdate);
			rowsAffected = psmt.executeUpdate();
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		
		return rowsAffected == 1;
	}
	
	/***** DELETE *****/
	
	public boolean deleteGenre(int genreIdToUpdate) 
			throws InstantiationException, IllegalAccessException, 
			ClassNotFoundException, SQLException {
		int rowsAffected = 0;
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "UPDATE genre set _IsDeleted = 1 where GenreId = ?";
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setInt(1, genreIdToUpdate);
			rowsAffected = psmt.executeUpdate();
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		
		return rowsAffected == 1;
	}
}
