package mysqlDatabase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Genre;
import model.Movie;
import model.MovieTicket;
import viewModel.MonthYearViewModel;
import viewModel.MovieViewModel;

public class MovieTicketManager {
	/***** CREATE *****/
	
	public boolean addMovieTicket(MovieTicket movieTicket) 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		int rowsAffected = 0;
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "INSERT INTO movieticket (`Seat_Row`,`Seat_SeatNumber`,`ShowTime_ShowTimeId`,`ShowTime_Hall_HallId`,`ShowTime_Hall_Theater_TheaterId`,`ShowTime_Movie_MovieId`,`MemberId`,`Price`,`MovieTypeId`) "
					+ "VALUES "
					+ "(?,?,?,?,?,?,?,?,?);";
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setString(1, movieTicket.getSeatRow());
			psmt.setInt(2, movieTicket.getSeatNumber());
			psmt.setInt(3, movieTicket.getShowTimeId());
			psmt.setInt(4, movieTicket.getHallId());
			psmt.setInt(5, movieTicket.getTheaterId());
			psmt.setInt(6, movieTicket.getMovieId());
			psmt.setInt(7, movieTicket.getMemberId());
			psmt.setInt(8, movieTicket.getPrice());
			psmt.setInt(9, movieTicket.getMovieTypeId());
			rowsAffected = psmt.executeUpdate();
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		
		return rowsAffected == 1;
	}
	
	/***** RETRIEVE *****/
	
	public MovieTicket getMovieTicket(int movieTicketId) 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		MovieTicket movieTicket = new MovieTicket();
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "SELECT MovieTicketId, Seat_Row, Seat_SeatNumber, ShowTime_ShowTimeId, ShowTime_Hall_HallId, ShowTime_Hall_Theather_TheaterId, ShowTime_Movie_MovieId, MemberId, PurchasedDateTime, Price, MovieTypeId "
					+ "FROM movieticket "
					+ "WHERE MovieTicketId = ?;";
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setInt(1, movieTicketId);
			ResultSet results = psmt.executeQuery();
			try {
				results.next();
				movieTicket.setMovieTicketId(results.getInt("MovieTicketId"));
				movieTicket.setSeatRow(results.getString("Seat_Row"));
				movieTicket.setSeatNumber(results.getInt("Seat_SeatNumber"));
				movieTicket.setShowTimeId(results.getInt("ShowTime_ShowTimeId"));
				movieTicket.setHallId(results.getInt("ShowTime_Hall_HallId"));
				movieTicket.setTheaterId(results.getInt("ShowTime_Hall_Theather_TheaterId"));
				movieTicket.setMovieId(results.getInt("ShowTime_Movie_MovieId"));
				movieTicket.setMemberId(results.getInt("MemberId"));
				movieTicket.setPurchasedDate(results.getTimestamp("PurchasedDateTime"));
				movieTicket.setPrice(results.getInt("Price"));
				movieTicket.setMovieTicketId(results.getInt("MovieTypeId"));
			} finally {
				results.close();
			}
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		return movieTicket;
	}
	
	public ArrayList<MovieTicket> getAllMovieTicketsOfMember(int memberId) 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		ArrayList<MovieTicket> movieTickets = new ArrayList<MovieTicket>();
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "SELECT MovieTicketId, Seat_Row, Seat_SeatNumber, ShowTime_ShowTimeId, ShowTime_Hall_HallId, ShowTime_Hall_Theater_TheaterId, ShowTime_Movie_MovieId, MemberId, PurchasedDateTime, Price, MovieTypeId "
					+ "FROM movieticket "
					+ "WHERE MemberId = ?;";
//			String query = "SELECT MovieTicketId, Seat_Row, Seat_SeatNumber, ShowTime_ShowTimeId, ShowTime_Hall_HallId, ShowTime_Hall_Theater_TheaterId, " + 
//					"ShowTime_Movie_MovieId, MemberId, PurchasedDateTime, Price, MovieTypeName, movieticket.`MovieTypeId`, HallNo " +
//					"FROM movieticket " +
//					"INNER JOIN movietype " +
//					"ON movieticket.`MovieTypeId` = movietype.`MovieTypeId` " +
//					"INNER JOIN hall " +
//					"ON movieticket.`ShowTime_Hall_HallId` = hall.`HallId` " +
//					"WHERE MemberId = ?;";
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setInt(1, memberId);
			ResultSet results = psmt.executeQuery();
			try {
				while (results.next()) {
					MovieTicket movieTicket = new MovieTicket();
					movieTicket.setMovieTicketId(results.getInt("MovieTicketId"));
					movieTicket.setSeatRow(results.getString("Seat_Row"));
					movieTicket.setSeatNumber(results.getInt("Seat_SeatNumber"));
					movieTicket.setShowTimeId(results.getInt("ShowTime_ShowTimeId"));
					movieTicket.setHallId(results.getInt("ShowTime_Hall_HallId"));
					movieTicket.setTheaterId(results.getInt("ShowTime_Hall_Theater_TheaterId"));
					movieTicket.setMovieId(results.getInt("ShowTime_Movie_MovieId"));
					movieTicket.setMemberId(results.getInt("MemberId"));
					movieTicket.setPurchasedDate(results.getTimestamp("PurchasedDateTime"));
					movieTicket.setPrice(results.getInt("Price"));
					movieTicket.setMovieTicketId(results.getInt("MovieTypeId"));
					movieTickets.add(movieTicket);
				}
			} finally {
				results.close();
			}
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		return movieTickets;
	}
	
	public ArrayList<MovieTicket> getAllMovieTicketsOfShowTime(int showTimeId)
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		ArrayList<MovieTicket> movieTickets = new ArrayList<MovieTicket>();
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "SELECT MovieTicketId, Seat_Row, Seat_SeatNumber, ShowTime_ShowTimeId, ShowTime_Hall_HallId, ShowTime_Hall_Theater_TheaterId, ShowTime_Movie_MovieId, MemberId, PurchasedDateTime, Price, MovieTypeId "
					+ "FROM movieticket "
					+ "WHERE ShowTime_ShowTimeId = ?;";
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setInt(1, showTimeId);
			ResultSet results = psmt.executeQuery();
			try {
				while (results.next()) {
					MovieTicket movieTicket = new MovieTicket();
					movieTicket.setMovieTicketId(results.getInt("MovieTicketId"));
					movieTicket.setSeatRow(results.getString("Seat_Row"));
					movieTicket.setSeatNumber(results.getInt("Seat_SeatNumber"));
					movieTicket.setShowTimeId(results.getInt("ShowTime_ShowTimeId"));
					movieTicket.setHallId(results.getInt("ShowTime_Hall_HallId"));
					movieTicket.setTheaterId(results.getInt("ShowTime_Hall_Theater_TheaterId"));
					movieTicket.setMovieId(results.getInt("ShowTime_Movie_MovieId"));
					movieTicket.setMemberId(results.getInt("MemberId"));
					movieTicket.setPurchasedDate(results.getTimestamp("PurchasedDateTime"));
					movieTicket.setPrice(results.getInt("Price"));
					movieTicket.setMovieTicketId(results.getInt("MovieTypeId"));
					movieTickets.add(movieTicket);
				}
			} finally {
				results.close();
			}
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		return movieTickets;
	}
	
	public ArrayList<MovieTicket> getAllMovieTickets() 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		ArrayList<MovieTicket> movieTickets = new ArrayList<MovieTicket>();
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "SELECT MovieTicketId, Seat_Row, Seat_SeatNumber, ShowTime_ShowTimeId, ShowTime_Hall_HallId, ShowTime_Hall_Theater_TheaterId, ShowTime_Movie_MovieId, MemberId, PurchasedDateTime, Price, MovieTypeId "
					+ "FROM movieticket;";
			Statement smt = cn.createStatement();
			ResultSet results = smt.executeQuery(query);
			try {
				while (results.next()) {
					MovieTicket movieTicket = new MovieTicket();
					movieTicket.setMovieTicketId(results.getInt("MovieTicketId"));
					movieTicket.setSeatRow(results.getString("Seat_Row"));
					movieTicket.setSeatNumber(results.getInt("Seat_SeatNumber"));
					movieTicket.setShowTimeId(results.getInt("ShowTime_ShowTimeId"));
					movieTicket.setHallId(results.getInt("ShowTime_Hall_HallId"));
					movieTicket.setTheaterId(results.getInt("ShowTime_Hall_Theater_TheaterId"));
					movieTicket.setMovieId(results.getInt("ShowTime_Movie_MovieId"));
					movieTicket.setMemberId(results.getInt("MemberId"));
					movieTicket.setPurchasedDate(results.getTimestamp("PurchasedDateTime"));
					movieTicket.setPrice(results.getInt("Price"));
					movieTicket.setMovieTicketId(results.getInt("MovieTypeId"));
					movieTickets.add(movieTicket);
				}
			} finally {
				results.close();
			}
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		return movieTickets;
	}
	
	public ArrayList<MovieViewModel> getTopTenMovies(int month, int year) 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
//		Populates the top 10 movies based on revenue, sorted by revenue in descending order
//		Uses movieviewmodel class which is an extension of movie class

		ArrayList<MovieViewModel> movies = new ArrayList<MovieViewModel>();
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "SELECT count(*) `Number of tickets`, sum(price) `Revenue`, moviename, showtime_movie_movieId, Sypnosis, Duration, ImageURL FROM movieticket " +
							"INNER JOIN movie " +
							"ON `ShowTime_Movie_MovieId` = MovieId " +
							"WHERE MONTH(PurchasedDateTime) = ? AND YEAR(PurchasedDateTime) = ? " +
							"GROUP BY showtime_movie_movieid " + 
							"ORDER BY `Revenue` desc " + 
							"LIMIT 10;";
			
			
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setInt(1, month);
			psmt.setInt(2, year);
			ResultSet rs = psmt.executeQuery();
			
			MovieViewModel mvm = null;
			while (rs.next()){
				mvm = new MovieViewModel();
				mvm.setRevenue(rs.getString("Revenue"));
				mvm.setNumberOfTickets(rs.getString("Number of tickets"));
				mvm.setMovieId(rs.getInt("showtime_movie_movieId"));
				mvm.setMovieName(rs.getString("MovieName"));
				mvm.setSypnosis(rs.getString("Sypnosis"));
				mvm.setDuration(rs.getInt("Duration"));
				mvm.setImageURL(rs.getString("ImageURL"));
				movies.add(mvm);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cn.close();
		}
		return movies;
	}
	
	public ArrayList<MonthYearViewModel> getMovieTicStatDateTime() 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
//		Populates the month and year dropdownlist

		MonthYearViewModel myvm = null;
		ArrayList<MonthYearViewModel> availDates = new ArrayList<>();
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "SELECT MONTH(purchaseddatetime) `Month`, YEAR(purchaseddatetime) `Year` " +
					"FROM movieticket " +
					"GROUP BY `Year`, `Month` " +
					"ORDER BY `Year` desc, `Month` desc;";
			PreparedStatement psmt = cn.prepareStatement(query);
			ResultSet rs = psmt.executeQuery();
			
			while (rs.next()){
				int mMonth = rs.getInt("Month");
				int mYear = rs.getInt("Year");
				String userFriendlyMonth = "";
				switch (mMonth) {
				case 1:
					userFriendlyMonth = "January";
					break;
				case 2:
					userFriendlyMonth = "February";
					break;
				case 3:
					userFriendlyMonth = "March";
					break;
				case 4:
					userFriendlyMonth = "April";
					break;
				case 5:
					userFriendlyMonth = "May";
					break;
				case 6:
					userFriendlyMonth = "June";
					break;
				case 7:
					userFriendlyMonth = "July";
					break;
				case 8:
					userFriendlyMonth = "August";
					break;
				case 9:
					userFriendlyMonth = "September";
					break;
				case 10:
					userFriendlyMonth = "October";
					break;
				case 11:
					userFriendlyMonth = "November";
					break;
				case 12:
					userFriendlyMonth = "December";
					break;
				default:
					break;
				}
				myvm = new MonthYearViewModel();
				myvm.setFriendlyDate(userFriendlyMonth + " " + mYear);
				myvm.setSqlParamDate(mMonth + "|" + mYear);
				availDates.add(myvm);
			}
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		return availDates;
	}
}
