package mysqlDatabase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.util.ArrayList;

import model.Seat;

public class SeatManager {
	
	/***** RETRIEVE *****/
	
	public ArrayList<ArrayList<Seat>> getSeatsForHall(int showTimeId) 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		ArrayList<ArrayList<Seat>> seats = new ArrayList<ArrayList<Seat>>();
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "SELECT Hall_HallID, `Row`, SeatNumber "
					+ "FROM seat "
					+ "WHERE Hall_HallID = (SELECT Hall_HallID FROM showtime WHERE ShowTimeId = ?);";
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setInt(1, showTimeId);
			ResultSet results = psmt.executeQuery();
			
			try {
				results.next();
				String row = results.getString("Row");
				ArrayList<Seat> seatRow = new ArrayList<Seat>();
				do {
					Seat seat = new Seat();
					seat.setHallId(results.getInt("Hall_HallID"));
					seat.setRow(results.getString("Row"));
					seat.setSeatNumber(results.getInt("SeatNumber"));
					
					if (!row.equals(results.getString("Row"))) {
						seats.add(seatRow);
						row = results.getString("Row");
						seatRow = new ArrayList<Seat>();
					}
					
					seatRow.add(seat);
				} while (results.next());
				
				seats.add(seatRow);
			} finally {
				results.close();
			}
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		
		return seats;
	}
}
