package mysqlDatabase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;

public class StaffManager {
	
	public boolean checkStaff(String email, String password) 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		boolean login = false;
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "SELECT COUNT(*) FROM staff "
					+ " WHERE Email = ? AND `Password` = ?;" ;
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setString(1, email);
			psmt.setString(2, password);
			String a = psmt.toString();
			ResultSet results = psmt.executeQuery();
			results.absolute(1);
			int b = results.getInt(1);
			if (results.getInt(1) == 1) {
				login = true;
			}
			results.close();
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		return login;
	}
}
