package mysqlDatabase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;


import com.mysql.jdbc.Statement;

import encryption.Security;
import model.Member;

public class MemberManager {
//	CREATE
	public int CreateMember(Member member) 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		int rowsAffected = 0;
		int memberId = 0;
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "INSERT INTO member (Email, Password, Name, ContactNumber, MailingAddress, CreditCardNumber) " 
							+ "VALUES (?,?,?,?,?,?)";
			PreparedStatement psmt = cn.prepareStatement(query, java.sql.Statement.RETURN_GENERATED_KEYS);
			psmt.setString(1, member.getEmail());
			psmt.setString(2, member.getPassword());
			psmt.setString(3, member.getName());
			psmt.setString(4, member.getContactNumber());
			psmt.setString(5, member.getMailingAddress());
			psmt.setString(6, member.getCreditCardNumber());
			rowsAffected = psmt.executeUpdate();
			
			ResultSet rs = psmt.getGeneratedKeys();
			if (rs.next()){
				memberId = rs.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cn.close();
		}
		return memberId;
	}
	
//	UPDATE
	public boolean UpdateMember(Member member) 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		int rowsAffected = 0;
		boolean isNewPassword = (member.getPassword() != null);
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "UPDATE member SET Email = ?, ";
			if (isNewPassword)
				query += "Password = ?, ";
			query += "Name = ?, ContactNumber = ?," 
							+ " MailingAddress = ?, CreditCardNumber = ? WHERE memberId = ?";
			
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setString(1, member.getEmail());
			
			if (isNewPassword) {
				psmt.setString(2, member.getPassword());
				psmt.setString(3, member.getName());
				psmt.setString(4, member.getContactNumber());
				psmt.setString(5, member.getMailingAddress());
				psmt.setString(6, member.getCreditCardNumber());
				psmt.setInt(7, member.getMemberId());
			} else {
				psmt.setString(2, member.getName());
				psmt.setString(3, member.getContactNumber());
				psmt.setString(4, member.getMailingAddress());
				psmt.setString(5, member.getCreditCardNumber());
				psmt.setInt(6, member.getMemberId());
			}
			
			rowsAffected = psmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cn.close();
		}
		return rowsAffected == 1;
	}
	
//	RETRIEVE
	public Member GetOneMember(int id) 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		Member nm = null;
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "SELECT email, password, memberid, name, contactnumber, mailingaddress, creditcardnumber " +
							"FROM member WHERE memberid = ?";
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setInt(1, id);
			ResultSet rs = psmt.executeQuery();
			while (rs.next()){
				nm = new Member();
				nm.setMemberId(rs.getInt("memberid"));
				nm.setEmail(rs.getString("email"));
				nm.setPassword(rs.getString("password"));
				nm.setName(rs.getString("name"));
				nm.setContactNumber(rs.getString("contactnumber"));
				nm.setMailingAddress(rs.getString("mailingaddress"));
				nm.setCreditCardNumber(rs.getString("creditcardnumber"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cn.close();
		}
		return nm;
	}
	
	public Member Login(Member m) 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		Security pw = new Security();
		Member nm = null;
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			//String query = "SELECT COUNT(*) as count FROM member where Email = ? AND password = ?";
			String query = "SELECT memberid, email, password, memberid, name, contactnumber, mailingaddress, creditcardnumber " +
					"FROM member WHERE email = ? AND password = ?";
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setString(1, m.getEmail());
			psmt.setString(2, pw.getHash(m.getPassword()));
			ResultSet rs = psmt.executeQuery();
			if (rs != null){
				while (rs.next()){
					nm = new Member();
					nm.setMemberId(rs.getInt("memberid"));
					nm.setEmail(rs.getString("email"));
					nm.setPassword(rs.getString("password"));
					nm.setName(rs.getString("name"));
					nm.setContactNumber(rs.getString("contactnumber"));
					nm.setMailingAddress(rs.getString("mailingaddress"));
					nm.setCreditCardNumber(rs.getString("creditcardnumber"));
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cn.close();
		}
		return nm;
	}
	
	public boolean GetAllMovies(String id) 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		int rowsAffected = 0;
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
//			TODO 
//			String query = "SELECT email, password, memberid, name, contactnumber, mailingaddress, creditcardnumber " +
//							"FROM movieticket";
//			PreparedStatement psmt = cn.prepareStatement(query);
//			ResultSet rs = psmt.executeQuery();
//			while (rs.next()){
//				Member m = new Member();
//				m.setMemberId(rs.getInt("memberid"));
//				m.setEmail(rs.getString("email"));
//				m.setPassword(rs.getString("password"));
//				m.setName(rs.getString("name"));
//				m.setContactNumber(rs.getString("contactnumber"));
//				m.setMailingAddress(rs.getString("mailingaddress"));
//				m.setCreditCardNumber(rs.getString("creditcardnumber"));
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
		} finally {
			cn.close();
		}
		return rowsAffected == 1;
	}
}
