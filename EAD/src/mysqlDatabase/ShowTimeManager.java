package mysqlDatabase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import model.ShowTime;

public class ShowTimeManager {
	
	/***** CREATE *****/
	
	public boolean addShowTime(ShowTime st) 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		int rowsAffected = 0;
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "INSERT INTO showtime "
					+ "(`Hall_Theater_TheaterId`, `Hall_HallId`, `Movie_MovieId`, `ShowTimeDate`, `ShowTimeTime`, `MovieTypeId`) "
					+ "VALUES "
					+ "(?, ?, ?, ?, ?, ?)";
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setInt(1, st.getTheaterId());
			psmt.setInt(2, st.getHallId());
			psmt.setInt(3, st.getMovieId());
			psmt.setDate(4, st.getShowTimeDate());
			psmt.setTime(5, new Time(st.getShowTimeTime().getTime()));
			psmt.setInt(6, st.getMovieTypeId());
			rowsAffected = psmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			cn.close();
		}
		
		return rowsAffected == 1;
	}
	
	/***** RETRIEVE *****/
	
	public ShowTime getShowTime(int showTimeId) 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		ShowTime showTime = new ShowTime();
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "SELECT Hall_Theater_TheaterId, TheaterName, Hall_HallId, Movie_MovieId, " +
					"ShowTimeId, ShowTimeDate, ShowTimeTime, showtime.MovieTypeId, MovieTypeName, HallNo " +
					"FROM showtime INNER JOIN theater " +
					"ON showtime.Hall_Theater_TheaterId = theater.TheaterId " +
					"INNER JOIN movietype " +
					"ON showtime.MovieTypeId = movietype.MovieTypeId " +
					"INNER JOIN hall " +
					"ON showtime.`Hall_HallId` = hall.`HallId` " +
					"WHERE ShowTimeId = ?;";
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setInt(1, showTimeId);
			ResultSet results = psmt.executeQuery();
			try {
				results.next();
				showTime.setMovieId(results.getInt("Movie_MovieId"));
				showTime.setTheaterId(results.getInt("Hall_Theater_TheaterId"));
				showTime.setTheaterName(results.getString("TheaterName"));
				showTime.setHallId(results.getInt("Hall_HallId"));
				showTime.setShowTimeId(results.getInt("ShowTimeId"));
				showTime.setShowTimeDate(results.getDate("ShowTimeDate"));
				showTime.setMovieTypeId(results.getInt("MovieTypeId"));
				showTime.setMovieTypeName(results.getString("MovieTypeName"));
				showTime.setHallNo(results.getInt("HallNo"));
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				try {
					java.util.Date date = sdf.parse("2015-01-01 "+results.getTime("ShowTimeTime"));
					showTime.setShowTimeTime(date);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} finally {
				results.close();
			}
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		return showTime;
	}
	public ArrayList<ShowTime> getShowTimes(int movieId) 
			throws SQLException, SQLTimeoutException, 
			ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		ArrayList<ShowTime> showTimes = new ArrayList<ShowTime>();
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
//			String query = "SELECT Hall_Theater_TheaterId, TheaterName, Hall_HallId, Movie_MovieId, ShowTimeId, ShowTimeDate, ShowTimeTime, showtime.MovieTypeId, MovieTypeName, HallNo "
//					+ "FROM showtime INNER JOIN theater "
//					+ "ON showtime.Hall_Theater_TheaterId = theater.TheaterId "
//					+ "INNER JOIN movietype "
//					+ "ON showtime.MovieTypeId = movietype.MovieTypeId " 
//					+ "WHERE Movie_MovieId = ?;";
			String query = "SELECT Hall_Theater_TheaterId, TheaterName, Hall_HallId, Movie_MovieId, "
					+ "ShowTimeId, ShowTimeDate, ShowTimeTime, showtime.MovieTypeId, MovieTypeName, HallNo "
					+ "FROM showtime "
					+ "INNER JOIN theater "
					+ "ON showtime.Hall_Theater_TheaterId = theater.TheaterId "
					+ "INNER JOIN movietype "
					+ "ON showtime.MovieTypeId = movietype.MovieTypeId "
					+ "INNER JOIN hall "
					+ "ON showtime.`Hall_HallId` = hall.`HallId` "
					+ "WHERE Movie_MovieId = ? "
					+ "ORDER BY Hall_Theater_TheaterId, ShowTimeDate, ShowTimeTime; ";
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setInt(1, movieId);
			ResultSet results = psmt.executeQuery();
			try {
				while (results.next()) {
					ShowTime showTime = new ShowTime();
					showTime.setMovieId(movieId);
					showTime.setTheaterId(results.getInt("Hall_Theater_TheaterId"));
					showTime.setTheaterName(results.getString("TheaterName"));
					showTime.setHallId(results.getInt("Hall_HallId"));
					showTime.setShowTimeId(results.getInt("ShowTimeId"));
					showTime.setShowTimeDate(results.getDate("ShowTimeDate"));
					showTime.setMovieTypeId(results.getInt("MovieTypeId"));
					showTime.setMovieTypeName(results.getString("MovieTypeName"));
					showTime.setHallNo(results.getInt("HallNo"));
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					try {
						java.util.Date date = sdf.parse("2015-01-01 "+results.getTime("ShowTimeTime"));
						showTime.setShowTimeTime(date);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					showTimes.add(showTime);
				}
			} finally {
				results.close();
			}
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		return showTimes;
	}
	
	/***** DELETE *****/
	
	public boolean deleteShowTime(int theaterId, int hallId, int movieId, int showTimeId) 
			throws InstantiationException, IllegalAccessException, 
			ClassNotFoundException, SQLException {
		int rowsAffected = 0;
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection cn = DriverManager.getConnection(DatabaseConnection.CONNECTION_STRING);
		try {
			String query = "DELETE FROM showtime "
					+ "WHERE Hall_Theater_TheaterId = ?, Hall_HallId = ?, Movie_MovieId = ?, ShowTimeId = ?";
			PreparedStatement psmt = cn.prepareStatement(query);
			psmt.setInt(1, theaterId);
			psmt.setInt(2, hallId);
			psmt.setInt(3, movieId);
			psmt.setInt(4, showTimeId);
			rowsAffected = psmt.executeUpdate();
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			cn.close();
		}
		
		return rowsAffected == 1;
	}
}
