<%@page import="mysqlDatabase.ActorManager"%>
<%@page import="model.Actor"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="LoginChecker.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@include file="AdminLinkScript.jsp" %>
<title>All Actors</title>
</head>
<body>
<%
	ArrayList<Actor> actors = null;
	
	try{
		actors = new ActorManager().getAllActors();	
	} catch (Exception e) {
		e.printStackTrace();
	}
	
%>
<%@include file="AdminNav.jsp" %>
	<div class="padTop">
		<div id="pgHeader" class="row inlineView">
			<div class="col-sm-1"></div>
			<div id="actionBtn" class="col-sm-10">
				<h3>All Actors</h3>
				<a href="AddActor.jsp" class="btn btn-primary taskbarBtn">Add Actor</a>
			</div>
			<div class="col-sm-1"></div>
		</div>
		<div class="row">
			<div class="col-sm-1"></div>
			<div class="col-sm-10">
				<hr></hr>
			</div>
			<div class="col-sm-1"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div id="content" class="col-md-8 centerAlign">
				<div id="error"><%= actors == null ? "Cannot connect to database." : "" %></div>
				<div id="message"><%= session.getAttribute("message") != null ? (String)session.getAttribute("message") : "" %></div>
				<table id="j-table" class="dataTable">
				<thead>
					<th>Actor</th>
					<th>Action</th>
				</thead>
			</table>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>


	
	
	
<%
	session.removeAttribute("message");
%>
<script>
	$("#j-table").DataTable({
		ajax: {
			url: "../GetActors",
			dataSrc: "",
		},
		order:[[0, "asc"]],
		columns: [
			{data: "ActorName"},
			null,
		],
		columnDefs: [
			{
				targets: 1,
				orderable: false,
				render: function(data, type, row, meta) {
					return (
						'<form action="UpdateActor.jsp" method="post" class="jtableInline">\
							<input type="hidden" name="actorId" value="' + row.ActorId + '"/>\
							<input type="submit" value="Update" class="update-btn btn" />\
						</form>\
						<form action="ProcessActor?action=delete" method="post" class="jtableInline">\
							<input type="hidden" name="actorId" value="' + row.ActorId + '" />\
							<input type="hidden" name="actorName" value="' + row.ActorName + '" />\
							<input type="submit" value="Delete" class="delete-btn btn" />\
						</form>'
					);
				},
			},
		],
	});
</script>
<%@include file="AdminFooter.jsp" %>