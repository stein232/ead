<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="viewModel.MonthYearViewModel"%>
<%@page import="mysqlDatabase.MovieTicketManager"%>
<%@page import="javax.servlet.RequestDispatcher"%>
<%@page import="java.util.ArrayList"%>
<%@include file="LoginChecker.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../../css/admheader.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="../../css/styles.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<title>Statistics</title>
</head>
<%@include file="AdminNav.jsp" %>
<%
	ArrayList<MonthYearViewModel> myvm = null;
	String status = request.getParameter("status");
	if (status == null){
		response.sendRedirect("../Admin/MovieStat");
	} else {
		myvm = (ArrayList<MonthYearViewModel>)request.getAttribute("dates");
	}
%>
	<div class="padTop">
		<div id="pgHeader" class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-6 alignLeft headerStyle">
				<h2 class="headerBaseline">Statistics</h2>
			</div>
			<div id="actionBtn" class="col-sm-2">
			</div>
			<div class="col-sm-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8"><hr></hr></div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<p>Select the time period to show data for top 10 best selling movies:</p>
				<div class="row">
					<div class="col-md-4">
						<select name="date-ddl" id="date-ddl" class="form-control textboxStyle">
							<% 
							if (myvm != null){
								for(int i = 0; i < myvm.size(); i++){
								%>
								<option value="<%= myvm.get(i).getSqlParamDate() %>">
								<%= myvm.get(i).getFriendlyDate() %>
								</option>
								<%
								}
							}
							
							%>
						</select>
					</div>
				</div>
				<div class="row padTop30">
					<div id="content">
					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
	<script>
	function makeQuery(dateSelected){
		/* Ajax call to make query */
		$("#content").empty().append("<div id=\"waitMsg\"><p>Please wait...</p></div>");
		var date = {
				sqlParamMonth: dateSelected[0],
				sqlParamYear: dateSelected[1]
		}
		console.log(date);
		$.ajax({
			type: "POST",
			url: "../Admin/MovieStat",
			data: { mDate : JSON.stringify(date) },
			dataType: "json",
			}).done(populateFromData)
			.fail((error) => {
				console.log(error.statusText);
			});
	}
	
	function populateFromData(data){
		console.log(data);
		var displayText = "";
		var count = 1;
		for (var i = 0; i < data.length; i++){
			if (count == 1){
				displayText += "<div class=\"row\">";
			}
			displayText += "<a href=\"/Admin/UpdateMovie.jsp?id=" + data[i].MovieId + "\"><div class=\"col-md-4 movTile\">";
			displayText += "<div class=\"numbering\">" + (i+1) + "</div>";
			displayText += "<img class=\"movThumbnail\" src=\""+ data[i].ImageURL +"\"></img>";
			displayText += "<p>" + data[i].MovieName + "</p>";
			displayText += "<p class=\"padBottomNone padTopNone\">Earned S$" + data[i].revenue + "</p>";
			displayText += "<p class=\"padBottomNone padTopNone\">" + data[i].numberOfTickets + " tickets sold</p>";
			displayText += "</div></a>";
			if (count == 3) {
				displayText += "</div>";
				count = 0;
			}
			count++;
		}
		$("#content").empty().append(displayText);
	}
	
	$(document).ready(function() {
		$("#date-ddl").change(function() {
		    var dateSelected = $("#date-ddl").val().split("|");
		    console.log(dateSelected);
		    makeQuery(dateSelected);
		});
		
		if ($("#date-ddl").val() != ""){
			var dateSelected = $("#date-ddl").val().split("|");
		    makeQuery(dateSelected);
		}
	});
	
	</script>
	
<%@include file="AdminFooter.jsp" %>