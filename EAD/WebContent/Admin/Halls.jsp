<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../../css/admheader.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="../../css/styles.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<title>Theatre and Hall Management</title>
</head>
<%@include file="AdminNav.jsp" %>
<div id="toast" class="toastVisible"></div>
<div id="ongoingToast" class="toastVisible"></div>
<div class="padTop">
	<div id="pgHeader" class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-6 alignLeft headerStyle">
			<h2 class="headerBaseline">Theatre and Hall Management</h2>
		</div>
		<div id="actionBtn" class="col-sm-2">
		</div>
		<div class="col-sm-2"></div>
	</div>
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8"><hr></hr></div>
		<div class="col-md-2"></div>
	</div>
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="form-group">
	   			<p>Select a theater to view and add halls:</p>
	   			<div class="row">
	   				<div class="col-sm-4">
					    <select id="theaterDdl" class="form-control textboxStyle"></select>
					</div>
					<button type="button" id="addTheaterBtn" class="btn btn-primary" data-toggle="modal" 
		                        data-target="#addTheaterModal">Add Theater</button>
					<button id="addHallBtn" class="btn btn-default">Add Hall</button>
	   			</div>
	   		</div>
	   		<div class="hallArea">
	   			<p>Halls:</p>
	   			<div id="hallBoxes">
	   			</div>
	   		</div>
	   		<div id="addTheaterModal" class="modal fade bs-example-modal-lg" 
	                    tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Add new theater</h4>
                        </div>
                        <div class="modal-body">
                            <input type="text" id="newTheaterName" class="form-control" placeholder="Enter theater name"/>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" id="addTheaterSaveBtn" class="btn btn-primary" data-dismiss="modal">Add</button>
                        </div>
                    </div> <%--Modal Content--%>
                </div>  <%--Modal Dialog--%>
            </div>  <%--Modal--%>
		</div>
		<div class="col-md-2"></div>
   	</div>
</div>
	<script>
	function toggleOngoingToast(message) {
    	$("#ongoingToast").html("<p>"+message+"</p>").toggleClass("toastVisible");
    }
	
	function toggleToast(message) {
    	$("#toast").html("<p>"+message+"</p>").toggleClass("toastVisible").delay(2500).queue(function(){$(this).toggleClass("toastVisible").dequeue()});
    }
	
	function getTheatres(){
		toggleOngoingToast("Please wait...");
		$.ajax({
            type: 'GET',
            url: '../GetTheaters',
            dataType: 'json'
        }).done((data) => {
        	console.log(data);
        	$("#theaterDdl").empty();
        	for (var i = 0; i < data.length; i++){
        		$("#theaterDdl").append("<option value=\""+data[i].TheaterId+"\">"+data[i].TheaterName+"</option>");
        		console.log("<option value=\""+data[i].TheaterId+"\">"+data[i].TheaterName+"</option>");
        	}
        	$("#theaterDdl").trigger("change");
        	toggleOngoingToast("");
        }).fail((e) => {
        	toggleToast("Error: " + e);
        }); 
	}
	
	function getHalls(){
		toggleOngoingToast("Please wait...");
		var theaterId = {
			"theater": $("#theaterDdl").val()
		}
		console.log(theaterId);
		$.ajax({
            type: 'POST',
            url: '../GetHalls',
            dataType: 'json',
            data: { theater : JSON.stringify(theaterId) }
        }).done((data) => {
        	toggleOngoingToast("");
        	console.log(data);
        	$("#hallBoxes").empty();
        	for (var i = 0; i < data.length; i++){
        		$("#hallBoxes").append("<div class=\"box\" value=\"" + data[i].HallNo + "\">" + data[i].HallNo  + "</div>");
        	}
        	
        }).fail((e) => {
        	toggleToast("Error: " + e);
        }); 
	}
	
	function addHall(){
		toggleOngoingToast("Please wait...");
		var theaterId = $("#theaterDdl").val();
		var urlString = '../Hall/AddHall/' + theaterId;
		$.ajax({
            type: 'POST',
            url: urlString,
            dataType: 'json'
        }).done((data) => {
        	toggleOngoingToast("");
        	toggleToast("Successfully added a hall");
        	getHalls();
        }).fail((e) => {
        	toggleToast("Error: " + e);
        }); 
	}
	
	$(document).ready(function(){
		getTheatres();
		
		$("#theaterDdl").change(function(){
			getHalls();
		});
		
		$("#addHallBtn").click(function(){
			addHall();
		});
		
		$("#addTheaterSaveBtn").click(function () {
			var theater = $("#newTheaterName").val();
			var urlString =  '../Hall/AddTheater/' + theater;
            $.ajax({
                type: 'POST',
                url: urlString,
                dataType: 'json',
            }).done((data) => {
            	toggleToast("Successfully added " + theater);
            	getTheatres();
            }).fail((e) => {
            	console.log(e);
            	toggleToast("Error: " + e);
            });
        });
	});
	</script>
<%@include file="AdminFooter.jsp" %>