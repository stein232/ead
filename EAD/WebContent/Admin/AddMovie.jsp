<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="LoginChecker.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<%@include file="AdminLinkScript.jsp" %>
	<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script>
    <script type="text/javascript" src="../js/jquery.autocomplete.js"></script>
</head>
<%@include file="AdminNav.jsp" %>
<div id="toast" class="toastVisible"></div>
<div id="ongoingToast" class="toastVisible"></div>
	<div class="padTop padBottom">
		<div id="pgHeader" class="row inlineView">
			<div class="col-sm-1"></div>
			<div id="actionBtn" class="col-sm-10">
				<h3>Add Movie</h3>
				<button type="button" id="nextBtn" class="btn btn-primary taskbarBtn">Next</button>
			</div>
			<div class="col-sm-1"></div>
		</div>
		<div class="row">
			<div class="col-sm-1"></div>
			<div class="col-sm-10">
				<hr></hr>
			</div>
			<div class="col-sm-1"></div>
		</div>
		<div id="formArea">
			<div class="form-group row">
				<div class="col-sm-1"></div>
	            <label for="movieTitle" class="col-sm-2 control-label">Movie Title:</label>
	            <div class="col-sm-8">
	                <input type="text" id="movieTitle" class="form-control textboxStyle" placeholder="Insert movie title here"/>
	            </div>
	            <div class="col-sm-1"></div>
	        </div>
	        <div class="form-group row">
				<div class="col-sm-1"></div>
	            <label for="sypnosis" class="col-sm-2 control-label">Sypnosis:</label>
	            <div class="col-sm-8">
	                <textarea id="sypnosis" rows="5" cols="100" class="form-control textboxStyle" placeholder="Insert sypnosis here"></textarea>
	            </div>
	            <div class="col-sm-1"></div>
	        </div>
	        <div class="row">
				<div class="col-sm-1"></div>
	            <label for="genre" class="col-sm-2 control-label">Genre:</label>
	            <div class="col-sm-4">
	                <input type="text" name="genre" id="addGenreTb" class="form-control textboxStyle" placeholder="Start typing to receive suggestions"/>
	            </div>
	            <div class="col-sm-4 inlineView">
	            	<h6 id="genreGuideText" class="guideText"></h6>
		            <button type="button" id="saveBtn" class="btn btn-primary" data-toggle="modal" 
		                        data-target="#genreModal"><div class="glyphicon glyphicon-plus"></div></button>
	            </div>
	            <div class="col-sm-1"></div>
	            <div id="genreModal" class="modal fade bs-example-modal-lg" 
	                    tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	                <div class="modal-dialog modal-lg">
	                    <div class="modal-content">
	                        <div class="modal-header">
	                            <button type="button" class="close" data-dismiss="modal"
	                                    aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                            <h4 class="modal-title">Add New Genre</h4>
	                        </div>
	                        <div class="modal-body">
	                            <input type="text" id="newGenreName" class="form-control" placeholder="Enter genre name"/>
	                        </div>
	                        <div class="modal-footer">
	                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                            <button type="button" id="addGenreSaveBtn" class="btn btn-primary" data-dismiss="modal">Add</button>
	                        </div>
	                    </div> <%--Modal Content--%>
	                </div>  <%--Modal Dialog--%>
	            </div>  <%--Modal--%>
	        </div>
	        <div class="row">
				<div class="col-sm-3"></div>
	            <div class="col-sm-8">
					<div id="genresSelected" class="buttonsMargin"></div>
	            </div>
	            <div class="col-sm1"></div>
	        </div>
	        <div class="form-group row">
				<div class="col-sm-1"></div>
	            <label for="duration" class="col-sm-2 control-label">Duration:</label>
	            <div class="col-sm-1">
	                <input type="text" id="duration" class="form-control textboxStyle" placeholder="e.g. 121"/>
	            </div>
	            <div class="col-sm7">
	            	<p>mins</p>
	            </div>
	            <div class="col-sm-1"></div>
	        </div>
	        <div class="form-group row">
				<div class="col-sm-1"></div>
	            <label for="releaseDate" class="col-sm-2 control-label">Release date:</label>
	            <div class="col-sm-3">
	                <input type="text" id="releaseDate" class="datepicker form-control textboxStyle"/>
	            </div>
	            <div class="col-sm5"></div>
	            <div class="col-sm-1"></div>
	        </div>
	        <div class="form-group row">
				<div class="col-sm-1"></div>
	            <label for="endDate" class="col-sm-2 control-label">End date:</label>
	            <div class="col-sm-3">
	                <input type="text" id="endDate" class="datepicker form-control textboxStyle"/>
	                <h6>Leave blank to determine end date at a later time</h6>
	            </div>
	            <div class="col-sm5"></div>
	            <div class="col-sm-1"></div>
	        </div>
	        <div class="row">
				<div class="col-sm-1"></div>
	            <label for="actor" class="col-sm-2 control-label">Actors:</label>
	            <div class="col-sm-4">
	            	<input type="text" name="actor" id="addActorTb" class="form-control textboxStyle" placeholder="Start typing to receive suggestions"/>
	                <!-- <input type="text" id="actor" class="form-control" placeholder="Insert actors here"/> -->
	            </div>
	            <div class="col-sm-4 inlineView">
	            	<h6 id="actorGuideText" class="guideText">
	            	</h6>
	            	<button type="button" id="actorAddBtn" class="btn btn-primary" data-toggle="modal" 
		                        data-target="#actorModal"><div class="glyphicon glyphicon-plus"></div></button>
	           	</div>
	            <div class="col-sm-1"></div>
	        </div>
	        <div class="row">
				<div class="col-sm-3"></div>
	            <div class="col-sm-8">
					<div id="actorsSelected" class="buttonsMargin"></div>
	            </div>
	            <div class="col-sm1"></div>
	            <div id="actorModal" class="modal fade bs-example-modal-lg" 
	                    tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	                <div class="modal-dialog modal-lg">
	                    <div class="modal-content">
	                        <div class="modal-header">
	                            <button type="button" class="close" data-dismiss="modal"
	                                    aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                            <h4 class="modal-title">Add New Actor</h4>
	                        </div>
	                        <div class="modal-body">
	                            <input type="text" id="newActorName" class="form-control" placeholder="Enter actor name"/>
	                        </div>
	                        <div class="modal-footer">
	                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                            <button type="button" id="addActorSaveBtn" class="btn btn-primary" data-dismiss="modal">Add</button>
	                        </div>
	                    </div> <%--Modal Content--%>
	                </div>  <%--Modal Dialog--%>
	            </div>  <%--Modal--%>
	        </div>
	        <div class="row">
				<div class="col-sm-1"></div>
			    <label for="endDate" class="col-sm-2 control-label">Movie poster:</label>
			    <div class="col-sm-3">
			    	<div id="imagePreview"></div>
			        <form id="imgForm" action="../ProcessImage" enctype="multipart/form-data">
						<input type="file" class="" name="image"/>
						<input id="imgSubmitBtn" type="submit" class="btn btn-default" value="Submit" />
					</form>
			    </div>
			    <div class="col-sm5"></div>
			    <div class="col-sm-1"></div>
	        	
	        </div>
	    </div> <!-- end of form area -->
	    <div id="addShowTimes" class="toggleVisibility">
	    	<div class="row">
	    		<div class="col-sm-1"></div>
	    		<div class="col-sm-10">
	    			<div class="col-sm-6">
	    				<div id="imagePreviewShowTimes"></div>
	    			</div>
	    			<div class="col-sm-6">
	    				<div id="movieText"></div>
	    			</div>
	    		</div>
	    		<div class="col-sm-1"></div>
	    	</div>
	    	<div class="row">
				<div class="col-sm-1"></div>
				<div class="col-sm-10">
					<hr></hr>
				</div>
				<div class="col-sm-1"></div>
			</div>
	    	<div class="form-group row">
	    		<div class="col-sm-1"></div>
	    		<label class="col-sm-3 control-label">Theater:</label>
	            <div class="col-sm-5">
	                <select id="theaterDdl" class="form-control textboxStyle"></select>
	            </div>
	    		<div class="col-sm-3"></div>
	    	</div>
	    	<div class="form-group row">
	    		<div class="col-sm-1"></div>
	    		<label class="col-sm-3 control-label">Halls:</label>
	            <div class="col-sm-5">
	                <select id="hallsDdl" class="form-control textboxStyle"></select>
	            </div>
	    		<div class="col-sm-3"></div>
	    	</div>
	    	<div class="form-group row">
				<div class="col-sm-1"></div>
	            <label for="movieTypeDdl" class="col-sm-3 control-label">Movie type:</label>
	            <div class="col-sm-5">
	                <select id="movieTypeDdl" class="form-control textboxStyle"></select>
	            </div>
	            <div class="col-sm-3">
	            </div>
	        </div>
            <div class="form-group row">
				<div class="col-sm-1"></div>
	            <label for="stDate" class="col-sm-3 control-label">Showtime Date:</label>
	            <div class="col-sm-5">
	                <input type="text" id="stDate" class="datepicker form-control textboxStyle"/>
	            </div>
	            <div class="col-sm-3"></div>
	        </div>
	        <div class="form-group row">
				<div class="col-sm-1"></div>
	            <label for="stTime" class="col-sm-3 control-label">Showtime Time:</label>
	            <div id="stTimeArea" class="col-sm-5">
	                <input type="text" id="stTime" class="form-control textboxStyle"/>
	                <button type="button" id="stAddButton" class="btn btn-primary"><div class="glyphicon glyphicon-plus"></div></button>
	            </div>
	            <div class="col-sm-3">
	            </div>
	        </div>
	        <div class="row">
	        	<div class="col-sm-4"></div>
	        	<div class="col-sm-5">
	        		<div id="showTimesSelected"></div>
	        	</div>
	        	<div class="col-sm-3"></div>
	        </div>
	    </div> <!--  end of addshowtimes -->
	</div>
	<script>
	 $(document).ready(function(){
		var actorsArray = [];
		var genresArray = [];
		
		$("#imgForm").submit(function(event){
			console.log($('.movThumbnail').attr('src'));
			event.preventDefault();
			toggleOngoingToast("Uploading image...");
			$("#imgForm").toggleClass("toastVisible");
		 	var formData = new FormData($("#imgForm")[0]);
		 	console.log("submit");
			$.ajax({
				type: "POST",
				url: "../ProcessImage",
				data: formData,
	            cache: false,
	            contentType: false,
	            processData: false,
		    }).done((data) => {
		    	$("#imgForm").toggleClass("toastVisible");
		    	toggleOngoingToast("");
		    	toggleToast("Successfully uploaded image");
		    	console.log(data);
		    	$("#imagePreview").html("<img class=\"movThumbnail\" src=\"" + data + "\">");
	    	})
	    }); 
		
	   /*  $(document).on("submit", "#imgForm", function() {
	        var $form = $(this);

	        $.post($form.attr("action"), $form.serialize(), function(responseJson) {
	        	
	        });
	    }); */
	    
	    function getActors() {
		    $.ajax({
					type: "GET",
					url: "../GetActors",
					dataType: "json"
			}).done((data) => {
				actorsArray = $.map(data, (dataItem) => { return { value: dataItem.ActorName, data: dataItem.ActorId }; });
				$('#addActorTb').devbridgeAutocomplete({
			    	lookup: actorsArray,
			    	onSelect: function(suggestion) {
			            $('#actorsSelected').append("<button type=\"button\" class=\"btn btn-danger buttonMargin\" value=\"" + 
			            		suggestion.data + "\" name=\""+ suggestion.value +"\"><span>" + suggestion.value + "</span><div class=\"glyphicon glyphicon-trash trashIcon\"></div></button>");
			            $('#addActorTb').val('');
			            
			            for (var a = 0; a < actorsArray.length; a++){
			            	if (actorsArray[a].data == suggestion.data){
			            		actorsArray.splice(a, 1);
			            	}
			            }
			        }
				});
				
			}).fail((e) => {
				console.log(e);
				alert("Error: " + e.status + " " + e.statusText);
			});
	    }
	    
	    function getGenres() {
		    $.ajax({
					type: "GET",
					url: "../GetGenres",
					dataType: "json"
			}).done((data) => {
				genresArray = $.map(data, (dataItem) => { return { value: dataItem.GenreName, data: dataItem.GenreId }; });
				$('#addGenreTb').devbridgeAutocomplete({
			    	lookup: genresArray,
			    	onSelect: function(suggestion) {
			            $('#genresSelected').append("<button type=\"button\" class=\"btn btn-danger buttonMargin\" value=\"" + 
			            		suggestion.data + "\" name=\""+ suggestion.value +"\"><span>" + suggestion.value + "</span><div class=\"glyphicon glyphicon-trash trashIcon\"></div></button>");
			            $('#addGenreTb').val('');
			            
			            for (var a = 0; a < genresArray.length; a++){
			            	if (genresArray[a].data == suggestion.data){
			            		genresArray.splice(a, 1);
			            	}
			            }
			        }
				});
			}).fail((e) => {
				console.log(e);
				alert("Error: " + e.status + " " + e.statusText);
			});
	    }
	    
	    getActors();
	    getGenres();
	    
	    $("#addGenreSaveBtn").click(function () {
            var thisGenreName =  {
            		"name": $("#newGenreName").val()
            };
            
            $.ajax({
                type: 'POST',
                url: '../AddGenre',
                dataType: 'json',
                data: { genreName : JSON.stringify(thisGenreName) },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var errorMsg = JSON.parse(XMLHttpRequest.responseText);
                    console.log(errorMsg);
                }
            }).done((data) => {
            	console.log("Done!");
            	getGenres();
            	toggleToast("Successfully added " + thisGenreName.name);
            }).fail((e) => {
            	console.log(e);
            });
        });
	    
	    function toggleToast(message) {
	    	$("#toast").html("<p>"+message+"</p>").toggleClass("toastVisible").delay(2500).queue(function(){$(this).toggleClass("toastVisible").dequeue()});
	    }
	    
	    function toggleOngoingToast(message) {
	    	$("#ongoingToast").html("<p>"+message+"</p>").toggleClass("toastVisible");
	    }
	    
	    $("#addActorSaveBtn").click(function () {
            var thisActorName =  {
            		"name": $("#newActorName").val()
            };
            
            $.ajax({
                type: 'POST',
                url: '../AddActor',
                dataType: 'json',
                data: { actorName : JSON.stringify(thisActorName) },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var errorMsg = JSON.parse(XMLHttpRequest.responseText);
                    console.log(errorMsg);
                }
            }).done((data) => {
            	console.log("Done!");
            	getActors();
            	toggleToast("Successfully added " + thisActorName.name);
            }).fail((e) => {
            	
            });
        });
	    
	    $("#nextBtn").click(function(){
	    	toggleOngoingToast("Please wait...");
	    	var actor;
	    	var genre;
	    	var movieTitle = $("#movieTitle").val();
	    	var sypnosis = $("#sypnosis").val();
	    	var duration = $("#duration").val();
	    	/* var releaseDate = $("#releaseDate").val() + "T00:00:00";
	    	//var releaseDate = "2013-02-10T13:45:30";
	    	//var endDate = "2013-02-11T13:45:30";
	    	var endDate = $("#endDate").val() + "T00:00:00"; */
	    	var originalRDate = $("#releaseDate").val();
	    	var releaseDate = originalRDate + "T00:00:00";
	    	//var releaseDate = "2013-02-10T13:45:30";
	    	//var endDate = "2013-02-11T13:45:30";
	    	var originalEDate = $("#endDate").val();
	    	if (originalEDate != ""){
	    		var endDate = originalEDate + "T00:00:00";
	    	}
	    	var genres = [];
	    	var actors = [];
	    	var image = $('.movThumbnail').attr('src');
	    	//var image = "xx";
	    	$('#genresSelected').find('button').each(function () {
   				genre = {"GenreId" : $(this).attr('value'), "GenreName" : $(this).attr('name')};
   				genres.push(genre);
	    	});
	    	$('#actorsSelected').find('button').each(function () {
   				actor = {"ActorId" : $(this).attr('value'), "ActorName" : $(this).attr('name')};
   				actors.push(actor);
	    	});
	    	
	    	
	    	var newMovie = {
	    			"Actors" : actors,
	    			"Genres" : genres,
	    			"MovieName" : movieTitle,
	    			"Sypnosis" : sypnosis,
	    			"Duration" : duration,
	    			"ReleaseDate" : releaseDate,
	    			"EndDate" : endDate,
	    			"ImageURL" : image
	    	}
	    	
	    	console.log(newMovie);

	    	/* Stuff to change when the button is clicked */
	    	$("#formArea").empty();
	    	$("#addShowTimes").toggleClass("toggleVisibility");
	    	$("#pgHeader h3").html("Add Showtime");
	    	//$("#pgHeader button").html("Done");
	    	$("#imagePreviewShowTimes").html("<img class=\"movThumbnail\" src=\"" + newMovie.ImageURL + "\">");
	    	
	    	/* TEMP AJAX */
	    	var newMovieTemp = {
	    			"Movie" : "2012"
	    	}
	    	$("#movieText").html("<p>" + movieTitle + 
	    			"</p><p>Sypnosis: " + sypnosis + 
	    			"</p><p>Duration: " + duration + 
	    			" mins</p><p>Release Date: " + originalRDate + 
	    			"</p><p>End Date: " + originalEDate + "</p>");
	    	var genreText = "<p>Genre: ";
			for (var i = 0; i < genres.length; i++){
				if (i == 0){
					genreText += genres[i].GenreName;
				} else {
					genreText += ", " + genres[i].GenreName;
				}
				
	    	}
			genreText += "</p>";
			$("#movieText").append(genreText);
			var actorText = "<p>Actor: ";
			for (var j = 0; j < actors.length; j++){
				if (j == 0){
					actorText += actors[j].ActorName;
				} else {
					actorText += ", " + actors[j].ActorName;
				}
	    	}
			
			actorText += "</p>"
			$("#movieText").append(actorText);
			
			/* Ajax to populate ddl */
			$.ajax({
                type: 'GET',
                url: '../GetTheaters',
                dataType: 'json'
            }).done((data) => {
            	console.log(data);
            	for (var i = 0; i < data.length; i++){
            		$("#theaterDdl").append("<option value=\""+data[i].TheaterId+"\">"+data[i].TheaterName+"</option>");
            		console.log("<option value=\""+data[i].TheaterId+"\">"+data[i].TheaterName+"</option>");
            	}
            	$("#theaterDdl").trigger("change");
            }).fail((e) => {
            	toggleToast("Error: " + e);
            }); 
			
			$.ajax({
                type: 'GET',
                url: '../MovieTypeController',
                dataType: 'json'
            }).done((data) => {
            	console.log(data);
            	for (var i = 0; i < data.length; i++){
            		$("#movieTypeDdl").append("<option value=\""+data[i].MovieTypeId+"\">"+data[i].MovieTypeName+"</option>");
            	}
            }).fail((e) => {
            	toggleToast("Error: " + e);
            }); 
			
			$("#stAddButton").click(function(){
				var theaterId = $('#theaterDdl').val();
				var theaterName = $('#theaterDdl option:selected').text();
				var hallId = $('#hallsDdl').val();
				var orignalDate = $('#stDate').val();
				var stDate = orignalDate + "T00:00:00";
				var originalTime = $('#stTime').val();
				var stTime = "2000-01-01T" + originalTime  + ":00";
				var movieTypeId = $('#movieTypeDdl').val();
				var movieTypeName  = $('#movieTypeDdl option:selected').text();
				$('#showTimesSelected').append("<button type=\"button\" class=\"btn btn-danger buttonMargin\" value=\"" + 
						"{'MovieId': '" + movieId +"','TheaterId': '" + theaterId + "','TheaterName': '" + theaterName + 
					"', 'HallId': '" + hallId +"','ShowTimeDate' : '" + stDate + "', 'ShowTimeTime' : '" + stTime +
					"', 'MovieTypeId' : '" + movieTypeId + "'}" + 
	            		"\"><span>" + theaterName + "</span><br><span>" + hallId + "<br><span>"+ orignalDate +"</span><br><span>" + originalTime + 
	            		"</span><br><span>" + movieTypeName + "</span><br><div class=\"glyphicon glyphicon-trash trashIcon\"></div></button>");
				/* var theaterId = $('#theaterDdl').val();
				var theaterName = $('#theaterDdl option:selected').text();
				var hallId = $('#hallsDdl').val();
				var stDate = $('#stDate').val() + "T00:00:00";
				var stTime = "2000-01-01T" + $('#stTime').val() + ":00";
				$('#showTimesSelected').append("<button type=\"button\" class=\"btn btn-danger buttonMargin\" value=\"" + 
						"{'MovieId': '" + movieId +"','TheaterId': '" + theaterId + "','TheaterName': '" + theaterName + "', 'HallId': '" + hallId +"','ShowTimeDate' : '" + stDate + "', 'ShowTimeTime' : '" + stTime + "'}" + 
	            		"\"><span>" + theaterName + "</span><br><span>"+ stDate +"</span><br><span>" + stTime + "</span><br><div class=\"glyphicon glyphicon-trash trashIcon\"></div></button>"); */
			});
			
			$("#showTimesSelected").on('click', 'button', function () {
	            $(this).remove();
	        });
			
			$("#nextBtn").remove();
			$("#actionBtn").append("<button id=\"submitBtn\" class=\"btn btn-danger taskbarBtn\">" + "Submit" + "</button>");
			
			$("#theaterDdl").change(function(){
				toggleOngoingToast("Please wait...");
				var theaterId = {
					"theater": $(this).val()
				}
				console.log(theaterId);
				$.ajax({
	                type: 'POST',
	                url: '../GetHalls',
	                dataType: 'json',
	                data: { theater : JSON.stringify(theaterId) }
	            }).done((data) => {
	            	toggleOngoingToast("");
	            	console.log(data);
	            	$("#hallsDdl").empty();
	            	for (var i = 0; i < data.length; i++){
	            		$("#hallsDdl").append("<option value=\"" + data[i].HallId + "\">" + data[i].HallNo  + "</option>");
	            	}
	            	
	            }).fail((e) => {
	            	toggleToast("Error: " + e);
	            }); 
			});
			var showTimes = [];
			var showTime;
			var movieId;
			$("#submitBtn").click(function(){
				toggleOngoingToast("Please wait...");
				$('#showTimesSelected').find('button').each(function () {
	   				showTime = $(this).attr('value');
	   				showTimes.push(showTime);
		    	});
				
				console.log(JSON.parse(JSON.stringify(showTimes)));
				//var parsed = JSON.parse("'" + showTimes + "'");
				 
				$.ajax({
	                type: 'POST',
	                url: '../AddShowTimes',
	                dataType: 'json',
	                data: { showTimes : JSON.stringify(showTimes) }
	            }).done((data) => {
	            	//toggleOngoingToast("");
	            	//toggleToast("Successfully added movie " + movieTitle);
	            	toggleOngoingToast("");
	            	window.location.replace("Dashboard.jsp");
	            }).fail((e) => {
	            	toggleToast("Error: " + e);
	            }); 
			});
			/*Actual ajax to send the movie data, change the url to addmovie */
	    	/*$.ajax({
                type: 'POST',
                url: '../GetMovie',
                dataType: 'json',
                data: { "name" : JSON.stringify(newMovieTemp) },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var errorMsg = JSON.parse(XMLHttpRequest.responseText);
                    console.log(errorMsg);
                }
            }).done((data) => {
            	toggleOngoingToast("");
            	toggleToast("Successfully added movie " + movieTitle);
            }).fail((e) => {
            	toggleToast("Error: " + e);
            }); */
	    	
	    	/* Comment out ajax for a while*/
	    	$.ajax({
                type: 'POST',
                url: '../AddMovie',
                dataType: 'json',
                data: { movie : JSON.stringify(newMovie) },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var errorMsg = JSON.parse(XMLHttpRequest.responseText);
                    console.log(errorMsg);
                }
            }).done((data) => {
            	console.log(data);
            	movieId = data;
            	toggleOngoingToast("");
            	toggleToast("Successfully added movie " + movieTitle);

            }).fail((e) => {
            	toggleToast("Error: " + e);
            });
	    });
	    
	    $( "#addActorTb" ).keyup(function(e) {
	    	  $("#actorGuideText").html('Press enter to insert');
	    	  
	    	  if (e.which == 13 || e.which == 8){
	    		  $("#actorGuideText").html('');
	    	  }
    	});
	    
	    $( "#addGenreTb" ).keyup(function(e) {
	    	  $("#genreGuideText").html('Press enter to insert');
	    	  
	    	  if (e.which == 13 || e.which == 8){
	    		  $("#genreGuideText").html('');
	    	  }
 		});
	    
	    
	    $("#actorsSelected").on('click', 'button', function () {
            id = $(this).attr('value');
            name = $(this).children().html();
            var obj = {value: name, data: id};
           
            actorsArray.push(obj);
            console.log(actorsArray);
            //$("#ddlTag").append('<option value="' + tagId + '">' + tagName + '</option>');
            $(this).remove();
        });
	    
	    $("#genresSelected").on('click', 'button', function () {
            //tagId = $(this).attr('value');
            //tagName = $(this).closest("div").find("p").html();
            //$("#ddlTag").append('<option value="' + tagId + '">' + tagName + '</option>');
            id = $(this).attr('value');
            name = $(this).children().html();
            var obj = {value: name, data: id};
            genresArray.push(obj);
            console.log(genresArray);
            $(this).remove();
        });
	    
		/* $('#autocomplete').devbridgeAutocomplete({
			serviceUrl: "../GetActors",
		    transformResult: function (response) {
		    	
		        return {
		            suggestions: $.map(JSON.parse(response), function(dataItem) {
		            	if ()
		                return { 
		                	value: dataItem.ActorName, 
		                	data: dataItem.ActorId 
	                	};
		            })
		        };
		    }
		}); */
		
		function datePicker () {
	        $(".datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
	    }
		datePicker();
	 });
	</script>

<%@include file="AdminFooter.jsp" %>
