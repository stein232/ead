<%@page import="mysqlDatabase.ActorManager"%>
<%@page import="model.Actor"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="LoginChecker.jsp" %>
<!DOCTYPE html>
<html>
<head>
<%@include file="AdminLinkScript.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Update Actor</title>
</head>
<%@include file="AdminNav.jsp" %>
	<%
		String actorId = request.getParameter("actorId");
		Actor actor = null;
		try {
			actor = new ActorManager().getActor(Integer.parseInt(actorId));	
		} catch (Exception e) {
			e.printStackTrace();
		}
	%>
	<div class="padTop">
		<div id="pgHeader" class="row inlineView">
			<div class="col-sm-1"></div>
			<div id="actionBtn" class="col-sm-10">
				<h3>Update Actor</h3>
			</div>
			<div class="col-sm-1"></div>
		</div>
		<div class="row">
			<div class="col-sm-1"></div>
			<div class="col-sm-10">
				<hr></hr>
			</div>
			<div class="col-sm-1"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div id="content" class="col-md-8 centerAlign">
				<form action="ProcessActor?action=update" method="POST">
					<input type="hidden" name="actorId" value="<%= actor.getActorId() %>" />
					<input type="text" class="form-group form-control textboxStyle" name="actorName" value="<%= actor.getActorName() %>" />
					<input type="submit" class="btn btn-primary" value="Update" />
				</form>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
<%@include file="AdminFooter.jsp" %>