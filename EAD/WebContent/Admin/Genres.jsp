<%@page import="model.Genre"%>
<%@page import="java.util.ArrayList"%>
<%@page import="mysqlDatabase.GenreManager"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="LoginChecker.jsp" %>
<!DOCTYPE html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>All Genres</title>
<%@include file="AdminLinkScript.jsp"%>
</head>
<%
	ArrayList<Genre> genres = null;
	
	try{
		genres = new GenreManager().getAllGenres();	
	} catch (Exception e) {
		e.printStackTrace();
	}
	
%>
<%@include file="AdminNav.jsp" %>
	<div class="padTop">
		<div id="pgHeader" class="row inlineView">
			<div class="col-sm-1"></div>
			<div id="actionBtn" class="col-sm-10">
				<h3>All Genres</h3>
				<a href="AddGenre.jsp" class="btn btn-primary taskbarBtn">Add Genre</a>
			</div>
			<div class="col-sm-1"></div>
		</div>
		<div class="row">
			<div class="col-sm-1"></div>
			<div class="col-sm-10">
				<hr></hr>
			</div>
			<div class="col-sm-1"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div id="content" class="col-md-8 centerAlign">
				<div id="error"><%= genres == null ? "Cannot connect to database." : "" %></div>
				<div id="message"><%= session.getAttribute("message") != null ? (String)session.getAttribute("message") : "" %></div>
				<table id="j-table" class="dataTable">
					<thead>
						<th>Genre</th>
						<th>Action</th>
					</thead>
				</table>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
<%
	session.removeAttribute("message");
%>
<script>
	$("#j-table").DataTable({
		ajax: {
			url: "../GetGenres",
			dataSrc: "",
		},
		order:[[0, "asc"]],
		columns: [
			{data: "GenreName"},
			null,
		],
		columnDefs: [
			{
				targets: 1,
				orderable: false,
				render: function(data, type, row, meta) {
					return (
						'<form action="UpdateGenre.jsp" method="post" class="jtableInline">\
							<input type="hidden" name="genreId" value="' + row.GenreId + '"/>\
							<input type="submit" value="Update" class="update-btn btn" />\
						</form>\
						<form action="ProcessGenre?action=delete" method="post" class="jtableInline">\
							<input type="hidden" name="genreId" value="' + row.GenreId + '" />\
							<input type="hidden" name="genreName" value="' + row.GenreName + '" />\
							<input type="submit" value="Delete" class="delete-btn btn" />\
						</form>'
					);
				},
			},
		],
	});
</script>
<%@include file="AdminFooter.jsp" %>