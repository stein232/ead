<%@page import="model.Genre"%>
<%@page import="mysqlDatabase.GenreManager"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="LoginChecker.jsp" %>
<!DOCTYPE html>
<html>
<head>
<%@include file="AdminLinkScript.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Update Genre</title>
</head>
<%@include file="AdminNav.jsp" %>
	<%
		String genreId = request.getParameter("genreId");
		Genre genre = null;
		try {
			genre = new GenreManager().getGenre(Integer.parseInt(genreId));	
		} catch (Exception e) {
			e.printStackTrace();
		}
	%>
	<div class="padTop">
		<div id="pgHeader" class="row inlineView">
			<div class="col-sm-1"></div>
			<div id="actionBtn" class="col-sm-10">
				<h3>Update Genre</h3>
			</div>
			<div class="col-sm-1"></div>
		</div>
		<div class="row">
			<div class="col-sm-1"></div>
			<div class="col-sm-10">
				<hr></hr>
			</div>
			<div class="col-sm-1"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div id="content" class="col-md-8 centerAlign">
				<form action="ProcessGenre?action=update" method="POST">
					<input type="hidden" name="genreId" value="<%= genre.getGenreId() %>" />
					<input type="text" class="form-group form-control textboxStyle" name="genreName" value="<%= genre.getGenreName() %>" />
					<input type="submit" class="btn btn-primary" value="Update" />
				</form>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
<%@include file="AdminFooter.jsp" %>