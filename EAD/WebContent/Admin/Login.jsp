<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@include file="AdminLinkScript.jsp" %>
<title>Login</title>
</head>
<%@include file="LoginNav.jsp" %>
	<div class="padTop">
		<div class="row">
			<div class="col-md-2"></div>
			<div id="content" class="col-md-8 centerAlign">
				<h3>ArcLight account</h3>
				<p id="admPanelPadding">Log in to Admin Panel</p>
				<div id="errorMessage"><%= request.getAttribute("error") != null ? request.getAttribute("error") : "" %></div>
				<form action="ProcessAuthentication" method="POST">
					<div class="col-md-4">
					</div>
					<div id="loginPanel" class="col-md-4">
						<input type="email" name="email" id="email" class="form-group form-control textboxStyle" placeholder="Email" />
						<input type="password" name="password" id="password" class="form-group form-control textboxStyle" placeholder="Password" />
						<input type="submit" value="Login" id="loginBtn" class="btn btn-primary"/>
					</div>
					<div class="col-md-4">
					</div>
				</form>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>

</body>
</html>