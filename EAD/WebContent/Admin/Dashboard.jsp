<%@page import="model.Movie"%>
<%@page import="java.util.ArrayList"%>
<%@page import="mysqlDatabase.MovieManager"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="LoginChecker.jsp" %>
<!DOCTYPE html>
<head>
	<%@include file="AdminLinkScript.jsp" %>
</head>
<%@include file="AdminNav.jsp" %>
	<div class="padTop">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8 center-block">
				<a class="btn btn-default" id="curBtn" role="button">Current Movies</a>
				<a class="btn btn-default" id="allBtn" role="button">All Movies</a>
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8"><hr></hr></div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div id="content" class="col-md-8">
			<!--  -->

			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
	<script>
	
		$(document).ready(function(){
			var pleaseWaitMsg = "<div id=\"waitMsg\"><p>Please wait...</p></div>";
			$("#content").empty().append(pleaseWaitMsg);
			function batchToggleBtn(){
				$("#curBtn").toggleClass("btn-default");
				$("#curBtn").toggleClass("btn-primary");
				$("#curBtn").toggleClass("selected");
				$("#allBtn").toggleClass("selected");
				$("#allBtn").toggleClass("btn-primary");
				$("#allBtn").toggleClass("btn-default");
			}
			
			$('#curBtn').click(function(event){
				if ((!$("#allBtn").hasClass("selected")) && (!$("#curBtn").hasClass("selected"))){
					console.log("Cone");
					$("#curBtn").toggleClass("selected");
					$("#curBtn").toggleClass("btn-primary");
					$("#curBtn").toggleClass("btn-default");
				} else {
					batchToggleBtn();
				}
				
				$("#contentArea").empty().append(pleaseWaitMsg);
				var text = {
						"type": "cur"
				}
				
				$.ajax({
						type: "POST",
						url: "../GetMovies",
						data: {
							type: JSON.stringify(text)
						},
						dataType: "json"
				}).done((data) => {
					console.log(data);
					populateContent(data);
				}).fail((e) => {
					console.log(e);
					alert("Error: " + e.status + " " + e.statusText);
				});
			});
			
			$('#allBtn').click(function(event){
				if ((!$("#allBtn").hasClass("selected")) && (!$("#curBtn").hasClass("selected"))){
					console.log("one");
					$("#allBtn").toggleClass("selected");
					$("#allBtn").toggleClass("btn-primary");
					$("#allBtn").toggleClass("btn-default");
				} else {
					batchToggleBtn();
				}

				$("#contentArea").empty().append(pleaseWaitMsg);

				var text = {
						"type": "all"
				}
				
				$.ajax({
						type: "POST",
						url: "../GetMovies",
						data: {
							type: JSON.stringify(text)
						},
						dataType: "json"
				}).done((data) => {
					console.log(data);
					populateContent(data);
				}).fail((e) => {
					console.log(e);
					alert("Error: " + e.status + " " + e.statusText);
				});
			});
			
			function populateContent(data){
				var displayText = "";
				var count = 1;
				for (var i = 0; i < data.length; i++){
					if (count == 1){
						displayText += "<div class=\"row\">";
					}
					displayText += "<a href=\"UpdateMovie.jsp?id=" + data[i].MovieId + "\"><div class=\"col-md-4 movTile\">";
					displayText += "<img class=\"movThumbnail\" src=\""+ data[i].ImageURL +"\"></img>";
					displayText += "<p>" + data[i].MovieName + "</p>";
					displayText += "</div></a>";
					if (count == 3) {
						displayText += "</div>";
						count = 0;
					}
					count++;
				}
				$("#content").empty().append(displayText);
			}
			$("#curBtn").trigger("click");
		});
	</script>
	
<%@include file="AdminFooter.jsp" %>
