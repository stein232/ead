<%@page import="mysqlDatabase.StaffManager" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page import="java.sql.SQLException" %>
<%
	String email = (String)session.getAttribute("email");
	String password = (String)session.getAttribute("password");
	boolean loggedIn = false;
	try {
		loggedIn = new StaffManager().checkStaff(email, password);
	} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	if (!loggedIn) {
		response.sendRedirect("Login.jsp");
	}
%>