<%@page import="model.Member"%>
<%
	Member m = (Member)session.getAttribute("member");
	String navbarDisplayText = "Log in";
	if (m != null){
		navbarDisplayText = m.getName();
	}
%>
<div id="headerWrapper" class="headerWrapperPublic">
	<div class="col-sm-2">
	</div>
	<div class="col-sm-4 alignLeft">
		<nav id="userNavbar">
			<ul>
			  <a href="/"><li>Arclight <span id="userNavGreen">Cineplexes</span></li></a>
			</ul>
		</nav>
	</div>
	<div class="col-sm-4 alignRight">
		<nav id="navBarNav">
			<ul>
			  <a href="javascript:;" id="loginBtn"><li id="loginText"><%= navbarDisplayText %></li></a>
			  <a href="/Cart" id="cartBtn"><li id="cartPadding"><div id="cartIcon"></div></li></a>
			</ul>
		</nav>
		<div id="signInOverlayPanel" class="signInOverlay toastVisible">
			<div id="signInPanel">
				<div id="pleaseWaitMenu" class="toastVisible">
					<div class="arrow arrowLogoutMenu"></div>
					<p>Please wait...</p>
				</div>
				<div id="signInMenu">
					<div class="arrow"></div>
					<h3>Member sign in</h3>
					<hr></hr>
					<p>Sign in to enjoy discounts, rewards, and more</p>
					<p id="err" class="errorMessage toastVisible"></p>
					<input type="email" name="userEmail" id="userEmail" class="form-group form-control textboxStyle" placeholder="Email" />
					<input type="password" name="userPassword" id="userPassword" class="form-group form-control textboxStyle" placeholder="Password" />
					<input type="button" value="Sign in" id="userLoginBtn" class="btn btn-primary"/>
					<p class="noMargin padTop20">Don't have an account?</p>
					<input type="button" value="Sign up" id="userSignUpBtn" class="btn btn-primary"/>
				</div>
				<div id="signOutMenu">
					<div class="arrow arrowLogoutMenu"></div>
					<p id="userDropdownDisplayText">Welcome, <%= navbarDisplayText %>!</p>
					<hr></hr>
					<p>Now you can enjoy one stop access to your booking information, discounts, and more with your Arclight account.</p>
					<input type="button" value="My Arclight" id="userInfoBtn" class="btn btn-primary"/>
					<input type="button" value="Sign out" id="userLogoutBtn" class="btn btn-primary"/>
				</div>
				<div id="loggedOutMessage" class="toastVisible">
					<div class="arrow arrowLogoutMenu"></div>
					<p id="userDropdownDisplayText">Logged out</p>
					<hr></hr>
					<p>You've been logged out. See you next time!</p>
					<input type="button" value="Sign in" id="userLogoutSignInBtn" class="btn btn-primary"/>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-2">
	</div>
</div>
<script>
	$(document).ready(function(){

		<% if (m != null) { %>
			$("#signInMenu").toggleClass("toastVisible");
		<% } else { %>
			$("#signOutMenu").toggleClass("toastVisible");
		<% } %>
		
		$("#loginBtn").click(function(){
			$(".signInOverlay").toggleClass("toastVisible");
		})
		
		$("#userSignUpBtn").click(function(){
			window.location.href = "CreateMember.jsp";
		})
		
		$("#userInfoBtn").click(function(){
			window.location.href = "/Member/Profile";
		})
		
		$("#userLogoutSignInBtn").click(function(){
			$("#signInMenu").toggleClass("toastVisible");
			$("#loggedOutMessage").toggleClass("toastVisible");
		});
		
		$("#userLogoutBtn").click(function(){
			$("#pleaseWaitMenu").toggleClass("toastVisible");
			$("#signOutMenu").toggleClass("toastVisible");
			console.log("logout pressed");
			var memberObject = null;
			$.ajax({
	            type: 'POST',
	            url: '/Member/logout',
	            dataType: 'json',
	            data: { member : JSON.stringify(memberObject) }
	        }).done((data) => {
	        	console.log("logged out");
	        	$("#pleaseWaitMenu").toggleClass("toastVisible");
	        	console.log("Logged out");
	        	$("#loggedOutMessage").toggleClass("toastVisible");
	        	$("#loginText").text("Log in");
	        }).fail((e) => {
	        	$("#signOutMenu").toggleClass("toastVisible");
	        	console.log(e);
	        });
		});
		
		$("#userLoginBtn").click(function(){
			$("#pleaseWaitMenu").toggleClass("toastVisible");
			$("#signInMenu").toggleClass("toastVisible");
			var userName = $("#userEmail").val();
			var userPassword = $("#userPassword").val();
			var memberObject = {
					"Email" : userName,
					"Password" : userPassword
			}
			$.ajax({
	            type: 'POST',
	            url: '/Member/login',
	            dataType: 'json',
	            data: { member : JSON.stringify(memberObject) },
	            error: function (XMLHttpRequest, textStatus, errorThrown) {
	                var errorMsg = JSON.parse(XMLHttpRequest.responseText);
	                console.log(errorMsg);
	            }
	        }).done((data) => {
	        	$("#pleaseWaitMenu").toggleClass("toastVisible");
	        	console.log(data);
	        	var member = data;
	        	if (data == null){
	        		$("#signInMenu").toggleClass("toastVisible");
	        		console.log("NO DATA");
	        		if (($("#err").hasClass("toastVisible"))){
	        			console.log("no class");
	        			$("#err").html("Username or password incorrect");
	        			$("#err").removeClass("toastVisible");
	        		}
	        		
	        	} else {
	        		var member = data;
		        	$("#loginText").text(member.Name);
		        	$("#userDropdownDisplayText").text("Welcome, " + member.Name + "!");
	        		$("#signOutMenu").toggleClass("toastVisible");
	        	}
	        }).fail((e) => {
	        	console.log(e);
	        });
		});
		
		$(document).mouseup(function (e) {
		    $container = $("#signInOverlayPanel, #loginText");
		    if (!$container.is(e.target)
		        && $container.has(e.target).length === 0 && !$container.hasClass("toastVisible")) {
		        $("#signInOverlayPanel").addClass("toastVisible");
		    }
		});
		
	});
</script>
<div class="blurred" id="blurredC">
	<div id="blurredContent">