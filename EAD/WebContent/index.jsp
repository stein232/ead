<%@page import="mysqlDatabase.MovieManager"%>
<%@page import="model.Movie"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@include file="PublicLinkScript.jsp" %>
<title>Arclight Cineplexes</title>
</head>
<%@include file="UserNav.jsp" %>
<div id="toast" class="toastVisible"></div>
<div id="ongoingToast" class="toastVisible"></div>
<%
	boolean signUpSuccessful = false;
	boolean updateSuccessful = false;
	
	if (request.getParameter("status") != null)
		signUpSuccessful = true;
	
	if (request.getParameter("update") != null)
		updateSuccessful = true;
%>
<div class="padTop">
		<div id="pgHeader" class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-6 alignLeft headerStyle">
				<h2 class="headerBaseline">Now showing</h2>
			</div>
			<div id="actionBtn" class="col-sm-2">
				<div class="ddlStyle">
					<select name="genre-ddl" id="genre-ddl" class="form-control textboxStyle"></select>
				</div>
			</div>
			<div class="col-sm-2"></div>
		</div>
		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-8">
				<hr></hr>
			</div>
			<div class="col-sm-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div id="content" class="col-md-8">
			<!--  -->

			</div>
			<div class="col-md-2"></div>
		</div>
</div>
	<script>
		'use strict'
		var movies;
		$(document).ready(() => {
			$("#content").empty().append("<div id=\"waitMsg\"><p>Please wait...</p></div>");
			
			$.ajax({
					type: "POST",
					url: "GetMovies",
					data: {
						type: JSON.stringify({"type": "moviesGenres"})
					},
					dataType: "json",
			}).then((data) => {
				movies = data;
				return data;
			}).done(populateFromData)
			.fail((error) => {
				console.log(error.statusText);
			});

			$.ajax({
					type: "GET",
					url: "GetGenres",
					dataType: "json",
			}).done(populateGenreDdl)
			.fail((error) => {
				console.log(error.statusText);
			});
			
			if (<%= signUpSuccessful %>){
				toggleToast("Your account was created successfully and you are now logged in.")
			}
			
			if (<%= updateSuccessful %>){
				toggleToast("Your account has been updated.")
			}
		});
		
		function toggleToast(message) {
	    	$("#toast").html("<p>"+message+"</p>").toggleClass("toastVisible").delay(2500).queue(function(){$(this).toggleClass("toastVisible").dequeue()});
	    }
		
		function populateFromData(data) {
			console.log("populating");
			console.log("Movies", data);
			$("#content").empty();
			
			var moviesPerRow = 3;
			var exact = data.length%moviesPerRow == 0;
			var rows = data.length/moviesPerRow;
			if (!exact) rows++;
			console.log(rows);
			
			for (var i = 0; i < rows; i++) {
				var rowDiv = $("<div class=\"row\"/>");
				for (var j = 0; j < moviesPerRow; j++ ) {
					console.log(i * moviesPerRow + j, data[i * moviesPerRow + j].MovieName);
					var displayText = "<div class=\"col-md-4 movTile\">";
					if (data[i * moviesPerRow + j].ImageURL) {
						displayText += "<img class=\"movThumbnail\" src=\"" + data[i * moviesPerRow + j].ImageURL + "\" alt=\"" + data[i * moviesPerRow + j].MovieName + "\"/>";
					} else {
						displayText += "<div class=\"movThumbnail\"></div>";	
					}
					
					displayText += "<p>" + data[i * moviesPerRow + j].MovieName + "</p>";
					displayText += "</div>";
					
					var movieA = $("<a href=\"Movies/" + data[i * moviesPerRow + j].MovieName + "\">");
					var movieDiv = $(displayText);
					movieA.append(movieDiv);
					rowDiv.append(movieA);
					if (i * moviesPerRow + j == data.length - 1) break;
				}
				$("#content").append(rowDiv);
			}
		}

		function populateGenreDdl(data) {
			console.log("Genres", data);
			var $genreDdl = $("#genre-ddl");
			$genreDdl.append($("<option />").val(-1).text("All"));
			$.each(data, function() {
				$genreDdl.append($("<option />").val(this.GenreId).text(this.GenreName));
			});
			$genreDdl.change(function() {
			    var genreSelected = $genreDdl.val();
			    if (+genreSelected !== -1) {
			    	var sortedMovies = [];
			    	for (var i = 0; i < movies.length; i++) {
				    	var Genres = movies[i].Genres;
				    	for (var j = 0; j < Genres.length; j++) {
				    		if (Genres[j].GenreId === +genreSelected) {
				    			sortedMovies.push(movies[i]);
				    			break;
				    		}
				    	}
				    }
				    populateFromData(sortedMovies);
			    } else {
			    	populateFromData(movies);
			    }
			});
		}
		
		
	</script>	
</body>
</html>