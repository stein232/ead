<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="viewModel.CartViewModel"%>
<%@page import="model.MovieTicket"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/admheader.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-alpha1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<title>My Arclight</title>
</head>
<%@include file="UserNav.jsp" %>
<% 
	String name = "", email = "", contactNumber = "", mailingAddress = "", ccNo = "";
	if(m == null){
		response.sendRedirect("/");
	} else {
		name = m.getName();
		email = m.getEmail();
		contactNumber = m.getContactNumber();
		mailingAddress = m.getMailingAddress();
		ccNo = m.getCreditCardNumber();
	}
	
	ArrayList<CartViewModel> purchasedCvms = (ArrayList<CartViewModel>)request.getAttribute("purchasedCvms");
%>
<div class="padTop padBottom">
		<div id="pgHeader" class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-6 alignLeft headerStyle">
				<h2 class="headerBaseline">My Arclight</h2>
			</div>
			<div id="actionBtn" class="col-sm-2">
			</div>
			<div class="col-sm-2"></div>
		</div>
		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-8">
				<hr></hr>
			</div>
			<div class="col-sm-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div id="content" class="col-md-8">
				<p>Here's your Arclight account information:</p>
				<span class="profileName"><%= name %></span>
				<span class="legendText profileInfoGroup">Email</span><span><%= email %></span>
				<span class="legendText profileInfoGroup">Contact Number</span><span><%= contactNumber %></span>
				<span class="legendText profileInfoGroup">Mailing Address</span><span><%= mailingAddress %></span>
				<span class="legendText profileInfoGroup">Credit Card Number</span><span><%= ccNo %></span>
				<a href="../EditProfile.jsp"><button class="btn btn-default editProfileBtn">Edit Profile</button></a>
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-8">
				<hr></hr>
			</div>
			<div class="col-sm-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div id="content" class="col-md-8">
				<p>Past movies you have watched:</p>
					<%
					if (purchasedCvms != null){
						for (int i = purchasedCvms.size() - 1; i >= 0; i--) {
							ArrayList<MovieTicket> movieTickets = purchasedCvms.get(i).getMovieTickets();
							String seats = "";
							ArrayList<String> seatsArray = new ArrayList<>();
							for (int j = 0; j < movieTickets.size(); j++) {
								seats += movieTickets.get(j).getSeatRow() + movieTickets.get(j).getSeatNumber() + " ";
								seatsArray.add(movieTickets.get(j).getSeatRow() + movieTickets.get(j).getSeatNumber());
							}
					
						
					%>
						<div class="row">
							<div class="col-md-4">
								<img class="movThumbnail" src="<%= purchasedCvms.get(i).getMovieImgUrl() %>" alt="<%= purchasedCvms.get(i).getMovieName() %>" />
							</div>
							<div class="col-md-8">
								<a href="/Movies/<%= purchasedCvms.get(i).getMovieName() %>">
									<h4 class="inlineBlock">
										<%= purchasedCvms.get(i).getMovieName() %> (<%= purchasedCvms.get(i).getMovieTypeName() %>)
									</h4>
								</a>
								<div class="block">
									<%
										SimpleDateFormat parseFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
										SimpleDateFormat printFormat = new SimpleDateFormat("HH:mm");
										Date date = parseFormat.parse(purchasedCvms.get(i).getShowTimeTime().toString());
										String friendlyTime = printFormat.format(date);
									%>
									<h5>on <%= purchasedCvms.get(i).getShowTimeDate() %> <%= friendlyTime %> at <%= purchasedCvms.get(i).getTheaterName() %> Hall <%= purchasedCvms.get(i).getHallNo() %></h5>
								</div>
								<div class="block">
									<% for (String s : seatsArray) { %>
										<div class="box"><%= s %></div>
									<% } %>
								</div>
								<h4 class="block">You spent S$ <%= movieTickets.get(0).getPrice() * movieTickets.size() %> on these <%= movieTickets.size() %> tickets</h4>
							</div>
							
						</div>
						<div class="col-md-12"><hr class="rowGrayscale"></hr></div>
					<%
							}
						}
					%>
			</div>
			<div class="col-md-2"></div>
		</div>
</div>
<script>

</script>
</body>
</html>