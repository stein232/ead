<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="model.MovieType"%>
<%@page import="model.ShowTime"%>
<%@page import="model.Seat"%>
<%@page import="model.MovieTicket"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%
	ShowTime showTime = (ShowTime)request.getAttribute("showTime");
	SimpleDateFormat parseFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
	SimpleDateFormat printFormat = new SimpleDateFormat("HH:mm");
	Date date = parseFormat.parse(showTime.getShowTimeTime().toString());
	String friendlyTime = printFormat.format(date);
	String movieName = (String)request.getAttribute("movieName");
	MovieType movieType = (MovieType)request.getAttribute("movieType");
	int price = (int)request.getAttribute("price");
	ArrayList<ArrayList<Seat>> seats = (ArrayList<ArrayList<Seat>>)request.getAttribute("seats");
	ArrayList<MovieTicket> bookedMovieTickets = (ArrayList<MovieTicket>)request.getAttribute("bookedMovieTickets");
	
	ArrayList<Seat> bookedSeats = new ArrayList<Seat>(); 
	
	for (int i = 0; i < bookedMovieTickets.size(); i++) {
		Seat seat = new Seat();
		seat.setHallId(bookedMovieTickets.get(i).getHallId());
		seat.setRow(bookedMovieTickets.get(i).getSeatRow());
		seat.setSeatNumber(bookedMovieTickets.get(i).getSeatNumber());
		
		bookedSeats.add(seat);
	}
%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/admheader.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-alpha1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="../js/vague.js"></script>
<script src="../js/blur.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Seat selection</title>
</head>
<%@include file="UserNav.jsp" %>
	<div class="padTop100 padBottom">
		<div id="pgHeader" class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-6 alignLeft">
					<h2>Select seats for: </h2>
					<h4><%= movieName %> (<%= showTime.getMovieTypeName() %>)</h4>
					<h5>on <%= showTime.getShowTimeDate() %> <%= friendlyTime %> at <%= showTime.getTheaterName() %> Hall <%= showTime.getHallNo() %></h5>
				</div>
				<div id="actionBtn" class="col-sm-2">
				</div>
				<div class="col-sm-2"></div>
		</div>
		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-8">
				<hr></hr>
			</div>
			<div class="col-sm-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div id="content" class="col-md-8">
				<div class="row">
					<div class="col-md-12">
						<div class="screen-marker">Screen</div>
					</div>
				</div>
				<div id="seating-board">
				<%
					for (int i = seats.size() - 1; i >= 0; i--) {
						ArrayList<Seat> seatRow = seats.get(i);
				%>
					<ul class="seat-row">
						<li class="seat-row-marker">
							<div class="row-marker"><%= seatRow.get(0).getRow() %></div>
						</li>	
				<%		
						for (int j = 0; j < seatRow.size(); j++) {
							Seat seat = seatRow.get(j);
							if (!bookedSeats.contains(seat)) {
				%>
						<li class="btn seat-available" onClick="seatClick(this, '<%= seatRow.get(0).getRow() %>-<%= j+1 %>')">
							<div><%= j+1 %></div>
						</li>
				<%			} else {  %>
						<li class="btn seat-taken">
							<div><%= j+1 %></div>
						</li>		
				<%
							}
						}
				%>
					</ul>
				<%
					}
				%>
				</div>
				</div>
			<div class="col-md-2"></div>
		</div>
		<div id="staticBookingBar">
			<h5>Selected seats:</h5>
				<div id="selectedSeats">
				</div>
			<h5>Price:</h5>
				<div id="price">
				</div>
			<form action="" method="post">
				<input class="textboxStyle" type="hidden" name="theaterName" id="theaterName" value="<%= showTime.getTheaterName() %>" readonly/>
				<input class="textboxStyle" type="hidden" name="hallId" id="hallId" value="<%= showTime.getHallId() %>" readonly/>
				<input class="textboxStyle" type="hidden" name="movieName" id="movieName" value="<%= movieName %>" readonly/>
				<input class="textboxStyle" type="hidden" name="showTimeId" id="showTimeId" value="<%= showTime.getShowTimeId() %>" readonly/>
				<input class="textboxStyle" type="hidden" name="showTimeDate" id="showTimeDate" value="<%= showTime.getShowTimeDate() %>" readonly/>
				<input class="textboxStyle" type="hidden" name="showTimeTime" id="showTimeTime" value="<%= showTime.getShowTimeTime() %>" readonly/>
				<input class="textboxStyle" type="hidden" name="seats" id="seats" value="" readonly/>
				<input class="textboxStyle" type="hidden" name="price" id="price" value="" readonly/>
				<button class="btn btn-default" type="submit">Add to cart</button>
			</form>
		</div>
	</div>
	
</body>
<script>
	var seatsSelected = [];

	function seatClick(seat, seatRowAndNumber) {
		$(seat).toggleClass('seat-selected');
		if ($(seat).hasClass('seat-selected')) {
			seatsSelected.push(seatRowAndNumber);
		} else {
			var index = seatsSelected.indexOf(seatRowAndNumber);
			seatsSelected.splice(index, 1);
		}
		
		var valueOfSeatsInput = "";
		for (var i = 0; i < seatsSelected.length; i++) {
			valueOfSeatsInput += seatsSelected[i];
			if (i !== seatsSelected.length - 1) {
				valueOfSeatsInput += '|';
			}
			
		}
		$('#seats').val(valueOfSeatsInput);
		
		calculatePrice();
		updateStaticBar();
	}
	
	function calculatePrice() {
		var pricePerSeat = <%= price %>;
		$('#price').val('SGD'+ (seatsSelected.length * pricePerSeat));
	}
	
	function updateStaticBar() {
		$('#selectedSeats').empty();
		if (seatsSelected != null) {
			for (var i = 0; i < seatsSelected.length; i++){
				$('#selectedSeats').append('<div class="box">' + seatsSelected[i] + '</div>');
			}
		}
		$('#price').empty();
		var pricePerSeat = <%= price %>;
		$('#price').append('<h5>' + 'S$ '+ (seatsSelected.length * pricePerSeat) + '</h5>');
	}
</script>
</html>