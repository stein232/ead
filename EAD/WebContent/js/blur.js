
$(document).ready(function(){
   // var main = $('.main');
//    var mainiframe = $('#mainiframe');
    var $iframe = $('#mainiframe');
    var navbarHeight = $('nav').css('height');
    var windowWidth;
    var setHeight = false;
    var stackedHeight = 0;
    

	var scrollIframe = function () {
	//	var offset = $("#blurredC").offset().top;
	//	console.log(offset);
	//	$iframe.offset({top:offset});
		var offset = $(document).scrollTop();
		//$iframe.contents().scrollTop(offset);
	  }
	
	var checkSize = function () {
		stackedHeight = $('#headerWrapper').css('height');
		windowWidth = $( window ).width();
		if (windowWidth <= 939 && !setHeight){
			if (stackedHeight == 0){
				stackedHeight = $('#headerWrapper').css('height');
			}
			$('#mainiframe').css('height', stackedHeight);
			$('.main').css('height', stackedHeight);
			$('#fixed').css('height', stackedHeight);
			setHeight = true;
		} else if (windowWidth > 939 && setHeight){
			navbarHeight = $('nav').css('height');
			$('#mainiframe').css('height', navbarHeight);
			$('.main').css('height', navbarHeight);
			$('#fixed').css('height', navbarHeight);
			setHeight = false;
		}
	}
	
	$(window).on('scroll',scrollIframe);
	$(window).on('resize', checkSize);
	
	$iframe.attr('src', window.location.href);
	
	$iframe.ready(function() {
		//$iframe.contents().find("body").append("bb");
		$("#fixed").toggleClass("fixedPaint");
	//	$("#fixed").toggleClass("fixedPaint");
	//	$("#fixed").toggleClass("fixedPaint");
		if (windowWidth <= 939){
			$('#mainiframe').css('height', stackedHeight);
			$('.main').css('height', stackedHeight);
			$('#fixed').css('height', stackedHeight);
		} else {
			$('.main').css('height', navbarHeight);
			$('#mainiframe').css('height', navbarHeight);
			$('#fixed').css('height', navbarHeight);
		}
	});
	
	$("#mainiframe").on("load", function(){
		$iframe.contents().find('#headerWrapper').css('visibility', 'hidden');
	});
	
	scrollIframe();
	checkSize();
	

});






