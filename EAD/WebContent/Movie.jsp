<%@page import="model.Movie"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	Movie movie = (Movie)request.getAttribute("movie");
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/admheader.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-alpha1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="../js/vague.js"></script>
<script src="../js/blur.js"></script>
<title>${ movie.getMovieName() }</title>
<script>
	$.ajax({
			type: "POST",
			url: "../GetShowTimes",
			data: {
				movieId: <%= movie.getMovieId() %>
			},
			dataType: "json",
	}).done(populateShowTimes).fail((error) => {
		console.log(error.statusText);
	});

	$.ajax({
			type: "POST",
			url: "../GetReviews",
			data: {
				movieId: <%= movie.getMovieId() %>
			},
			dataType: "json",
	}).done(populateReviews).fail((error) => {
		console.log(error.statusText);
	});

	function populateShowTimes(data) {
		console.log(data);
		$("#showtimes").empty();
		var theaterId = data[0].TheaterId;
		var showTimeDate = data[0].ShowTimeDate;
		var $theater = $('<div class="theater"><h5 class="theater-name minPadding">' + data[0].TheaterName + '</h5></div>');
		var $date = $('<div class="date"><p class="theater-name">' + data[0].ShowTimeDate + '</p></div>');
		for (var i = 0; i < data.length; i++) {
			if (data[i].TheaterId != theaterId) {
				theaterId = data[i].TheaterId;
				showTimeDate = data[i].ShowTimeDate;
				$theater.append($date);
				$("#showtimes").append($theater);
				$theater = $('<div class="theater"><h5 class="theater-name minPadding">' + data[i].TheaterName + '</h5></div>');
				$date = $('<div class="date"><p class="theater-name">' + data[i].ShowTimeDate + '</p></div>');
			}
			if (data[i].ShowTimeDate != showTimeDate) {
				showTimeDate = data[i].ShowTimeDate;
				$theater.append($date);
				$date = $('<div class="date"><p class="theater-name">' + data[i].ShowTimeDate + '</p></div>');
			}
			var time = new Date(data[i].ShowTimeTime);
			var type = data[i].MovieTypeId;
			var typeClass;
			var typeText;
			var movieTypeName = data[i].MovieTypeName;
			if (type == 1){
				typeClass = "typeLabel digital";
			} else if (type == 11) {
				typeClass = "typeLabel atmos";
				movieTypeName = "";
			} else if (type == 21) {
				typeClass = "typeLabel imax";
				movieTypeName = "";
			} else if (type == 31) {
				typeClass = "typeLabel imax3d";
				movieTypeName = "";
			}
			$date.append($('<a href="../Booking/' +  data[i].ShowTimeId + '" class="btn btn-default buttonMargin show-time">' + time.getHours() + ':' 
					+ time.getMinutes() + '<div class="' + typeClass + '">' + movieTypeName + '</div>' + '</a>'))
			console.log("showtime",i);
			console.log($theater);
		};
		$theater.append($date);
		$("#showtimes").append($theater);
	}

	function populateReviews(data) {
		console.log(data);
		$("#reviews").empty();
		for (var i = 0; i < data.length; i++) {
			
			$review = $('<div class="review"></div>');
			$review
				.append($('<p class="reviewer-name">' + data[i].Name + '</p>'))
				.append($('<p class="reviewer-rating">' + data[i].Rating + '<span class="glyphicon glyphicon-star paddingLeft"></span></p>'))
				.append($('<p class="reviewer-review-message">' + data[i].ReviewMessage + '</p>'));
			$('#reviews').append($review);
			console.log("review",i);
		};
	}

</script>
</head>
<%@include file="UserNav.jsp" %>
<div class="padTop padBottom">
		<div id="pgHeader" class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-6 alignLeft headerStyle">
				<h2 class="headerBaseline">${ movie.getMovieName() }</h2>
			</div>
			<div id="actionBtn" class="col-sm-2">
			</div>
			<div class="col-sm-2"></div>
		</div>
		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-8">
				<hr></hr>
			</div>
			<div class="col-sm-2"></div>
		</div>
		<div class="row">
    		<div class="col-sm-1"></div>
    		<div class="col-sm-10">
    			<div class="col-sm-4">
    				<div id="imagePreviewShowTimes"><img class="movThumbnail" src="<%= movie.getImageURL() %>" alt="<%= movie.getMovieName() %>" /></div>
    			</div>
    			<div class="col-sm-6">
    				<div>
    					<p class="legendText">Sypnosis</p>
						<p>${ movie.getSypnosis() }</p>
						<p class="legendText">Release date</p>
						<p>${ movie.getReleaseDate() }</p>
						<p class="legendText">Duration</p>
						<p>${ movie.getDuration() } mins</p>
						<p class="legendText">Starring</p>
						<p>
							<% for(int i = 0; i < movie.getActors().size(); i++) { %>
								<% if ( i != (movie.getActors().size()-1)){ %>
									<%= movie.getActors().get(i).getActorName() %>,
								<% } else {%>
									<%= movie.getActors().get(i).getActorName() %>
								<% } %>
							<% } %>
						</p>
						<p class="legendText">Genre</p>
						<p>
							<% for(int i = 0; i < movie.getGenres().size(); i++) { %>
								<% if ( i != (movie.getGenres().size()-1)){ %>
									<%= movie.getGenres().get(i).getGenreName() %>,
								<% } else { %>
									<%= movie.getGenres().get(i).getGenreName() %>
								<% } %>
							<% } %>
						</p>
						<p class="legendText">Showtimes</p>
						<div id="showtimes"></div>
    				</div>
    			</div>
    		</div>
    		<div class="col-sm-3"></div>
    	</div>
    	<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-8">
				<hr></hr>
			</div>
			<div class="col-sm-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div id="content" class="col-md-8">			
				<div class="col-sm-1"></div>
				<div class="col-sm-10">
					<p class="legendText paddingBottom">Write Review</p>
					<form action="${ movie.getMovieName() }" method="post" enctype="UTF-8">
					<input type="hidden" name="movieId" value="${ movie.getMovieId() }">
					<div class="row">
						<div class="col-md-3">
							<input type="text" name="name" class="form-group form-control textboxStyle" placeholder="Name">
						</div>
						<div class="col-md-9"></div>
					</div>
					<div class="row">
						<div class="col-md-2">
							<input type="number" name="rating" class="form-group form-control textboxStyle" placeholder="Rating">
						</div>
						<div class="col-md-10"></div>
					</div>
					<textarea name="reviewMessage" class="form-group form-control textboxStyle" placeholder="Review"></textarea>
					<input type="submit" id="reviewSubmitBtn" class="btn btn-primary reviewSubmitBtn" value="Submit">
				</form>
				</div>
				<div class="col-sm-1"></div>	
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-8">
				<hr></hr>
			</div>
			<div class="col-sm-2"></div>
		</div>
		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-8">
				<div class="col-sm-1"></div>
				<div class="col-sm-10">
					<p class="legendText paddingBottom">Reviews</p>
					<div id="reviews"></div>
				</div>
				<div class="col-sm-1"></div>
			</div>
			<div class="col-sm-2"></div>
		</div>
</div>
	
</body>
</html>