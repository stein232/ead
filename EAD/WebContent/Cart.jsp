<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="model.MovieTicket"%>
<%@page import="viewModel.CartViewModel"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	ArrayList<CartViewModel> cvms = (ArrayList<CartViewModel>)request.getSession().getAttribute("cvms");
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Your Ticket Cart</title>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/admheader.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-alpha1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="../js/vague.js"></script>
<script src="../js/blur.js"></script>
</head>
<%@include file="UserNav.jsp" %>
	<div class="padTop padBottom">
		<div id="pgHeader" class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-6 alignLeft headerStyle">
				<h2 class="headerBaseline">Cart</h2>
			</div>
			<div id="actionBtn" class="col-sm-2">
			</div>
			<div class="col-sm-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8"><hr></hr></div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<p>Select the movies you want to purchase and click Checkout:</p>
				<div class="row">
					<form action="/Cart" method="post">
						<%
							for (int i = 0; i < cvms.size(); i++) {
								ArrayList<MovieTicket> movieTickets = cvms.get(i).getMovieTickets();
								int price = 0;
								String seats = "";
								ArrayList<String> seatsArray = new ArrayList<>();
								for (int j = 0; j < movieTickets.size(); j++) {
									price += movieTickets.get(j).getPrice();
									seats += movieTickets.get(j).getSeatRow() + movieTickets.get(j).getSeatNumber() + " ";
									seatsArray.add(movieTickets.get(j).getSeatRow() + movieTickets.get(j).getSeatNumber());
								}
						%>
							<div class="row">
								<div class="col-md-4">
									<img class="movThumbnail" src="<%= cvms.get(i).getMovieImgUrl() %>" alt="<%= cvms.get(i).getMovieName() %>" />
								</div>
								<div class="col-md-8">
									<input type="checkbox" class="cbPadding inlineBlock" name="cvmSelections" value="<%= i %>" />
									<a href="/Movies/<%= cvms.get(i).getMovieName() %>">
										<h4 class="inlineBlock">
											<%= cvms.get(i).getMovieName() %> (<%= cvms.get(i).getMovieTypeName() %>)
										</h4>
									</a>
									<div class="block">
										<%
											SimpleDateFormat parseFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
											SimpleDateFormat printFormat = new SimpleDateFormat("HH:mm");
											Date date = parseFormat.parse(cvms.get(i).getShowTimeTime().toString());
											String friendlyTime = printFormat.format(date);
										%>
										<h5>on <%= cvms.get(i).getShowTimeDate() %> <%= friendlyTime %> at <%= cvms.get(i).getTheaterName() %> Hall <%= cvms.get(i).getHallNo() %></h5>
									</div>
									<div class="block">
										<% for (String s : seatsArray) { %>
											<div class="box"><%= s %></div>
										<% } %>
									</div>
									
									<h4 class="block">S$ <%= movieTickets.get(0).getPrice() * movieTickets.size() %> for these <%= movieTickets.size() %> tickets</h4>
									<input class="textboxStyle" type="hidden" name="theaterName" value="<%= cvms.get(i).getTheaterName() %>" readonly/>
									<input class="textboxStyle" type="hidden" name="hallId" value="<%= movieTickets.get(0).getHallId() %>" readonly/>
									<input class="textboxStyle" type="hidden" name="showTimeDate" value="<%= cvms.get(i).getShowTimeDate() %>" readonly/>
									<input class="textboxStyle" type="hidden" name="showTimeTime" value="<%= cvms.get(i).getShowTimeTime() %>" readonly/>
									<input class="textboxStyle" type="hidden" name="price" value="<%= price %>" readonly/>
									<input class="textboxStyle" type="hidden" name="seats" value="<%= seats %>" readonly/>
								</div>
							</div>
							<div class="col-md-12"><hr class="rowGrayscale"></hr></div>
						<%
							}
						%>
						<div class="col-md-12 rightAlign">
							<button class="btn btn-danger" type="submit" name="delete">Delete</button>
							<button class="btn btn-default" type="submit" name="checkout">Checkout</button>
						</div>
						
					</form>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
</body>
</html>