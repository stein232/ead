<%@page import="java.util.Arrays"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="model.MovieTicket"%>
<%@page import="java.util.ArrayList"%>
<%@page import="viewModel.CartViewModel"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@include file="PublicLinkScript.jsp" %>
<!-- Credit card checker library provided by Stripe -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.payment/1.2.3/jquery.payment.js" ></script>
<!-- reCaptcha provided by Google -->
<script type="text/javascript" src='https://www.google.com/recaptcha/api.js'></script>
<title>Update Account</title>
</head>
<%@include file="UserNav.jsp" %>
<% 
	String name = "", email = "", contactNumber = "", mailingAddress = "", ccNo = "";
	String[] ccArray = {"", "", "", ""};
	if(m == null){
		response.sendRedirect("/");
	} else {
		name = m.getName();
		email = m.getEmail();
		contactNumber = m.getContactNumber();
		mailingAddress = m.getMailingAddress();
		ccNo = m.getCreditCardNumber();
		/* Regex to split every 4 characters*/
		ccArray = ccNo.split("(?<=\\G....)");
	}
	
	ArrayList<CartViewModel> purchasedCvms = (ArrayList<CartViewModel>)request.getAttribute("purchasedCvms");
%>
<div class="padTop padBottom">
		<div id="pgHeader" class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-6 alignLeft headerStyle">
				<h2 class="headerBaseline">Update Account</h2>
			</div>
			<div id="actionBtn" class="col-sm-2">
			</div>
			<div class="col-sm-2"></div>
		</div>
		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-8">
				<hr></hr>
			</div>
			<div class="col-sm-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div id="content" class="col-md-8">
					<div id="formArea">
						<form action="/Member/update" method="POST" onsubmit="return checkAllFields()">
							<div class="form-group row">
					            <div class="col-sm-4">
					                <input type="email" id="userEmailText" class="form-control textboxStyle" 
					                name="userEmailText" onfocus="clearContent('userEmailText')" placeholder="Email"
					                value="<%= email %>"/>
					            </div>
					            <div class="col-sm-4">
						            <h6 id="userEmailTextErr" class="guideText errorMessage">
			            			</h6>
		            			</div>
					            <div class="col-sm-4"></div>
					        </div>
							<div class="form-group row">
					            <div class="col-sm-4">
					                <input type="text" id="userNameText" class="form-control textboxStyle"
					                name="userNameText" onfocus="clearContent('userNameText')" placeholder="Name"
					                value="<%= name %>"/>
					            </div>
					            <div class="col-sm-4">
						            <h6 id="userNameTextErr" class="guideText errorMessage">
			            			</h6>
		            			</div>
					            <div class="col-sm-4"></div>
					        </div>
					        <div class="form-group row">
					            <div class="col-sm-4">
					                <input type="password" id="userPasswordText" class="form-control textboxStyle" 
					                name="userPasswordText" onfocus="clearContent('userPasswordText')" placeholder="Type a new password (optional)"/>
					            </div>
					            <div class="col-sm-4">
						            <h6 id="userPasswordTextErr" class="guideText errorMessage">
			            			</h6>
		            			</div>
					            <div class="col-sm-4"></div>
					        </div>
					        <div class="form-group row">
					            <div class="col-sm-4">
					                <input type="password" id="userPasswordCfm" class="form-control textboxStyle" 
					                onfocus="clearContent('userPasswordCfm')" placeholder="Confirm your new password"/>
					            </div>
					            <div class="col-sm-4">
						            <h6 id="userPasswordCfmErr" class="guideText errorMessage">
			            			</h6>
		            			</div>
					            <div class="col-sm-4"></div>
					        </div>
					        <div class="form-group row">
					            <div class="col-sm-4">
					                <input type="tel" pattern="[0-9]*" id="userPhone" class="form-control textboxStyle" 
					                name="userPhone" onfocus="clearContent('userPhone')" placeholder="Phone number"
					                value="<%= contactNumber %>"/>
					            </div>
					            <div class="col-sm-4">
						            <h6 id="userPhoneErr" class="guideText errorMessage">
			            			</h6>
		            			</div>
					            <div class="col-sm-4"></div>
					        </div>
					        <div class="form-group row">
					            <div class="col-sm-6">
					                <textarea id="userAddress" rows="5" cols="100" class="form-control textboxStyle" 
					                name="userAddress" onfocus="clearContent('userAddress')" placeholder="Address"><%= mailingAddress %></textarea>
					            </div>
					            <div class="col-sm-4">
						            <h6 id="userAddressErr" class="guideText errorMessage">
			            			</h6>
		            			</div>
					            <div class="col-sm-2"></div>
					        </div>
					        <div class="form-group row">
					            <div class="col-xs-3 col-sm-2">
					                <input type="text" id="ccField1" class="form-control textboxStyle" 
					                name="ccField1" placeholder="0000" maxlength="4" onfocus="clearCCContent(1)" onkeyup="keyPressed(1)"
					                value="<%= ccArray[0] %>"/>
					            </div>
					            <div class="col-xs-3 col-sm-2">
					                <input type="text" id="ccField2" class="form-control textboxStyle" 
					                name="ccField2" placeholder="0000" maxlength="4" onfocus="clearCCContent(2)" onkeyup="keyPressed(2)"
					                value="<%= ccArray[1] %>"/>
					            </div>
					            <div class="col-xs-3 col-sm-2">
					                <input type="text" id="ccField3" class="form-control textboxStyle" 
					                name="ccField3" placeholder="0000" maxlength="4" onfocus="clearCCContent(3)" onkeyup="keyPressed(3)"
					                value="<%= ccArray[2] %>"/>
					            </div>
					            <div class="col-xs-3 col-sm-2">
					                <input type="text" id="ccField4" class="form-control textboxStyle" 
					                name="ccField4" placeholder="0000" maxlength="4" onfocus="clearCCContent(4)" onkeyup="keyPressed(4)"
					                value="<%= ccArray[3] %>"/>
					            </div>
					            <div class="col-sm-2"><div id="cardType"></div></div>
					            <div class="col-sm-2"><h6 id="cardTypeErr" class="guideText errorMessage"></h6></div>
					        </div> 
					        <div class="form-group row">
						        <div class="g-recaptcha col-sm-5" data-sitekey="6LfzLRYTAAAAADV0vLJSKs5mgD-Wou-y1UPPIRZd" data-theme="dark" data-callback="rcCallback">
						        </div>
					        	<div class="col-sm-7"><h6 id="rcErr" class="guideText errorMessage"></h6></div>
					        </div>
					         
			        		<input type="submit" id="memberSubmitBtn" class="btn btn-default"/>
		        		</form>
			        </div>
				</div>
			<div class="col-md-2"></div>
		</div>
</div>
<script>
var rcResponse;
function rcCallback(response) {
	$("#rcErr").empty();
	rcResponse = response;
}

function clearContent(id){
	$("#" + id + "Err").empty();
	$("#" + id).css({ "border-bottom": "1px solid white" });
}

function clearCCContent(i){
	$("#ccField" + i).css({ "border-bottom": "1px solid white" });
	$("#cardTypeErr").empty();
}

function keyPressed(id){
	var toCount = $("#ccField" + id).val();
	console.log(toCount.length);
	if (toCount.length == 4){
		if (id < 4)
			$("#ccField" + (id+1)).focus();
	}
	checkCCFields();
}

function checkCCFields(){
	var ccNum = $("#ccField" + 1).val() + $("#ccField" + 2).val() + $("#ccField" + 3).val() + $("#ccField" + 4).val();
	if (ccNum.length == 16){
		$("#cardType").empty();
		var cardIssuer = $.payment.cardType(ccNum)
		switch(cardIssuer) {
		case "visa":
			$("#cardType").removeClass();
			$("#cardType").addClass("visa");
			break;
		case "mastercard":
			$("#cardType").removeClass();
			$("#cardType").addClass("mastercard");
			break;
		case "amex":
			$("#cardType").removeClass();
			$("#cardType").addClass("amex");
			break;
		default:
			$("#cardType").removeClass();
			if (cardIssuer == null)
				$("#cardType").text("Invalid card");
			else
				$("#cardType").text(cardIssuer);
		}
	}
}

function checkText(name, isCCField){
	console.log(name);
	var count = $("#" + name).val();
	console.log(count.length);
	console.log(count);
	if (count.length != 0){
		if (!isCCField)
			$("#" + name + "Err").empty();
		$("#" + name).css({ "border-bottom": "1px solid white" });
		return true;
	} else {
		if (!isCCField)
			$("#" + name + "Err").text("This field cannot be empty");
		$("#" + name).css({ "border-bottom": "1px solid #FF3119" });
		return false;
	}
}

function checkAllFields(){
	var emailCheckPassed = checkText("userEmailText");
	var userNameCheckPassed = checkText("userNameText"); 
	var userPhoneCheckPassed = checkText("userPhone");
	var userAddressCheckPassed = checkText("userAddress");
	var checkPasswordPassed = checkPasswordValid();
	var ccCheckPassed = true;
	for (var i = 1; i < 5; i++){
		var ccCheck = checkText("ccField" + i);
		if (!ccCheck)
			ccCheckPassed = false;
	}
	
	if(!ccCheckPassed)
		$("#cardTypeErr").text("These fields cannot be empty");
	
	if(rcResponse != null)
		$("#rcErr").empty();
	else
		$("#rcErr").text("Pass the turing test (Ex-Machina style)");
	
	if(emailCheckPassed && userNameCheckPassed  
			&& userPhoneCheckPassed && userAddressCheckPassed && ccCheckPassed){
		console.log("Passed");
		var checkPasswordMatchPasssed = checkCfmPassword();
		if (rcResponse != null)
			if ($("#userPasswordText").val() == "")
				return true;
			else if (checkPasswordPassed && checkPasswordMatchPasssed) 
				return true;
			else
				return false;
		else
			return false;
	} else {
		return false;
	}
}

function checkCfmPassword(){
	if ($('#userPasswordText').val() != $("#userPasswordCfm").val()){
		$("#userPasswordCfmErr").text("Password does not match");
		$('#userPasswordCfm').css({ "border-bottom": "1px solid #FF3119" });
		return false;
	} else {
		$("#userPasswordCfmErr").empty();
		$('#userPasswordCfm').css({ "border-bottom": "1px solid white" });
		return true;
	}
}

function checkPasswordValid(){
	var regexExp  = /^[a-zA-Z0-9]*$/;
	var pwText = $("#userPasswordText").val();
	var isValid = !(regexExp.test(pwText));
	
	if (isValid){
		console.log("VALID");
		$("#userPasswordTextErr").empty();
		$('#userPasswordText').css({ "border-bottom": "1px solid white" });
		console.log(pwText.length);
		if (pwText.length < 17 && pwText.length > 7){
			$("#userPasswordTextErr").empty();
			$('#userPasswordText').css({ "border-bottom": "1px solid white" });
			return true;
		} else {
			$("#userPasswordTextErr").text("Password needs to be between 8 to 16 characters long");
			$('#userPasswordText').css({ "border-bottom": "1px solid #FF3119" });
			return false;
		}
	} else {
		console.log("INVALID");
		$("#userPasswordTextErr").text("Password needs to contain at least 1 alphanumeric character");
		$('#userPasswordText').css({ "border-bottom": "1px solid #FF3119" });
		return false;
	}
}

$(document).ready(function() { 
	$("#userPasswordCfm").keyup(function(){
		checkCfmPassword();
	});
	
	$("#userPasswordText").keyup(function(){
		checkPasswordValid();
	});
});
</script>
</body>
</html>